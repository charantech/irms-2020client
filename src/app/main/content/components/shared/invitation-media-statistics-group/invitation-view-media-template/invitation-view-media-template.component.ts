import { Component, OnInit, Input } from '@angular/core';
import { noImagePreview, MediaType } from '../../../../../../constants/constants'
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'irms-invitation-view-media-template',
  templateUrl: './invitation-view-media-template.component.html',
  styleUrls: ['./invitation-view-media-template.component.scss']
})
export class InvitationViewMediaTemplateComponent implements OnInit {

  @Input() template: FormGroup;
  @Input() templateStatistics: FormGroup;
  @Input() templateType: MediaType;
  @Input() fullWidth;
  @Input() isAcceptedRejected = false;
  transparentSource: string;
  informationBar: any[];
  templateInformation: any[];
  leftMessage: string = '';
  constructor() {
  }

  ngOnInit() {
    this.prepareFormValues();

    if (!this.template) {
      this.transparentSource = noImagePreview;
    } else {
      this.template.valueChanges.subscribe(x => {
        this.mapGlobalValues(x);
        this.transparentSource = x.image || noImagePreview;
      })
    }
    if (this.templateStatistics) {
      this.templateStatistics.valueChanges.subscribe(values => {
        this.mapValues(this.templateStatistics.value);
      });
    }
  }

  mapGlobalValues(values) {
    let bar = [];
    Object.keys(values).forEach(key => {
      let item = this.templateInformation.find(x => x.key == key);
      if (item) {
        item.value = values[key] || '-';
        bar.push(item);
      }
    });
    this.templateInformation = bar;
    if (bar['image']) {
      this.transparentSource = bar['image'];
    } else {
      this.transparentSource = noImagePreview;
    }
  }

  mapValues(values: any[]) {
    let bar = [];
    Object.keys(values).forEach(key => {
      let item = this.informationBar.find(x => x.key == key);
      if (item) {
        item.value = values[key];
        bar.push(item);
      }
    })
    this.informationBar = bar;
  }

  prepareFormValues(): void {
    switch (this.templateType) {
      case MediaType.Email: {
        this.leftMessage = 'Email';
        this.informationBar = [
          { key: 'triggered', name: 'Emails Initiated', value: 0 },
          { key: 'processed', name: 'Processed', value: 0 },
          { key: 'delivered', name: 'Delivered', value: 0 },
          { key: 'clicks', name: 'Clicks', value: 0 },
          { key: 'uniqueClicks', name: 'Unique Clicks', value: 0 },
          { key: 'opens', name: 'Opens', value: 0 },
          { key: 'uniqueOpens', name: 'Unique Opens', value: 0 },
          { key: 'bounces', name: 'Bounces', value: 0 },
          { key: 'spamReports', name: 'Spam Reports', value: 0 },
          { key: 'blocked', name: 'Blocks', value: 0 },
        ]
        if (this.isAcceptedRejected) {
          this.informationBar = [
            ...this.informationBar,
            { key: 'accepted', name: 'Accepted', value: 0 },
            { key: 'rejected', name: 'Rejected', value: 0 },
          ]
        }
        this.templateInformation = [
          { key: 'id', value: '' },
          { key: 'image', value: '' },
          { key: 'subject', name: 'Subject', value: this.template.controls['subject'].value || '-' },
          { key: 'sender', name: 'From Sender', value: this.template.controls['sender'].value || '-' }
        ]
        break;
      }

      case MediaType.Sms: {
        this.leftMessage = 'Sms';
        this.informationBar = [
          { key: 'triggered', name: 'SMS Initiated', value: 0 },
          { key: 'sent', name: 'Sent', value: 0 },
          { key: 'delivered', name: 'Delivered', value: 0 },
          { key: 'unDelivered', name: 'UnDelivered', value: 0 },
          { key: 'accepted', name: 'Accepted', value: 0 },
          { key: 'rejected', name: 'Rejected', value: 0 },
          { key: 'failed', name: 'Failed', value: 0 }
        ]
        if (this.isAcceptedRejected) {
          this.informationBar = [
            ...this.informationBar,
            { key: 'accepted', name: 'Accepted', value: 0 },
            { key: 'rejected', name: 'Rejected', value: 0 },
          ]
        }
        this.templateInformation = [
          { key: 'id', value: '' },
          { key: 'image', value: '' },
          { key: 'sender', name: 'From Sender', value: this.template.controls['sender'].value || '-' }
        ]
        break;
      }

      case MediaType.whatsapp: {
        this.leftMessage = 'Whatsapp';
        this.informationBar = [
          { key: 'initiated', name: 'Whatsapp Initiated', value: 0 },
          { key: 'sent', name: 'Sent', value: 0 },
          { key: 'delivered', name: 'Delivered', value: 0 },
          { key: 'read', name: 'Read', value: 0 },
          { key: 'failed', name: 'Failed', value: 0 },
          { key: 'accepted', name: 'Accepted', value: 0 },
          { key: 'rejected', name: 'Rejected', value: 0 },
        ],
          this.templateInformation = [
            { key: 'id', value: '' },
            { key: 'image', value: '' },
            { key: 'sender', name: 'From Sender', value: this.template.controls['sender'].value || '-' }
          ]
        break;
      }
    }
  }

  // TODO
  rename() {

  }

  // TODO
  export() {

  }
}
