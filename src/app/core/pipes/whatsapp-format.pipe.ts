import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'whatsappFormat'
})
export class WhatsappFormatPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {      
      let valueString = value;
      valueString = valueString.replace(/\*(\S(.*?\S)?)\*/gm, '<strong>$1</strong>');
      valueString = valueString.replace(/\_(\S(.*?\S)?)\_/gm, '<em>$1</em>');
      valueString = valueString.replace(/\~(\S(.*?\S)?)\~/gm, '<strike>$1</strike>');
      valueString = valueString.replace(/\```(\S(.*?\S)?)\```/gm, '<code>$1</code>');
      valueString = valueString.replace(/\n/g, '<br>');
      return valueString;
    }
    else { 
      return null;
    }
  }

}
