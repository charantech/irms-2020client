import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Section } from 'app/constants/constants';
import { EventCampaignModel } from '../../event-campaign.model';
import { CampaignInvitationAcceptedService } from './campaign-invitation-accepted.service';
import { CampaignInvitationAcceptedDataService } from './campaign-invitation-accepted-data.service';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { MatDialog } from '@angular/material';
import { CampaignConfigurationsDataService } from '../campaign-configurations/campaign-configurations-data.service';
import { EventCampaignEditService } from '../event-campaign-edit.service';

@Component({
  selector: 'irms-campaign-invitation-accepted',
  templateUrl: './campaign-invitation-accepted.component.html',
  styleUrls: ['./campaign-invitation-accepted.component.scss']
})
export class CampaignInvitationAcceptedComponent extends SectionListComponent<EventCampaignModel> implements OnInit {

  nodes: any;
  campaignId: any;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    private campaignDataService: CampaignConfigurationsDataService,
    private campaignService: EventCampaignEditService,
    public sectionService: CampaignInvitationAcceptedService,
    protected dataService: CampaignInvitationAcceptedDataService) {
    super(Section.EventCampaigns, sectionService, dataService, router);
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.sectionService.setFilter({ campaignId: this.campaignId });
      this.updateNodes();
      this.reloadCampaignName();
    });
  }
  
  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }

  createNode() {
    this.dataService.create({ eventCampaignId: this.campaignId, sortOrder: this.nodes.length + 1, title: `Message ${this.nodes.length + 1}` }).subscribe(id => {
      this.nodes.push({ id: id });
      this.updateNodes();
    });
  }
  
  updateNodes() {
    this.dataService.getList(this.sectionService.getFilter()).subscribe(result => {
      this.nodes = result;
    });
  }
  
  @PopUp('Are you sure want to duplicate a reminder?')
  copyNode(event) {
    this.dataService.duplicate({invitationId: event})
    .subscribe(() => {
      this.updateNodes();
    })
  }

  @PopUp('Are you sure want to delete a reminder?')
  deleteNode(event) {
    this.dataService.delete({id: event})
    .subscribe(() => {
      this.updateNodes();
      this.router.navigate(['../invitation-accepted'], { relativeTo: this.route })
    })
  }
}
