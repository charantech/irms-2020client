import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IrmsCroppieBtnComponent } from './irms-croppie-btn.component';

describe('IrmsCroppieBtnComponent', () => {
  let component: IrmsCroppieBtnComponent;
  let fixture: ComponentFixture<IrmsCroppieBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrmsCroppieBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IrmsCroppieBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
