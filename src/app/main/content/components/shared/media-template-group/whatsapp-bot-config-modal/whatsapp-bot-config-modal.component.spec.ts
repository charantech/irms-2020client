import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappBotConfigModalComponent } from './whatsapp-bot-config-modal.component';

describe('WhatsappBotConfigModalComponent', () => {
  let component: WhatsappBotConfigModalComponent;
  let fixture: ComponentFixture<WhatsappBotConfigModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappBotConfigModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappBotConfigModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
