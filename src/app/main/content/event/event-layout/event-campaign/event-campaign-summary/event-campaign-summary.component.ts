import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { timePeriodUnits, sideDialogConfig } from 'app/constants/constants';
import { Location } from '@angular/common';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { EventCampaignTestComponent } from './event-campaign-test/event-campaign-test.component';
import { EventCampaignDataService } from '../event-campaign-data.service';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { EventCampaignGoLiveModalComponent } from './event-campaign-go-live-modal/event-campaign-go-live-modal.component';

@Component({
  selector: 'irms-event-campaign-summary',
  templateUrl: './event-campaign-summary.component.html',
  styleUrls: ['./event-campaign-summary.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignSummaryComponent implements OnInit, CanExit {
  timePeriodUnits = timePeriodUnits;
  campaignId;

  summary: any = {
    stats: {
      guestCount: 0,
      sendTime: 0,
      emailCount: 0,
      smsCount: 0,
      whatsAppCount: 0
    },
    invitation: {
      id: '',
      interval: 0,
      intervalType: 0,
      isInstant: true,
      isEmail: false,
      isWhatsapp: false,
      isSms: false
    },
    pending: [],
    accepted: [],
    rejected: []
  };
  liveClicked: any;
  constructor(protected route: ActivatedRoute,
              private router: Router,
              protected location: Location,
              private dialog: MatDialog,
              private dataService: EventCampaignDataService,
              public sideDialog: DialogAnimationService) { }

  ngOnInit(): void {
    // campaign id param
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.dataService.loadCampaignSummary(this.campaignId).subscribe(result => {
        if (!result.invitation.isInstant) {
          const invitationDate = new Date(result.invitation.startDate);
          const now = new Date();
          if (invitationDate.getTime() < now.getTime()) {
            result.invitation.isPastDate = true;
          }
        }
        this.summary = result;
      });
    });
  }

  // Go back
  back(): void {
    this.location.back();
  }

  // Go Live 
  goLive(): void {
    this.liveClicked = true;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30vw';
    // dialogConfig.maxWidth = '50vw';
    // dialogConfig.maxHeight = '20vw';
    // dialogConfig.minHeight = '20vw';
    dialogConfig.data = this.campaignId;
    // this.router.navigate(['../'], { relativeTo: this.route })
    this.dialog.open(EventCampaignGoLiveModalComponent, dialogConfig).afterClosed().subscribe((x) => {
      if (x.res){
        this.router.navigate(['../../../'], { relativeTo: this.route });
      } else if (!x.res && x.message === 'Schedule datetime of Rsvp invitation is passed.') {
        const invitationDate = new Date(this.summary.invitation.startDate);
        const now = new Date();
        if (invitationDate.getTime() < now.getTime()) {
          this.summary.invitation.isPastDate = true;
        }
      } else {
        return;
      }
    });
    
    // this.dataService.goLive(this.campaignId)
    //   .subscribe(x => {
    //     this.router.navigate(['../'], { relativeTo: this.route })
    //   }, error => {
    //     if (error.error && error.error.code === 503) {
    //       const toasterService = appInjector().get(ToasterService);
    //       if (error.error && error.error.message) {
    //         toasterService.pop('error', null, error.error.message);
    //       } else {
    //         toasterService.pop('success', null, 'Campaign is queued and will be launched by background process');
    //       }
    //     }
    //   })
  }

  /// Sed test campaign
  sendTestCampaign(): void {
    // const dialogConfig = new MatDialogConfig();
    // dialogConfig.disableClose = true;
    // dialogConfig.autoFocus = true;
    // dialogConfig.minWidth = '60vw';
    // dialogConfig.maxWidth = '80vw';
    const dialogConfig: any = sideDialogConfig;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.width = '40vw';
    dialogConfig.data = { campaignId: this.campaignId };
    const dialogRef = this.sideDialog.open(EventCampaignTestComponent, dialogConfig);
    // dialogRef.afterClosed().subscribe(
    //   res => {
    //     if (res) {
    //       this.dataService.testCampaign({
    //         id: this.campaignId,
    //         emails: res.emails,
    //         phoneNumbers: res.phoneNumbers,
    //         whatsappNumbers: res.whatsappNumbers
    //       }).subscribe(x => { });
    //     }
    //   });
  }

  canDeactivate(): any {
    // if (!this.liveClicked) {
    //   const dialogConfig = new MatDialogConfig();
    //   dialogConfig.disableClose = true;
    //   const dialogRef = this.sideDialog.open(FuseConfirmDialogComponent, dialogConfig);
    //   dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
    //   dialogRef.componentInstance.confirmButton = 'Leave page';
    //   dialogRef.componentInstance.cancelButton = 'Stay';
    //   dialogRef.componentInstance.confirmMessage = 'This action will take you out of this page...\nDo you want to proceed?';
    //   return dialogRef.afterClosed();
    // }
    // else {
      return true;
    // }
  }
}
