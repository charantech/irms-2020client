import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-whatsapp-preview',
  templateUrl: './whatsapp-preview.component.html',
  styleUrls: ['./whatsapp-preview.component.scss']
})
export class WhatsappPreviewComponent implements OnInit {
 @Input() sender = 'See-U';
 @Input() messages = [];
 @Input() twoWay = false;
 public timeNow: Date = new Date();
  constructor() { }

  ngOnInit(): void{
  }

}
