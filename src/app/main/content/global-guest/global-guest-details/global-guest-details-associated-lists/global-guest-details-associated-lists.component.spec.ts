import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsAssociatedListsComponent } from './global-guest-details-associated-lists.component';

describe('GlobalGuestDetailsAssociatedListsComponent', () => {
  let component: GlobalGuestDetailsAssociatedListsComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsAssociatedListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsAssociatedListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsAssociatedListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
