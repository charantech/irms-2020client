import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '../../core/animations';
import { AccountService } from '../account.service';
import { FuseConfigService } from '../../core/services/config.service';
import { RegexpPattern } from '../../constants/constants';
import { AppStateService } from 'app/main/content/services/app-state.service';

@Component({
  selector: 'mvms-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  animations: fuseAnimations
})

export class ChangePasswordComponent implements OnInit {
  logoLink: any;
  loading = false;
  logoLoad = true;

  public userId = '';
  public token = '';

  public changePasswordForm = this.fb.group({
    newPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16), Validators.pattern(new RegExp(RegexpPattern.password))]],
    confirmNewPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16), Validators.pattern(new RegExp(RegexpPattern.password))]]
  }, { validator: this.isMatch('newPassword', 'confirmNewPassword') });

  constructor(
    private fb: FormBuilder,
    private accountService: AccountService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appState: AppStateService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.userId = params['userId'];
      this.token = params['token'];
    });
  }

  private isMatch(first, second) {
    return (group: FormGroup): { [key: string]: any } => {
      if ((group.controls[first].value !== group.controls[second].value) &&
        ((group.controls[first].value !== '' && group.controls[first].value !== null) && (group.controls[second].value !== '' && group.controls[second].value !== null))
      ) {
        return {
          isMatch: true
        };
      }
      return null;
    };
  }

  ngOnInit() {
    this.logoLoad = true;
    this.appState.getTenantLogo().subscribe(result => {
      this.logoLink = result.logoPath;
      this.logoLoad = false;
    }, error => {
      this.logoLoad = false;
      this.logoLink = null;
    });
  }

  public setNewPassword() {
    const model = {
      newPassword: this.changePasswordForm.value.newPassword,
      confirmPassword: this.changePasswordForm.value.confirmNewPassword,
      token: this.token,
      userId: this.userId
    };
    this.accountService.setNewPassword(model).subscribe(data => {
      this.router.navigateByUrl('/account/login');
    });
  }

  public getErrorMessage() {
    return this.changePasswordForm.controls.confirmNewPassword.hasError('required') ? 'You must enter a value' :
      this.changePasswordForm.hasError('isMatch') ? ' Passwords do not match' : '';
  }
}
