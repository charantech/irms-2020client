import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestPersonalInfoComponent } from './event-guest-personal-info.component';

describe('EventGuestPersonalInfoComponent', () => {
  let component: EventGuestPersonalInfoComponent;
  let fixture: ComponentFixture<EventGuestPersonalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestPersonalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestPersonalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
