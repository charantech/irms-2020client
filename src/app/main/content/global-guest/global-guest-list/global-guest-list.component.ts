import { Component, OnInit, OnDestroy } from '@angular/core';
import { SectionListComponent } from '../../components/shared/section/section-list/section-list.component';
import { GlobalGuestModel } from '../global-guest.model';
import { GlobalGuestService } from '../global-guest.service';
import { GlobalGuestDataService } from '../global-guest-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Section } from 'app/constants/constants';
import { fuseAnimations } from '@fuse/animations';
import { GlobalGuestListItemComponent } from './global-guest-list-item/global-guest-list-item.component';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'irms-global-guest-list',
  templateUrl: './global-guest-list.component.html',
  styleUrls: ['./global-guest-list.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestListComponent extends SectionListComponent<GlobalGuestModel> implements OnInit, OnDestroy {
  public childComponent: Object = GlobalGuestListItemComponent;
  public selectable = false;
  listHeaders = ['Name', 'Count', 'Event', ' Campaign', 'Last Updated By', 'Updated Date', 'Actions'];
  count = 0;
  constructor(public sectionService: GlobalGuestService,
              protected dataService: GlobalGuestDataService,
              router: Router, public route: ActivatedRoute, private dialog: MatDialog) {
  super(Section.GlobalGuest, sectionService, dataService, router);
}

  ngOnInit() {
    this.sectionService.setFilter({});

    this.dataService.getGlobalGuestsCount().subscribe(result => this.count = result);
    super.ngOnInit();
  }

  loadGuests(): Observable<any> {
    return this.dataService.getGlobalGuestsList(this.sectionService.filterExtension);
  }

  searchQueryChange(event): void {
    this.sectionService.resetQueryString(event);
  }

  ngOnDestroy(): void{
    super.ngOnDestroy();
  }

}
