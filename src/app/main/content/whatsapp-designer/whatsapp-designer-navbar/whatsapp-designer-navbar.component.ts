import { Component, OnInit } from '@angular/core';
import { RsvpFormService } from '../../components/shared/rsvp-form/rsvp-form.service';
import { WhatsappDesignerService } from '../whatsapp-designer.service';
import { WhatsappDesignerDataService } from '../whatsapp-designer-data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { WhatsappDesignerNavigation, WhatsappDesignerNavigationWithoutRsvp } from './whatsapp-designer-navigation';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';
import { Location } from '@angular/common';
import { WhatsappDesignerSendTestMessageComponent } from '../whatsapp-designer-send-test-message/whatsapp-designer-send-test-message.component';
import { EventGuestService } from '../../event/event-layout/event-guest/event-guest.service';

@Component({
  selector: 'irms-whatsapp-designer-navbar',
  templateUrl: './whatsapp-designer-navbar.component.html',
  styleUrls: ['./whatsapp-designer-navbar.component.scss']
})
export class WhatsappDesignerNavbarComponent implements OnInit {

  navigation;
  invitationId: string;
  templateId: string;
  model;
  saveEnabled: boolean;

  constructor(
    private previewService: RsvpFormService,
    private WhatsappService: WhatsappDesignerService,
    private dataService: WhatsappDesignerDataService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    protected location: Location,
    protected designerService: WhatsappDesignerService,
    private guestService: EventGuestService
  ) {
    if (this.router.url.includes('rsvp-form')) {
      this.navigation = WhatsappDesignerNavigation;
    } else {
      this.navigation = WhatsappDesignerNavigationWithoutRsvp;
    }
    this.route.params.subscribe((params: Params) => {

      this.invitationId = params['tempId'];

      this.designerService.invitationId.next(this.invitationId);

      this.WhatsappService.fieldsFilled.subscribe(x => {
        this.saveEnabled = x;
      });

      if (!this.invitationId) {
        this.invitationId = '00000000-0000-0000-0000-000000000000';
      }

      this.dataService.get(this.invitationId).subscribe(m => {
        this.model = m;
        if (m) {
          this.templateId = this.model.id;

          let whatsapp = this.WhatsappService.template;
          whatsapp.senderName = m.senderName,
            whatsapp.body = m.body;
          this.WhatsappService.setTemplate(whatsapp);

          const backgroundImage = m.backgroundImagePath;

          const formTheme = JSON.parse(m.themeJson);

          let themeJson = null;

          if (formTheme) {
            themeJson = {
              buttons: formTheme.buttons,
              background: {
                color: formTheme.background.color,
                style: formTheme.background.style,
                image: backgroundImage
              }
            };
          } else {
            themeJson = {
              buttons: '',
              background: {
                color: '',
                style: '',
                image: '',
              }
            }
          }

          this.previewService.rvspFormValue = {
            formContent: {
              welcome: {
                content: m.welcomeHtml,
                button: m.proceedButtonText
              },
              rsvp: {
                content: m.rsvphtml,
                acceptButton: m.acceptButtonText,
                rejectButton: m.rejectButtonText
              },
              accept: {
                content: m.acceptHtml,
              },
              reject: {
                content: m.rejectHtml
              }
            },
            formTheme: themeJson,
            formSettings: {
              title: ''
            }
          }

          if (m.welcomeHtml || m.rsvphtml || m.acceptHtml || m.rejectHtml) {
            this.previewService.setRsvpForm(this.previewService.rvspFormValue);
            this.previewService.refreshFormTrigger();
          }
          else {
            this.initDefaultForm();
          }
        } else { //form was not loaded
          this.initDefaultForm();
        }
      });
    });
  }

  ngOnInit() {
  }

  private initDefaultForm() {
    let sms = this.WhatsappService.template;
    sms.senderName = '',
      sms.body = '';
    this.WhatsappService.setTemplate(sms);
    this.previewService.rvspFormValue = {
      formContent: {
        welcome: {
          content: `<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
              <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
              <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
              <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`,
          button: 'Proceed'
        },
        rsvp: {
          content: `<p><span style="font-family: tahoma, arial, helvetica, sans-serif;">Do you accept the Invitation to the&nbsp;</span></p>
              <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
              <p><span style="color: #34495e; font-family: 'arial black', sans-serif;">Select your Response:&nbsp;</span></p>`,
          acceptButton: `Yes I'll be there`,
          rejectButton: ` Nope I won't be there`
        },
        accept: {
          content: `<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
              <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
              <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
              <p style="text-align: center;">&nbsp;</p>`,
        },
        reject: {
          content: `<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
              <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
              <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
              <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`
        }
      },
      formTheme: {
        buttons: {
          background: '#576268',
          text: '#fff',
        },
        background: {
          color: '#fff',
          image: '',
          style: '',
        },
      },
      formSettings: {
        title: ''
      }
    };
    this.previewService.setRsvpForm(this.previewService.rvspFormValue);
  }

  onClickSend(): void {
    if (!this.templateId) {
      this.templateId = this.WhatsappService.templateId;
    }

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '60vw';
    dialogConfig.maxWidth = '80vw';
    dialogConfig.data = {
      invitationId: this.templateId
    }

    //Send Test WhatsApp
    const dialogRef = this.dialog.open(WhatsappDesignerSendTestMessageComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {

      });
  }

  onClickSave(): void {
    this.WhatsappService.triggerSave();
  }

  // Go back
  back(): void {
    this.location.back();
  }

}
