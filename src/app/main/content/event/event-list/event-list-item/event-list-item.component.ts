import { Component, OnInit, OnChanges, OnDestroy, Input, HostBinding, Type, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { transparentSource } from 'app/constants/constants';
import { Router } from '@angular/router';

@Component({
  selector: 'irms-event-list-item',
  templateUrl: './event-list-item.component.html',
  styleUrls: ['./event-list-item.component.scss']
})
export class EventListItemComponent {
  @Input() selectable: boolean;
  @Input() data;
  public disabled: any;
  public transparentSource = transparentSource;
  @Input() public dataListItemBody: Type<any>;

  constructor(private router: Router) { }

  doPathUrl(path): any {
    return `url(${path})`;
  }

  editTemplate() { }
}
