import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'irms-icon-rating',
  templateUrl: './icon-rating.component.html',
  styleUrls: ['./icon-rating.component.scss']
})
export class IconRatingComponent implements OnInit {
  @Input() theme: any;
  @Input() reviewMode = false;
  @Input() isArabic: boolean;
  @Input() question;
  @Input() public inputControl: FormControl;
  @Output() scrollNext = new EventEmitter<any>();
  @Output() fieldUpdate = new EventEmitter<string>();
  readyToRender = false;
  rateValue = 0;
  constructor() { }

  setAsCurrent() { }

  ngOnInit(): void{
    if (!this.reviewMode && this.question.hasOwnProperty('response')
      && this.question.response !== undefined 
      && this.question.response !== null 
      && this.question.response !== '' ) {
        this.rateValue = this.question.response;
        this.readyToRender = true; 
    } else if (this.reviewMode && this.inputControl.value !== null && this.inputControl.value !== undefined && this.inputControl.value !== '') { 
      this.rateValue = this.inputControl.value;
      this.readyToRender = true; 
    } else { 
      this.rateValue = 0;
      this.readyToRender = true; 
    }

    
    

    if ( this.question.required) {
      this.inputControl.setValidators([Validators.required]);
    }
  }
  onRate(event): void {
    this.inputControl.setValue(event.newValue);
    this.gotoNext();
  }


  /**
   * Go to next
   */
  gotoNext() {
    if (this.reviewMode) {
      return;
    }
    this.setAsCurrent();
    this.fieldUpdate.next();
    this.scrollNext.emit();
  }

}
