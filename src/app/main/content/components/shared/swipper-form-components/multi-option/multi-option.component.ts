import { Component, OnInit, Input, EventEmitter, Output, HostListener } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { FilterPipe } from './filter.pipe';
@Component({
  selector: 'irms-multi-option',
  templateUrl: './multi-option.component.html',
  styleUrls: ['./multi-option.component.scss']
})
export class MultiOptionComponent implements OnInit {
  @Input() theme: any;
  @Input() isArabic: boolean;
  @Input() question;
  @Input() reviewMode = false;
  @Input() public inputControl: FormControl;
  selectSearchControl: FormControl = new FormControl();
  @Output() scrollNext = new EventEmitter<any>();

  @Output() fieldUpdate = new EventEmitter<string>();
  searchText: any;
  /**
   * Selected Options
   */
  selectedOptions = [];

  constructor() { }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent): void {
      if (event.keyCode === 13 || event.keyCode === 9) {
        event.preventDefault();
        this.gotoNext();
      }
  }

  ngOnInit(): void{
    if ( this.question.required) {
      this.inputControl.setValidators([Validators.required]);
    }
    if (this.question.hasOwnProperty('response')
      && this.question.response !== undefined 
      && this.question.response !== null 
      && this.question.response !== '' ) {
        this.preSelect(this.question.response);
    }
    if (this.reviewMode && this.inputControl.value !== null && this.inputControl.value !== undefined && this.inputControl.value !== '') { 
      this.preSelect(this.inputControl.value);
    }
  }

  preSelect(selected): void {
    selected.forEach(preSelected => {
      const choice = this.question.choices.find(item => item.value === preSelected);
      if (choice) {
        this.selectedOptions.push(choice);
      }
    });
    
  }

  /**
   * Add to list os selected option
   * @param option
   */
  addToSelectedOptions(option): void {
    if (this.selectedOptions.indexOf(option) === -1) {
      this.selectedOptions.push(option);
    }
    this.validate();
  }

  /**
   * Remove Selected Option
   * @param option
   */
  removeOption(option) {
    this.selectedOptions = this.selectedOptions.filter(item => {
      return option !== item;
    });
    this.validate();
  }

  validate(): void{
    this.inputControl.markAsDirty() ;
    if (this.selectedOptions.length === 0 && this.question.required) {
      this.inputControl.setErrors({ required: true });
    } else {
      this.inputControl.setErrors(null);
    }
    this.updateValue();
    this.fieldUpdate.next();
  }
  updateValue(): void{
    const selections = [];
    this.selectedOptions.forEach(selection => selections.push(selection.value));
    this.inputControl.setValue(selections);
  }

  /**
   * Go to next question
   */
  gotoNext(): void{
    this.validate();
    if (this.reviewMode) {
      return;
    } else {
      this.scrollNext.emit();
    }
  }
}
