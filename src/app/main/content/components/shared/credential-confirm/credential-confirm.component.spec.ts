import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CredentialConfirmComponent } from './credential-confirm.component';

describe('CredentialConfirmComponent', () => {
  let component: CredentialConfirmComponent;
  let fixture: ComponentFixture<CredentialConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CredentialConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CredentialConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
