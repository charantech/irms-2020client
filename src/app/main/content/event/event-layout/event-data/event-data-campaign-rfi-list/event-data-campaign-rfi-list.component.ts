import { Component, OnInit, OnDestroy } from '@angular/core';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { EventDataModel } from '../event-data.model';
import { EventDataCampaignRfiListItemComponent } from '../event-data-campaign-rfi-list/event-data-campaign-rfi-list-item/event-data-campaign-rfi-list-item.component';
import { EventDataModService } from '../event-data.service';
import { EventDataDataService } from '../event-data.data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Section } from 'app/constants/constants';
import { of} from 'rxjs';
import { fuseAnimations } from 'app/core/animations';
import { DataModuleRfiDataService } from '../../../data-module-rfi.data.service';

@Component({
  selector: 'irms-event-data-campaign-rfi-list',
  templateUrl: './event-data-campaign-rfi-list.component.html',
  styleUrls: ['./event-data-campaign-rfi-list.component.scss'],
  animations: fuseAnimations
})
export class EventDataCampaignRfiListComponent extends SectionListComponent<EventDataModel> implements OnInit, OnDestroy {
  listHeaders = ['RFI Form', 'Targeted Guests', 'Responses', 'Unique Visits'];
  public listItemComponent: Object = EventDataCampaignRfiListItemComponent;
  eventId: string;
  campaignId: any;
  campaignName: any = 'Campaign name';
  rfiPage: any;
  constructor(public sectionService: EventDataModService,
              protected dataService: DataModuleRfiDataService,
              router: Router, public route: ActivatedRoute) {
    super(Section.EventDataRfi, sectionService, dataService, router);
  }

  ngOnInit() {
    this.sectionService.subscriptions = [];
    this.route.params.subscribe((params: Params) => {
      this.eventId = this.router.url.substring(8).substring(0, 36);
      this.campaignId = params.cid;

      this.rfiPage = {
        id: params.cid,
        pageSize: 5,
        pageNo: 1
      };

      this.dataService.getCampaignName({
        id: params.cid
      })
      .subscribe(result => {
        this.campaignName = result;
      });
      this.sectionService.setFilter({ });
    });
    this.getRfiList();
  }
  getRfiList() {
    this.dataService.getList(this.rfiPage)
      .subscribe(response => {
        this.data = of(response);
        this.sectionService.data.next(response);
        this.totalCount = response.totalCount;
      });
  }
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
