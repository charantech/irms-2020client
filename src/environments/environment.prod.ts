export const environment = {
    production: false,
    hmr       : false,
    apiUrl:   'https://irms-api.azurefd.net/'
};