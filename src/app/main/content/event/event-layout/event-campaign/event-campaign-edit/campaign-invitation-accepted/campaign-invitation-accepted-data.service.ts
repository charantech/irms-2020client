import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { EventCampaignModel } from '../../event-campaign.model';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationAcceptedDataService implements SectionDataService<EventCampaignModel> {

  private url = `${BASE_URL}api/CampaignInvitation`;
  constructor(private http: HttpClient) { }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/accepted/${id}`);
  }

  getList(filter: any): Observable<any> {
    return this.http.get(`${this.url}/${filter.campaignId}/accepted`);
  }

  @Toast('Successfully updated')
  create(model: any): Observable<string> {
    return this.http.post<string>(`${this.url}/accepted`, model);
  }

  @Toast('Successfully deleted')
  delete(model: any): Observable<any> {
    return this.http.post<string>(`${this.url}/delete`, model);
  }
  
  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
  @Toast('Successfully duplicated')
  duplicate(model: any): Observable<any> {
    return this.http.post<string>(`${this.url}/duplicate`, model);
  }
}
