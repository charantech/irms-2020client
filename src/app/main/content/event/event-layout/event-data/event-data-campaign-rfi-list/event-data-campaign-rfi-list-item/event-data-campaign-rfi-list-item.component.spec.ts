import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiListItemComponent } from './event-data-campaign-rfi-list-item.component';

describe('EventDataCampaignRfiListItemComponent', () => {
  let component: EventDataCampaignRfiListItemComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
