import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BASE_PAGE_SIZE, BASE_PAGE_SIZE_OPTIONS, Section } from 'app/constants/constants';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { of } from 'rxjs';
import { DataModuleDataService } from '../../../data-module-data.service';
import { DataModuleRfiDataService } from '../../../data-module-rfi.data.service';
import { EventDataModel } from '../event-data.model';
import { EventDataModService } from '../event-data.service';
import { EventDataListAnalysisCampaignListItemComponent } from './event-data-list-analysis-campaign-list-item/event-data-list-analysis-campaign-list-item.component';

@Component({
    selector: 'irms-event-data-list-analysis-campaign-list',
    templateUrl: './event-data-list-analysis-campaign-list.component.html',
    styleUrls: ['./event-data-list-analysis-campaign-list.component.scss']
})
export class EventDataListAnalysisCampaignListComponent  implements OnInit, OnDestroy {
    listHeaders = ['RFI Form', 'List Name', 'Responses', 'Unique Visits'];
    public pageSize = BASE_PAGE_SIZE;
    public pageSizeOptions = BASE_PAGE_SIZE_OPTIONS;
    public pageIndex = 0;
    public listItemComponent: Object = EventDataListAnalysisCampaignListItemComponent;
    eventId: string;
    campaignId: any;
    rfiPage: any;
    data: any;
    totalCount: any;
    constructor(public sectionService: EventDataModService,
                protected dataService: DataModuleRfiDataService,
                router: Router, public route: ActivatedRoute) {
    }

    changePage(event){
        let model = {
            eventId: this.eventId,
            pageNo: event.pageIndex+1,
            pageSize: event.pageSize
        }
        this.dataService.getListAnalysisRFIList(model).subscribe(x=>{
            if(x){
               this.data = of(x)
               this.rfiPage.pageSize = model.pageSize;
               this.rfiPage.pageNo = model.pageNo;
            }
            
            
        })
    }

    ngOnInit() {
        this.sectionService.subscriptions = [];
        this.route.params.subscribe((params: Params) => {
            this.eventId = params.id;
            this.rfiPage = {
                eventId: this.eventId,
                pageSize: 5,
                pageNo: 1
            };
            console.log(this.rfiPage)
            this.sectionService.setFilter({});
        });
        this.dataService.getListAnalysisRFIList(this.rfiPage).subscribe(x=>{
            this.data = of(x);
            this.totalCount = x.totalCount;
        })
    }
    ngOnDestroy(){

    }
}
