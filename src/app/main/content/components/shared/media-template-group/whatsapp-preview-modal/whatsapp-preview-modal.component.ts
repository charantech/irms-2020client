import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WhatsappDesignerModel } from 'app/main/content/whatsapp-designer/whatsapp-designer.model';

@Component({
  selector: 'irms-whatsapp-preview-modal',
  templateUrl: './whatsapp-preview-modal.component.html',
  styleUrls: ['./whatsapp-preview-modal.component.scss']
})
export class WhatsappPreviewModalComponent implements OnInit {

  sendTest = false;

  constructor(
    public dialogRef: MatDialogRef<WhatsappPreviewModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.data.messages = this.data.messages.replace(/\*(\S(.*?\S)?)\*/gm, '<strong>$1</strong>');
    this.data.messages = this.data.messages.replace(/\_(\S(.*?\S)?)\_/gm, '<em>$1</em>');
    this.data.messages = this.data.messages.replace(/\~(\S(.*?\S)?)\~/gm, '<strike>$1</strike>');
    this.data.messages = this.data.messages.replace(/\```(\S(.*?\S)?)\```/gm, '<code>$1</code>');
    this.data.messages = this.data.messages.replace(/\n/g, "<br>");
  }

  close() {
    this.dialogRef.close();
  }

}
