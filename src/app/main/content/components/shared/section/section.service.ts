import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BaseModel } from './base.model';
import { SectionDataService } from './section-data.service';
import { BASE_PAGE_SIZE } from '../../../../../constants/constants';
import { QueryService } from '../../../services/query.service';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { IResponseModel, IPage } from 'app/core/types/types';


export class SectionService<T extends BaseModel> {
  public assignMode = false;
  subscriptions: Subscription[] = [];
  public loading: boolean = false;

  constructor(protected dataService: SectionDataService<T>,
    protected queryService: QueryService,
    protected router: Router) {
    this.init();
  }

  public routeParams: any;

  public selectedItems: T[] = [];
  public selectedIds: string[] = [];

  public onCurrentChanged: BehaviorSubject<T> = new BehaviorSubject(null);
  public hasAllSelected = false;
  public isIndeterminate = false;
  public data: BehaviorSubject<IResponseModel> = new BehaviorSubject(null);

  public pageSize = BASE_PAGE_SIZE;
  public pageIndex = 0;
  public selectedItemId;
  public queryString = '';
  public isVisibleListBox = true;
  public isCreated = false;

  public filterExtension: object;

  public resetScroll: Subject<any> = new Subject<any>();
  public resetSubScroll: Subject<any> = new Subject<any>();

  public emitRefreshList: Subject<any> = new Subject<any>();

  init(): void {
    if (this.subscriptions.length === 0) {
      // this.subscriptions.push(this.queryService.queryString.subscribe(m => {
      //   this.queryString = m;
      //   this.pageIndex = 0;
      //   // this.refreshList();
      // }));

      this.router.events.subscribe(e => {
        if (e instanceof NavigationEnd) {
          let leaf = this.router.routerState.snapshot.root;
          while (leaf && leaf.children && leaf.children.length > 0 && Object.keys(leaf.params).length === 0) {
            leaf = leaf.children[0];
          }
          this.routeParams = leaf.params; // Top route with parameters
        }
      });

      this.subscriptions.push(this.onCurrentChanged.subscribe(v => {
        if (v) {
          this.resetSubScroll.next(true);
        }
      }));

      this.subscriptions.push(this.emitRefreshList.subscribe(() => {
        this.doRefreshList();
      }));
    }
  }

  refresh(): void {
    this.refreshList();
    this.refreshCurrentItem();
  }

  refreshList(): void {
    this.emitRefreshList.next(true);
  }

  doRefreshList(): void {
    const filter: any = this.getFilter();
    this.dataService.getList(filter).subscribe(data => {
      if (filter.createdId) {
        this.setFilter({ createdId: null });
      }
      this.data.next(data);
      this.handleRowCheckChange(this.selectedIds);
    },
      error => {
        console.log('error:', error);
      });
  }

  refreshCurrentItem(): void {
    if (this.onCurrentChanged.value) {
      this.dataService.get(this.onCurrentChanged.value.id).subscribe(data => {
        this.onCurrentChanged.next(data);
      });
    }
  }

  public handleRowCheckChange(selectedIds: string[]): void {
    let totalVisible = 0;
    if (selectedIds.length === 0) {
      this.selectedItems = [];
    }
    else {
      this.selectedItems = this.data.value.items.filter(i => selectedIds.indexOf(i.id) >= 0);
      totalVisible = this.data.value.items.length;
    }
    this.selectedIds = selectedIds;
    const totalSelected = this.selectedIds.length;
    this.hasAllSelected = totalSelected > 0 && totalVisible === totalSelected;
    this.isIndeterminate = totalSelected > 0 && totalSelected < totalVisible;
  }

  public getFilter(): IPage {
    return Object.assign({
      pageNo: this.pageIndex + 1,
      pageSize: this.pageSize,
      searchText: this.queryString
    },
      this.filterExtension);
  }

  public setFilter(filterExtension: object): void {
    this.resetScroll.next(true);
    this.filterExtension = filterExtension;
  }
  public resetQueryString(value): void {
    this.queryString = value;
    this.refreshList();
  }

  public extendFilter(filterExtension: object): void {
    this.resetScroll.next(true);
    this.filterExtension = Object.assign(filterExtension, this.filterExtension);
  }

  public resetFilter(): void {
    this.resetScroll.next(true);
    this.filterExtension = null;
    this.queryService.resetAllFilters();
    this.queryString = '';
  }

  public clearFilterValue(key: string): void {
    this.resetSelection();
    this.resetScroll.next(true);
    delete this.filterExtension[key];
  }

  public setFilterValue(key: string, value: any): void {
    if (this.filterExtension) {
      this.resetSelection();
      this.resetScroll.next(true);
      this.filterExtension[key] = value;
    }
  }
  public trimValues(model: any): any {
    model = Object.assign({}, model);
    for (const key in model) {
      if (model.hasOwnProperty(key)) {
        const value = model[key];
        const type = typeof value;
        if (value || (typeof value === 'boolean' && value === false)) {
          if (type === 'string' || type === 'object') {
            if (type === 'object') {
              this.trimValues(model[key]);
            }
            else {
              model[key] = model[key].trim();
            }
          }
        }
        else {
          delete model[key];
        }
      }
    }
    return model;
  }
  public resetSelection(): void {
    this.selectedItems = [];
    this.selectedIds = [];
  }

  public clearArray(array) {
    while (array.length) {
      array.pop();
    }
  }

  public dispose(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.filterExtension = null;
    this.hasAllSelected = false;
    this.isIndeterminate = false;
    this.selectedItems = [];
    this.selectedIds = [];
    this.resetSelection();
  }
}
