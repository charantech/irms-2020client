import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, NgZone } from '@angular/core';
import { SectionCreateComponent } from '../../components/shared/section/section-create/section-create.component';
import { EventModel } from '../event.model';
import { Validators, FormBuilder } from '@angular/forms';
import { RegexpPattern, Section, mapsStyle } from 'app/constants/constants';
import { EventService } from '../event.service';
import { EventDataService } from '../event-data.service';
import { Router } from '@angular/router';
import { AppStateService } from '../../services/app-state.service';
import { fuseAnimations } from '@fuse/animations';
import { MapsAPILoader } from '@agm/core';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'irms-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.scss'],
  animations: fuseAnimations
})
export class EventCreateComponent extends SectionCreateComponent<EventModel> implements OnInit {


  constructor(public sectionService: EventService,
              protected dataService: EventDataService,
              protected router: Router,
              private appStateService: AppStateService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private fb: FormBuilder) {
    super(Section.Events, sectionService, dataService, router);
  }

  @ViewChild('locationSearch', { read: ElementRef, static: false }) searchElementRef: ElementRef;
  public createForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
    eventTypeId: ['', [, Validators.maxLength(500)]],
    isLimitedAttendees: [true, [Validators.required]],
    maximumAttendees: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.digits))]],
    overflowAttendees: ['', [Validators.pattern(RegExp(RegexpPattern.digits))]],
    duration: ['', [Validators.required]],
    startDateTime: ['', [Validators.required]],
    endDateTime: ['', [Validators.required]],
    // timeZoneName: ['', [Validators.required]],
    location: ['', [Validators.required]],
    city: [''],
    region: [''],
    zipCode: [''],
    country: ['', [Validators.required]],
    image: [''],
    latitude: [''],
    longitude: ['']
  });
  // , { validator: this.reqMaxAttendees() });
  mapStyle = mapsStyle;
  public setupForm = this.fb.group({
    features: ['', [Validators.required]]
  });

  @Output() public handleCreateNew: EventEmitter<any> = new EventEmitter();
  // public timeZones: any[];
  public eventTypes: any[];
  public placeService: any;
  public placeServiceIsReady = false;
  autocomplete: any;
  lat = 26.302294;
  lng = 50.206738;
  public now = new Date();
  featuresList: any[];
  selectedTabIndex: any;
  marker: google.maps.Marker;
  country: google.maps.GeocoderAddressComponent;
  createSuccess = false;
  createLoading = false;

  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;

  ngOnInit() {
    this.loadLookups();

    this.createForm.get('isLimitedAttendees').valueChanges.takeUntil(this.componentDestroyed).subscribe((value: boolean) => {
      if (value) {
        this.setValidators('createForm', 'maximumAttendees', [Validators.required, Validators.pattern(RegExp(RegexpPattern.digits))]);
      } else {
        this.clearValidators('createForm', ['maximumAttendees']);
      }
    });

    // load google auto complete
    this.createForm.get('location').valueChanges
      .pipe(debounceTime(800),
        distinctUntilChanged())
      .subscribe((value) => {
        if (value !== null && this.placeServiceIsReady) {
          if (!this.autocomplete) {
            this.autocomplete = this.createAutocomplete();
          }
        }
      });

    this.loadGoogle();
  }

  loadLookups(): void {
    this.appStateService.getEventTypes().subscribe(result => this.eventTypes = result);
    // this.appStateService.getTimeZones().subscribe(result => this.timeZones = result);
    this.dataService.getFeatures().subscribe(data => this.featuresList = data);
  }

  durationChanged(event) {
    this.createForm.controls['startDateTime'].setValue(event.value[0].toISOString());
    this.createForm.controls['endDateTime'].setValue(event.value[1].toISOString());
  }
  
  deleteEventImage(): void{
    this.createForm.controls['image'].reset();
  }

  /////////////// geo auto complete//////////////////////
  loadGoogle() {
    this.mapsAPILoader.load().then(() => {
      this.placeService = new google.maps.places.AutocompleteService();
      this.placeServiceIsReady = true;
    });
  }

  createAutocomplete() {
    // load Places Autocomplete
    const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      // types: ['(cities)'],
    });

    autocomplete.addListener('place_changed', () => {
      this.ngZone.run(() => {
        // get the place result
        const place: google.maps.places.PlaceResult = autocomplete.getPlace();

        // verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.createForm.controls.city.setValue(place.formatted_address);
        this.lat = place.geometry.location.lat();
        this.lng = place.geometry.location.lng();
        const rsltAdrComponent = place.address_components;
        const location = place.formatted_address;
        const city = rsltAdrComponent.find(x => x.types.includes('locality')) ? rsltAdrComponent.find(x => x.types.includes('locality')).long_name : (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')) ? (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')).long_name) : undefined);
        const region = rsltAdrComponent.find(x => x.types.includes('administrative_area_level_1')).long_name;
        this.country = rsltAdrComponent.find(x => x.types.includes('country'));
        const postalCode = rsltAdrComponent.find(x => x.types.includes('postal_code')) ? rsltAdrComponent.find(x => x.types.includes('postal_code')).long_name : undefined;

        this.createForm.controls.location.setValue(location);
        this.createForm.controls.city.setValue(city);
        this.createForm.controls.region.setValue(region);
        this.createForm.controls.country.setValue(this.country.long_name);
        this.createForm.controls.zipCode.setValue(postalCode);
        this.createForm.controls.latitude.setValue(this.lat);
        this.createForm.controls.longitude.setValue(this.lng);
      });
    });

    return autocomplete;
  }

  getLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
    this.getGeoLocation(this.lat, this.lng);
  }

  getGeoLocation(lat: number, lng: number) {
    if (navigator.geolocation) {
      const geocoder = new google.maps.Geocoder();
      const latlng = new google.maps.LatLng(lat, lng);

      geocoder.geocode({ location: latlng }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          const result = results[0];
          const rsltAdrComponent = result.address_components;
          if (result != null) {
            const location = result.formatted_address;
            const city = rsltAdrComponent.find(x => x.types.includes('locality')) ? rsltAdrComponent.find(x => x.types.includes('locality')).long_name : (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')) ? (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')).long_name) : undefined);
            const region = rsltAdrComponent.find(x => x.types.includes('administrative_area_level_1')).long_name;
            this.country = rsltAdrComponent.find(x => x.types.includes('country'));
            const postalCode = rsltAdrComponent.find(x => x.types.includes('postal_code')) ? rsltAdrComponent.find(x => x.types.includes('postal_code')).long_name : undefined;

            this.createForm.controls.location.setValue(location);
            this.createForm.controls.city.setValue(city);
            this.createForm.controls.region.setValue(region);
            this.createForm.controls.country.setValue(this.country.long_name);
            this.createForm.controls.zipCode.setValue(postalCode);
            this.createForm.controls.latitude.setValue(lat);
            this.createForm.controls.longitude.setValue(lng);

          } else {
            alert('No address available!');
          }
        }
      });
    }
  }

  ////////////// create event methods////////////////////////
  create() {
    this.createLoading = true;
    let model: any = {};
    if (this.createForm.dirty && this.createForm.valid && this.setupForm.valid) {
      const eventInfo = this.sectionService.trimValues(this.createForm.value);
      eventInfo.country = this.country.short_name;
      eventInfo.locationName = eventInfo.location;
      // eventInfo.timeZoneUtcOffset = eventInfo.timeZoneName.id;
      // eventInfo.timeZoneName = eventInfo.timeZoneName.value;
      eventInfo.eventTypeId = eventInfo.eventTypeId ? eventInfo.eventTypeId.id : '';
      delete eventInfo.location;
      delete eventInfo.duration;

      const features = this.sectionService.trimValues(this.setupForm.value);
      model = { ...eventInfo, ...features };
      model = this.sectionService.trimValues(model);

      const fd = new FormData();
      for (const key in model) {
        fd.append(key, model[key]);
      }

      this.dataService.create(fd).subscribe(() => {
        this.createLoading = false;
        this.selectedTabIndex = 2;
        this.createSuccess = true;
      }, () => {
        this.createLoading = false;
      }
      );
    }
  }
  goToEvents(): void {
    this.router.navigate([Section.Events]);
  }

  ////////////////// Select Features////////////////////
  featuresChange(event) {
    this.setupForm.controls['features'].setValue(event);
  }
}
