import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';
import { GlobalContactDataService } from '../../global-contact-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { GlobalContactService } from '../../global-contact.service';
import { GlobalContactListItemRenameModalComponent } from './global-contact-list-item-rename-modal/global-contact-list-item-rename-modal.component';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';
import { GlobalContactExportToGuestModalComponent } from '../../global-contact-export-to-guest-modal/global-contact-export-to-guest-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { sideDialogConfig } from 'app/constants/constants';

@Component({
  selector: 'irms-global-contact-list-item',
  templateUrl: './global-contact-list-item.component.html',
  styleUrls: ['./global-contact-list-item.component.scss']
})
export class GlobalContactListItemComponent {
  @Input() data: any;

  @Output() updateEvent: EventEmitter<any> = new EventEmitter();
  constructor(
    private dialog: DialogAnimationService,
    private matDialog: MatDialog,
    private sectionService: GlobalContactService,
    private dataService: GlobalContactDataService,
    private route: ActivatedRoute
  ) {
  }

  public rename(): void {
    const dialogConfig: any = {}
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '20vw';
    dialogConfig.data = {
      id: this.data.id,
      name: this.data.name
    };

    const dialogRef = this.dialog.open(GlobalContactListItemRenameModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result && result.name && result.name !== this.data.name) {
          this.dataService.renameList({
            id: this.data.id,
            name: result.name
          })
            .subscribe(_ => {
              this.sectionService.doRefreshList();
            });
        }
      });
  }

  @PopUp('Are you sure want to delete list?')
  public delete(): void {
    const toasterService = appInjector().get(ToasterService);

    this.dataService.deleteGlobalList(this.data.id)
      .subscribe(result => {
        if (result) {
          toasterService.pop('success', null, 'Successfully deleted.');
        } else {
          toasterService.pop('error', 'This list can not be deleted!', 'This list can’t be deleted because it is in use with a campaign.\n'
            + 'Please disassociate this list from this association before attempting to delete.');
        }
        this.sectionService.doRefreshList();
      });
  }

  public addToList(listId): void {
    const dialogConfig : any = sideDialogConfig;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '50vw';
    
    const dialogRef = this.dialog.open(GlobalContactExportToGuestModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        const request = {
          fromListId: listId,
          toListId: data.guestListId
        };
        //API Call below
        this.dataService.appendContactListToGuestsList(request)
          .subscribe(result => {
          });

      }
    });

  }

  public export(): void {
    const dialogConfig: any = sideDialogConfig;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      isFromContactModule : true
    };
    const dialogRef = this.dialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const columns = result.columns;
          const customFieldIds = result.customFieldIds;

          this.dataService
            .exportContacts({
              contactListId: this.data.id,
              filename: this.data.name,
              columns: columns,
              customFieldIds: customFieldIds
            }).subscribe(resp => {
              this.saveExportedContacts(resp);
            });
        }
      });
  }

  private saveExportedContacts(resp: any): any {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.data.name}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

}
