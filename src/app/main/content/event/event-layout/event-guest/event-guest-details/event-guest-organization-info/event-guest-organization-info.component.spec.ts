import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestOrganizationInfoComponent } from './event-guest-organization-info.component';

describe('EventGuestOrganizationInfoComponent', () => {
  let component: EventGuestOrganizationInfoComponent;
  let fixture: ComponentFixture<EventGuestOrganizationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestOrganizationInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestOrganizationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
