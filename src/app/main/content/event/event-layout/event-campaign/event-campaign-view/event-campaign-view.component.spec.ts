import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignViewComponent } from './event-campaign-view.component';

describe('EventCampaignViewComponent', () => {
  let component: EventCampaignViewComponent;
  let fixture: ComponentFixture<EventCampaignViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
