import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailLandingDesignComponent } from './email-landing-design.component';

describe('EmailLandingDesignComponent', () => {
  let component: EmailLandingDesignComponent;
  let fixture: ComponentFixture<EmailLandingDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailLandingDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLandingDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
