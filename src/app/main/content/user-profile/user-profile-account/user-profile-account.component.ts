import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Validators, FormBuilder } from '@angular/forms';
import { RegexpPattern, sideDialogConfig } from 'app/constants/constants';
import { UserProfileService } from '../user-profile.service';
import { UserProfileDataService } from '../user-profile-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppStateService } from '../../services/app-state.service';
import { CurrentUserService } from '../../services/current-user.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CredentialConfirmComponent } from '../../components/shared/credential-confirm/credential-confirm.component';
import { UserProfileAccountChangePasswordModalComponent } from './user-profile-account-change-password-modal/user-profile-account-change-password-modal.component';
import { DialogAnimationService } from '../../services/dialog-animation.service';

@Component({
  selector: 'irms-user-profile-account',
  templateUrl: './user-profile-account.component.html',
  styleUrls: ['./user-profile-account.component.scss'],
  animations: fuseAnimations
})
export class UserProfileAccountComponent implements OnInit {

  public userInfoForm = this.fb.group({
    fullName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    email: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.email))]],
    mobileNumber: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.phone))]],
    twoFactorAuth: ['']
  });

  editInfoMode: boolean = false;
  loading: boolean;
  model = {
    fullName: "John Doe",
    email: "email@example.com",
    mobileNumber: "0512345678",
    twoFactorAuth: false
  };

  constructor(
    public sectionService: UserProfileService,
    protected dataService: UserProfileDataService,
    protected router: Router,
    protected route: ActivatedRoute,
    private appStateService: AppStateService,
    protected fb: FormBuilder,
    public currentUser: CurrentUserService,
    private dialog: MatDialog,
    public dialogRef: DialogAnimationService) {

  }

  ngOnInit(): void {
    this.userInfoForm.patchValue(this.model);
    this.userInfoForm.controls.email.disable();
    this.userInfoForm.controls.mobileNumber.disable();
    this.currentUser.getCurrentUser().subscribe(data => {
      //API here to get user information
      if (data) {
        this.model = data;
        
      }
    });
  }

  updateUserInfo() {
    if (this.userInfoForm.controls.isValid) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      const dialogRef = this.dialog.open(CredentialConfirmComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {

        }
      });
      //API Here to update user information
      this.dataService.updateUserInfo(this.userInfoForm.controls.value).subscribe(data => {
        if (this.userInfoForm.controls.email.dirty || this.userInfoForm.controls.mobileNumber.dirty) {



        }
      });
    }
  }


  public toggleMode(): void {
    this.editInfoMode = !this.editInfoMode;
  }

  changePassword(){
    const dialogConfig:any  = sideDialogConfig;
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      const dialogRef = this.dialogRef.open(UserProfileAccountChangePasswordModalComponent, dialogConfig);

  }

  checkPhone(control) {
    if (control.valid && control.dirty) {
      this.dataService.checkPhoneUniqueness({ mobileNumber: control.value }).subscribe((res: any) => {
        if (res) {
          control.setErrors({ notUnique: true });
        } else {
          this.userInfoForm.updateValueAndValidity();
        }
      });
    }
  }

  checkEmail(control) {
    if (control.valid && control.dirty) {
      this.dataService.checkEmailUniqueness({ email: control.value }).subscribe((res: any) => {
        if (res) {
          control.setErrors({ notUnique: true });
        } else {
          this.userInfoForm.updateValueAndValidity();
        }
      });
    }
  }
}
