import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptedViewMessageConfigComponent } from './accepted-view-message-config.component';

describe('AcceptedViewMessageConfigComponent', () => {
  let component: AcceptedViewMessageConfigComponent;
  let fixture: ComponentFixture<AcceptedViewMessageConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptedViewMessageConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptedViewMessageConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
