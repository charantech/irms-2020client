import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignViewInvitationPendingComponent } from './campaign-view-invitation-pending.component';

describe('CampaignViewInvitationPendingComponent', () => {
  let component: CampaignViewInvitationPendingComponent;
  let fixture: ComponentFixture<CampaignViewInvitationPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignViewInvitationPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewInvitationPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
