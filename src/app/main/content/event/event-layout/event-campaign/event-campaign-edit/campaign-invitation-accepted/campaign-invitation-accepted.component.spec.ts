import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignInvitationAcceptedComponent } from './campaign-invitation-accepted.component';

describe('CampaignInvitationAcceptedComponent', () => {
  let component: CampaignInvitationAcceptedComponent;
  let fixture: ComponentFixture<CampaignInvitationAcceptedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignInvitationAcceptedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignInvitationAcceptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
