import { Component, OnInit, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, Validators } from '@angular/forms';
import { SectionViewEditComponent } from '../section/section-view-edit/section-view-edit.component';
import { BaseModel } from '../section/base.model';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { Subscription } from 'rxjs';
import { FormService } from '../form/form.service';
import { DataReviewFormBuilderService } from './data-review-form-builder.service';
import { DataReviewFormBuilderDataService } from './data-review-form-builder-data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DataReviewFormDesignerService } from 'app/main/content/data-review-form-designer/data-review-form-designer.service';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { Section } from 'app/constants/constants';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'irms-data-review-form-builder',
  templateUrl: './data-review-form-builder.component.html',
  styleUrls: ['./data-review-form-builder.component.scss'],
  animations: fuseAnimations
})
export class DataReviewFormBuilderComponent extends SectionViewEditComponent<BaseModel> implements OnInit, AfterViewInit, OnDestroy, CanExit {
  selectedTab = 0;
  public form = this.fb.group(
    {
      formContent: this.fb.group({
        welcome: this.fb.group({
          content: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
      <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">
      Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
          contentArabic: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
      <h5 style="text-align: left;"><span style="color: #34495e;"><em>
      <span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
          button: ['Proceed'],
          buttonArabic: ['تقدم']
        }),
        questions: this.fb.array([]),
        submit: this.fb.group({
          submitMessage: `Everything seems good,\nYou can submit now!`,
          submitMessageArabic: `جميع البيانات صحيحة،\n قم بتقديم الطلب الآن!`,
          submitButton: 'Submit',
          submitButtonArabic: 'تقدم'
        }),
        thanks: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`],
          contentArabic: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`]
        })
      }),
      formTheme: this.fb.group({
        buttons: this.fb.group({
          background: '#576268',
          text: '#fff',
        }),
        questions: this.fb.group({
          color: '#576268'
        }),
        answers: this.fb.group({
          color: '#576268'
        }),
        background: this.fb.group({
          color: '#fff',
          image: '',
          style: '',
        }),
      }),
      formSettings: this.fb.group({
        title: ['Personal Information'],
        defaultLanguage: 'en',
        languageOption: false
      })
    });
  @Input() invitationId: any;
  @Input() questions;
  @Input() formId: string;
  subscriptions: Subscription[] = [];
  listId: any;
  @Input() importantFields: any;

  constructor(private fb: FormBuilder,
              protected preview: FormService,
              public sectionService: DataReviewFormBuilderService,
              protected dataService: DataReviewFormBuilderDataService,
              protected route: ActivatedRoute,
              protected router: Router,
              protected designerService: DataReviewFormDesignerService,
              private dialog: MatDialog,
              public dialogRef: MatDialogRef<DataReviewFormBuilderComponent>
  ) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
    this.subscriptions.push(this.designerService.saveObs().subscribe(() => this.saveRfiForm()));
  }

  ngOnInit(): void {
    
    this.sectionService.importantFields = this.importantFields;
    this.subscriptions.push(this.sectionService.getFormContent().subscribe(val => { this.form.patchValue(val); }));
    
    this.dataService.get(this.invitationId).subscribe(m => {
      this.model = m;
      if (m) {
        this.model['rfiFormQuestions'] = m.rfiFormQuestions.sort((obj1, obj2) => {
          if (obj1.sortOrder > obj2.sortOrder) {
            return 1;
          }

          if (obj1.sortOrder < obj2.sortOrder) {
            return -1;
          }

          return 0;
        });
        this.patchModelInForms();
      } else {
        this.patchQuestions(this.questions);
      }
    });
    this.preview.setForm(this.form.value);
    this.form.valueChanges.subscribe(data => {
      this.preview.setForm(data);
    });
  }


  ngAfterViewInit(): void {
    this.preview.setForm(this.form.value);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.form = this.fb.group(
      {
        formContent: this.fb.group({
          welcome: this.fb.group({
            content: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
        <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
        <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">
        Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
            contentArabic: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
        <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
        <h5 style="text-align: left;"><span style="color: #34495e;"><em>
        <span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
            button: ['Proceed'],
            buttonArabic: ['تقدم']
          }),
          questions: this.fb.array([]),
          submit: this.fb.group({
            submitMessage: `Everything seems good,\nYou can submit now!`,
            submitMessageArabic: `جميع البيانات صحيحة،\n قم بتقديم الطلب الآن!`,
            submitButton: 'Submit',
            submitButtonArabic: 'تقدم'
          }),
          thanks: this.fb.group({
            content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
        <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
        <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: center;">&nbsp;</p>`],
            contentArabic: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
        <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
        <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
        <p style="text-align: center;">&nbsp;</p>`]
          })
        }),
        formTheme: this.fb.group({
          buttons: this.fb.group({
            background: '#576268',
            text: '#fff',
          }),
          questions: this.fb.group({
            color: '#576268'
          }),
          answers: this.fb.group({
            color: '#576268'
          }),
          background: this.fb.group({
            color: '#fff',
            image: '',
            style: '',
          }),
        }),
        formSettings: this.fb.group({
          title: ['hello form'],
          defaultLanguage: 'en',
          languageOption: false
        })
      });
  }

  refreshFormPreview(): void {
    this.preview.refreshFormTrigger();
  }

  patchModelInForms(): void {

    this.form.controls['formContent']['controls'].welcome.patchValue(JSON.parse(this.model['welcomeHtml']));
    this.form.controls['formContent']['controls'].submit.patchValue(JSON.parse(this.model['submitHtml']));
    this.form.controls['formContent']['controls'].thanks.patchValue(JSON.parse(this.model['thanksHtml']));
    this.form.controls['formContent']['controls'].submit.patchValue(JSON.parse(this.model['submitHtml']));
    this.form.controls['formTheme']['controls'].buttons.patchValue(JSON.parse(this.model['formTheme']).buttons);
    this.form.controls['formTheme']['controls'].questions.patchValue(JSON.parse(this.model['formTheme']).questions);
    this.form.controls['formTheme']['controls'].answers.patchValue(JSON.parse(this.model['formTheme']).answers);
    this.form.controls['formTheme']['controls'].background.controls.color.setValue(JSON.parse(this.model['formTheme']).background.color);
    this.form.controls['formTheme']['controls'].background.controls.style.setValue(JSON.parse(this.model['formTheme']).background.style);
    this.form.controls.formSettings.patchValue(JSON.parse(this.model['formSettings']));

    // patch image
    if (this.model['themeBackgroundImagePath']) {
      this.form.controls['formTheme']['controls'].background.controls.image.setValue(this.model['themeBackgroundImagePath']);
    }

    // patch questions
    this.patchQuestions(this.model['rfiFormQuestions']);
    // 

  }

  patchQuestions(questions): void {
    const mappedField = [];
    questions.forEach(q => {
      const parsedQ = JSON.parse(q.question);
      this.addQuestion(parsedQ);
      if (parsedQ.mapping) {
        mappedField.push({
          type: parsedQ.type,
          field: parsedQ.mappingField
        });
      }      
    });    
    
    this.sectionService.setSelectedFields(mappedField);
    this.sectionService.setIsFormLoaded(true);
    this.isDataLoaded = true;
  }

  addQuestion(ques): void {
    // add choices if question type is select/search
    if (ques.type === 'select' || ques.type === 'selectSearch') {
      if (!ques.choices) {
        ques.choices = [];
      }
      this.addChoices(ques, ques.choices);
    }

    this.form.controls['formContent']['controls'].questions.insert(this.form.controls['formContent']['controls'].questions.length, this.fb.group(ques));
  }

  addChoices(ques, choices): void {
    ques.choices = this.fb.array([]);
    if (choices && choices.length > 0) {
      choices.forEach(ch => {
        ques.choices.insert(ch.value, this.fb.group({
          label: ch.label,
          labelArabic: ch.labelArabic,
          value: ch.value,
          selected: ch.selected
        }));
      });
    }
    
  }

  canDeactivate(): any {
    if (this.form.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

  close() {
    this.dialogRef.close();
  }

  saveRfiForm(): void {
    const formValue = this.form.value;
    if (this.form.valid) {

      const allQuestions = formValue.formContent.questions;
      const questions = [];
      allQuestions.forEach((q, i) => {
        // fallback for validations in question object
        if (this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('minLength')) {
          q.minLength = this.form.controls['formContent']['controls'].questions.controls[i].controls.minLength.value;
          q.maxLength = this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('maxLength') ? this.form.controls['formContent']['controls'].questions.controls[i].controls.maxLength.value : 50;
        } else if (this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('minDate')) {
          q.minDate = this.form.controls['formContent']['controls'].questions.controls[i].controls.minDate.value;
          q.maxDate = this.form.controls['formContent']['controls'].questions.controls[i].controls.maxDate.value;
        }
        if (this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('pattern')) {
          q.pattern = this.form.controls['formContent']['controls'].questions.controls[i].controls.pattern.value;
        }

        questions.push({
          id: q.id,
          question: JSON.stringify(q),
          mapped: q.mapping,
          mappedField: q.mappingField,
          sortorder: (i + 1)
        });
      });

      let rfi = {
        id: this.formId,
        campaignInvitationId: this.invitationId,
        welcomeHtml: JSON.stringify(this.form.controls['formContent']['controls'].welcome.value),
        submitHtml: JSON.stringify(this.form.controls['formContent']['controls'].submit.value),
        thanksHtml: JSON.stringify(this.form.controls['formContent']['controls'].thanks.value),
        formTheme: JSON.stringify({
          buttons: this.form.controls['formTheme']['controls'].buttons.value,
          questions: this.form.controls['formTheme']['controls'].questions.value,
          answers: this.form.controls['formTheme']['controls'].answers.value,
          background: {
            color: this.form.controls['formTheme']['controls'].background.controls.color.value,
            style: this.form.controls['formTheme']['controls'].background.controls.style.value
          }
        }),

        formSettings: JSON.stringify(this.form.controls['formSettings'].value),
        themeBackgroundImage: this.form.controls['formTheme']['controls'].background.controls.image.value,
        questionJson: JSON.stringify(questions)
      };

      rfi = this.sectionService.trimValues(rfi);
      const fd = new FormData();
      for (const key in rfi) {
        fd.append(key, rfi[key]);
      }
      this.dataService.upsert(fd).subscribe(id => {
        this.formId = id.toString();
      });
    }
  }
}
