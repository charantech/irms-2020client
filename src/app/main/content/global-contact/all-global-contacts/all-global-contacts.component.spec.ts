import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGlobalContactsComponent } from './global-contact-list-contacts.component';

describe('AllGlobalContactsComponent', () => {
  let component: AllGlobalContactsComponent;
  let fixture: ComponentFixture<AllGlobalContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllGlobalContactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGlobalContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
