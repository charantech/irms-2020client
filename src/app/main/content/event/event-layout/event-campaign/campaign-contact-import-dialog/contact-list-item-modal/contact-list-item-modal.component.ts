import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-contact-list-item-modal',
  templateUrl: './contact-list-item-modal.component.html',
  styleUrls: ['./contact-list-item-modal.component.scss']
})
export class ContactListItemModalComponent implements OnInit {

  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
