import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'irms-rfi-data-review-modal',
  templateUrl: './rfi-data-review-modal.component.html',
  styleUrls: ['./rfi-data-review-modal.component.scss']
})
export class RfiDataReviewModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RfiDataReviewModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    
  }

}
