import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-empty-list-message',
  templateUrl: './empty-list-message.component.html',
  styleUrls: ['./empty-list-message.component.scss']
})
export class EmptyListMessageComponent implements OnInit {

  @Input() heading = 'No records';
  @Input() secondaryText = '';
  @Input() type: string;
  constructor() { }

  ngOnInit() {
  }

}
