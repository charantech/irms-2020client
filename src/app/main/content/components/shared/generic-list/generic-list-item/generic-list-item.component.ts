import {
  Component, EventEmitter, HostBinding, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges,
  Type
} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {GenericListService} from '../generic-list.service';
import {fuseAnimations} from '@fuse/animations';


@Component({
  selector: 'irms-generic-list-item',
  templateUrl: './generic-list-item.component.html',
  styleUrls: ['./generic-list-item.component.scss'],
  animations: fuseAnimations
})
export class GenericListItemComponent implements OnInit, OnChanges, OnDestroy {
  @Input() selectable: boolean;
  @Input() data;
  @HostBinding('class.selected') selected: boolean;
  @Input() public dataListItemBody: Type<any>;
  @Input() selectedIds;
  onSelectedlistItemChanged: Subscription;
  @Output() public onSelectItem: EventEmitter<Event> = new EventEmitter();

  constructor(private genericListService: GenericListService) {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnInit() {
    // Set the initial values
    // Subscribe to update on selected mail change
    this.onSelectedlistItemChanged =
      this.genericListService.onCurrentIdsSelectesChanged
        .subscribe(selectedIds => {
          this.selected = false;
          if (selectedIds.length > 0) {
            for (const id of selectedIds) {
              if (id === this.data.id) {
                this.selected = true;
                break;
              }
            }
          }
        });
  }

  ngOnDestroy() {
    this.onSelectedlistItemChanged.unsubscribe();
  }

  onSelectedChange(item) {
    // this.genericListService.toggleSelectedId(this.data.id);
    this.onSelectItem.emit(item);
  }

}
