import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwipperMatSelectSearchExtComponent } from './swipper-mat-select-search-ext.component';

describe('SwipperMatSelectSearchExtComponent', () => {
  let component: SwipperMatSelectSearchExtComponent;
  let fixture: ComponentFixture<SwipperMatSelectSearchExtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwipperMatSelectSearchExtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwipperMatSelectSearchExtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
