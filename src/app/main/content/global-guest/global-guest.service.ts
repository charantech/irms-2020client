import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { GlobalGuestDataService } from './global-guest-data.service';
import { QueryService } from '../services/query.service';
import { Router } from '@angular/router';
import { SectionService } from '../components/shared/section/section.service';
import { GlobalGuestModel } from './global-guest.model';

@Injectable({
  providedIn: 'root'
})
export class GlobalGuestService extends SectionService<GlobalGuestModel>{
  setSelectedGuestName(name: string): void {
    this.selectedGuestName.next(name);
  }
  private selectedGuestName = new BehaviorSubject<any>('');

  constructor(
    protected dataService:GlobalGuestDataService,
    queryService:QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
   }

   public getSelectedGuestName(): Observable<any>{
     return this.selectedGuestName.asObservable();
   }
   public setSelectedContactName(name: string):void{
     return this.selectedGuestName.next(name);
   }
}
