import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  private form = new Subject<any>();
  private refreshForm = new Subject<any>();
  private currentSection = new Subject<any>();
  private currentQuestion = new Subject<any>();
  constructor() { }

  getForm(): Observable<any> {
    return this.form.asObservable();
  }
  setForm(questionnaire): void{
    this.form.next(questionnaire);
  }

  getRefreshFormTrigger(): Observable<any> {
    return this.refreshForm.asObservable();
  }
  refreshFormTrigger(): void{
    this.refreshForm.next(true);
  }

  getCurrentSection(): Observable<any> {
    return this.currentSection.asObservable();
  }
  setCurrentSection(section): void{
    this.currentSection.next(section);
  }

  setCurrentQuestion(question): void{
    this.currentQuestion.next(question);
  }
  getCurrentQuestion(): Observable<any> {
    return this.currentQuestion.asObservable();
  }
}
