import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { IResponseModel } from 'types';
import { PreferredMediaListItemComponent } from './preferred-media-list-item/preferred-media-list-item.component';
import { QueryService } from 'app/main/content/services/query.service';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';
import { CampaignConfigurationsDataService } from 'app/main/content/event/event-layout/event-campaign/event-campaign-edit/campaign-configurations/campaign-configurations-data.service';
import { CampaignConfigurationsService } from 'app/main/content/event/event-layout/event-campaign/event-campaign-edit/campaign-configurations/campaign-configurations.service';

@Component({
  selector: 'irms-campaign-preferred-media-dialog',
  templateUrl: './campaign-preferred-media-dialog.component.html',
  styleUrls: ['./campaign-preferred-media-dialog.component.scss']
})
export class CampaignPreferredMediaDialogComponent implements OnInit, OnDestroy {
  isSubscribed: boolean = false;
  public data: Observable<IResponseModel>;
  public listItemComponent: Object = PreferredMediaListItemComponent;
  isLoading = true;
  mediaFilterArray = [
    {
      id: 'email',
      name: 'Email'
    },
    {
      id: 'sms',
      name: 'SMS'
    },
    {
      id: 'whatsapp',
      name: 'Whatsapp'
    },
  ];
  reachability: any;
  public selectedAll = {
    email: false,
    sms: false,
    whatsapp: false
  };
  public preferredMediaForm = this.fb.group({
    guests: new FormArray([])
  });

  saveLoading = false;
  public filterArgs = { queryString: '', selectedFilters: [] };
  private unsubscribeAll: Subject<any>;
  activeMedia = {
    email: false,
    sms: false,
    whatsapp: false
  }

  constructor(private fb: FormBuilder,
    public sectionService: CampaignConfigurationsService,
    protected dataService: CampaignConfigurationsDataService,
    protected router: Router,
    protected queryService: QueryService,
    @Inject(MAT_DIALOG_DATA) public model: any,
    private dialogRef: MatDialogRef<CampaignPreferredMediaDialogComponent>) {
    this.unsubscribeAll = new Subject();
    this.reachability = this.model.reachability;
  }

  ngOnInit(): void {
    this.sectionService.setFilter({ filter: this.filterArgs.selectedFilters });
    this.loadList();
  }
  ngOnDestroy(): void {
    this.preferredMediaForm = this.fb.group({
      guests: new FormArray([])
    });
    // Unsubscribe from all subscriptions
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
    this.sectionService.resetFilter();
  }

  loadList(): void {
    const filters = this.sectionService.getFilter();
    filters['guestListId'] = this.model.listId;
    filters['campaignId'] = this.model.campaignId;
    filters['searchText'] = this.filterArgs.queryString;
    // this.data = this.model.needCheck
    //   ? this.dataService.getContactsWithoutReachabilityList(filters)
    //   : this.dataService.getContactsReachabilityList(filters);

    this.data = this.dataService.getContactsReachabilityList(filters);
    this.patchForm();
  }

  // convenience getters for easy access to form fields
  get f(): any { return this.preferredMediaForm.controls; }
  get t(): any { return this.f.guests as FormArray; }

  patchForm(): void {
    this.sectionService.clearArray(this.preferredMediaForm.controls.guests['controls']);
    this.data.subscribe(result => {
      this.isSubscribed = result['item1'];
      result['item3'].items.forEach(item => {
        this.t.push(this.fb.group({
          id: [item.id, Validators.required],
          fullName: [item.fullName],
          email: [item.emailChecked, [Validators.required]],
          emailDisabled: [item.email],
          sms: [item.smsChecked, [Validators.required]],
          smsDisabled: [item.sms],
          whatsApp: [item.whatsAppChecked, [Validators.required]],
          whatsAppDisabled: [item.whatsApp],
          whatsappOptedIn: [item.whatsAppOptedIn]
        }));
      });
      if (this.isSubscribed) {
        this.dataService.checkReachability(this.model.listId).subscribe(result => {
          this.reachability = result;
        });
      }
      this.isLoading = false;
    }, error => {
      console.log('error:', error);

      if (error.error && error.error.code === 503) {
        const toasterService = appInjector().get(ToasterService);
        if (error.error && error.error.message) {
          toasterService.pop('error', null, error.error.message);
        } else {
          toasterService.pop('success', null, 'Reachability scan will is queued by background process.');
        }
      }
      this.isLoading = false;
    });
  }

  // Search Filte rchnage event
  searchQueryChange(event): void {
    this.filterArgs.queryString = event;
  }
  // handle filter change
  filterChange(event): void {
    this.filterArgs.selectedFilters = event;
  }

  /// Toggle Select All
  selectAll(select, media): void {
    this.t.controls.forEach((guest: FormGroup) => {
      if (media === 'email') {
        guest.controls.email.setValue(select);
      } else if (media === 'sms') {
        guest.controls.sms.setValue(select);
      } else if (media === 'whatsapp') {
        guest.controls.whatsApp.setValue(select);
      }
    });
    this.preferredMediaForm.markAllAsTouched();
  }

  autoSelect(): void {
    this.t.controls.forEach((guest: FormGroup) => {
      guest.controls.email.setValue(guest.controls.emailDisabled.value);
      guest.controls.sms.setValue(guest.controls.smsDisabled.value);
      guest.controls.whatsApp.setValue(guest.controls.whatsAppDisabled.value);
    });
    this.preferredMediaForm.markAllAsTouched();
  }

  confirm(): void {
    this.save();
  }

  close(): void {
    this.dialogRef.close();
  }

  save(): void {
    if (this.preferredMediaForm.valid && !this.preferredMediaForm.touched) {
      this.dialogRef.close();
      return;
    } else {
      if (this.preferredMediaForm.valid && this.preferredMediaForm.touched) {
        const guests = this.preferredMediaForm.value.guests.map(item => {
          //Set Active Media For Collect Info
          // if (!this.activeMedia.email && item.email) { this.activeMedia.email = true; }
          // if (!this.activeMedia.sms && item.sms) { this.activeMedia.sms = true; }
          // if (!this.activeMedia.whatsapp && item.whatsapp) { this.activeMedia.whatsapp = true; }

          return {
            id: item.id,
            email: item.email,
            sms: item.sms,
            whatsApp: item.whatsApp
          };
        });
        if (!this.model.isCollect) { //Check Reachability not Triggered from List Analysis
          const model = {
            campaignId: this.model.campaignId,
            guestPreferredMediaList: guests
          };
          this.saveLoading = true;
          this.dataService.updateGuestsPreferredMedia(model).subscribe(resp => {
            this.saveLoading = false;
            this.dialogRef.close();
          }, () => {
            this.saveLoading = false;
          });
        } else { //Check Reachability Triggered from List Analysis
          const model = {
            campaignId: this.model.campaignId,
            guestPreferredMediaList: guests
          }
          // Set Active Media
          this.activeMedia.email = guests.find(i => i.email === true) ? true : false;
          this.activeMedia.sms = guests.find(i => i.sms === true) ? true : false;
          this.activeMedia.whatsapp = guests.find(i => i.whatsApp === true) ? true : false;
          
          this.dataService.updateGuestsPreferredMedia(model).subscribe(res=>{
            this.saveLoading = false;
            this.dialogRef.close(this.activeMedia);
          }, () => {
            this.saveLoading = false;
          })
        }
      }
    }
  }
}
