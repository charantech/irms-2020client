import { BaseModel } from 'app/main/content/components/shared/section/base.model';

export class EventCampaignModel extends BaseModel{
    name: string;
}
