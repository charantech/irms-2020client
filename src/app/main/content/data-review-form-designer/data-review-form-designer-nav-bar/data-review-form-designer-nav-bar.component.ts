import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataReviewFormDesignerService } from '../data-review-form-designer.service';
import {Location} from '@angular/common';

@Component({
  selector: 'irms-data-review-form-designer-nav-bar',
  templateUrl: './data-review-form-designer-nav-bar.component.html',
  styleUrls: ['./data-review-form-designer-nav-bar.component.scss']
})
export class DataReviewFormDesignerNavBarComponent implements OnInit {

  @Output() saveForm: EventEmitter<any> = new EventEmitter();
  constructor(protected location: Location, protected dataReviewFormDesignerService: DataReviewFormDesignerService ) { }

  ngOnInit(): void {
  }

  onClickSave(): void{
    this.dataReviewFormDesignerService.triggerSave(true);
  }

  // Go back
  back(): void{
    this.location.back();
  }

}
