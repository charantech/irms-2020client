import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { IResponseSimpleEntity } from 'types';

@Component({
  selector: 'irms-single-option',
  templateUrl: './single-option.component.html',
  styleUrls: ['./single-option.component.scss']
})
export class SingleOptionComponent implements OnInit {
  @Input() theme: any;
  @Input() isArabic = false;
  @Input() question;
  @Input() public inputControl: FormControl;
  @Input() reviewMode = false;

  @Output() scrollNext = new EventEmitter<any>();
  @Output() fieldUpdate = new EventEmitter<any>();

  lookups = ['nationalityid', 'issuingcountryid', 'documenttypeid', 'gender'];
  enumLookups = ['documenttypeid', 'gender'];

  nationalities: any[];
  documentTypes: any[];
  issuingCountries: any[];
  valueProperty = 'label';

  public filteredArray: Observable<IResponseSimpleEntity[]>;

  public mockInput: FormControl = new FormControl('');

  constructor(public appStateService: AppStateService) { }



  ngOnInit(): void {
    if (this.question.hasOwnProperty('response')
      && this.question.response !== undefined 
      && this.question.response !== null 
      && this.question.response !== '' ) {
        if (!this.reviewMode) {
          if (this.enumLookups.includes(this.question.mappingField.toLowerCase())) {
            this.mockInput.setValue(Number(this.question.response));
            this.inputControl.setValue(Number(this.question.response));
          } else {
            this.mockInput.setValue(this.question.response);
            this.inputControl.setValue(this.question.response);
          }
        } else {
          if (this.enumLookups.includes(this.question.mappingField.toLowerCase())) {
            this.mockInput.setValue(Number(this.inputControl.value));
          } else {
            const selected = this.question.choices.find(item => item[this.valueProperty] === this.inputControl.value);      
            this.mockInput.setValue(selected);
          }
        }
    }
    
    this.loadLookups();
  }

  createAuto(): void{
    const displayNameRef = this.displayName;

    this.filteredArray = this.mockInput.valueChanges.pipe(
      startWith<string | IResponseSimpleEntity>(''),
      map(entity => {
        if (typeof entity === 'string') {
          return entity;
        }
        if (entity != null) {
          return displayNameRef(entity);
        }
      }),
      map(val => {
        if (val) { return this.filter(val); }
        else { return this.question.choices; }
      })
    );
    if (this.inputControl.value !== null && this.inputControl.value !== undefined && this.inputControl.value !== '') { 
      const selected = this.lookups.indexOf(this.question.mappingField.toLowerCase()) > -1 ? 
      this.question.choices.find(item => item.value === this.inputControl.value) 
      : this.question.choices.find(item => item[this.valueProperty] === this.inputControl.value);      
      this.mockInput.setValue(selected);
    }
  }

  public filter(val): void {
    const displayNameRef = this.displayName;
    return this.question.choices.filter(item => displayNameRef(item).toLowerCase().indexOf(val.toLowerCase()) === 0);
  }
  clearAutoComplete(): void {
    this.mockInput.setValue('');
    this.inputControl.setValue('');
  }

  public displayName(val): string {
    if (val) {
      return val.label;
    }
    return undefined;
  }
  loadLookups(): void {
    if (this.question.mappingField.toLowerCase() === 'nationalityid') {
      this.valueProperty = 'value';
      this.question.choices = [];
      this.appStateService.GetNationalities().subscribe((result) => {
        this.nationalities = result;
        this.nationalities.forEach(element => {
          this.question.choices.push({
            label: element.nationality,
            labelArabic: element.nationality,
            selected: false,
            value: `${element.id}`
          });
        });
        this.createAuto();
      });
    } else if (this.question.mappingField.toLowerCase() === 'issuingcountryid') {
      this.valueProperty = 'value';
      this.question.choices = [];
      this.appStateService.getCountries().subscribe((result) => {
        this.issuingCountries = result;
        this.issuingCountries.forEach(element => {
          this.question.choices.push({
            label: element.value,
            labelArabic: element.value,
            value: `${element.id}`
          });
        });
        this.createAuto();
      });
    } else if (this.question.mappingField.toLowerCase() === 'documenttypeid') {
      this.question.choices = [];
      this.valueProperty = 'value';
      this.appStateService.GetDocumentTypes().subscribe((result) => {
        this.documentTypes = result;
        this.documentTypes.forEach(element => {
          this.question.choices.push({
            label: element.name,
            labelArabic: element.name,
            value: element.id
          });
        });
        this.createAuto();
      });
    } else if (this.question.mappingField.toLowerCase() === 'gender') {
      this.valueProperty = 'value';
      this.question.choices = [
        {
          label: 'Male',
          labelArabic: 'ذكر',
          value: 0
        },
        {
          label: 'Female',
          labelArabic: 'انثى',
          value: 1
        }
      ];
      this.createAuto();
    } else {
      this.createAuto();
    }
  }
  /**
   * Go to next Question
   */
  gotoNext(): void {
    // this.setAsCurrent();
    if (this.reviewMode) {
      return;
    }
    this.fieldUpdate.next();
    const jumps = this.question.jumps;
    let destination;
    this.scrollNext.emit({
      question: this.question,
      destination: destination || ''
    });
  }
}
