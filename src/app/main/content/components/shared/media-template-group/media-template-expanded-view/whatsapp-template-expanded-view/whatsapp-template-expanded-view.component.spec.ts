import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappTemplateExpandedViewComponent } from './whatsapp-template-expanded-view.component';

describe('WhatsappTemplateExpandedViewComponent', () => {
  let component: WhatsappTemplateExpandedViewComponent;
  let fixture: ComponentFixture<WhatsappTemplateExpandedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappTemplateExpandedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappTemplateExpandedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
