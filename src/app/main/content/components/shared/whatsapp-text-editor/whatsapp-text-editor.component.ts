import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-whatsapp-text-editor',
  templateUrl: './whatsapp-text-editor.component.html',
  styleUrls: ['./whatsapp-text-editor.component.scss']
})
export class WhatsappTextEditorComponent implements OnInit {
  @Input() public inputControl: FormControl;
  @Input() public placeholder: string;
  @Input() public fxFlex = '100';
  @Input() public required: boolean;
  @Input() public fieldLabel: string;
  @Input() public maxRows = 5;
  @Input() public minRows = 2;
  @Input() disabled = false;
  @Output() onBlur: EventEmitter<Event> = new EventEmitter();

  errorRangeText;
  errorPatternText;
  selection = {
    start: null,
    end: null
  };

  formatChar = {
    bold: '*',
    italic: '_',
    strikethrough: '~',
    monospace: '```'
  };


  constructor() { }

  ngOnInit(): void {
    if (this.disabled) {
      this.inputControl.disable();
    }
  }
  selectionchange(ev: any): void {
    this.selection.start = ev.target.selectionStart;
    this.selection.end = ev.target.selectionEnd;
  }

  handleBlur(event): void {
    this.onBlur.emit(event);
  }

  format(opt): void {   
    if (this.selection.start != null && this.selection.end != null) {
      this.inputControl.setValue(
      this.inputControl.value.substr(0, this.selection.start) + this.formatChar[opt] +
      this.inputControl.value.substr(this.selection.start, this.selection.end - this.selection.start) + this.formatChar[opt] + 
      this.inputControl.value.substr(this.selection.end + 1, this.inputControl.value.length - 1));
    }
    else {
      this.inputControl.setValue(this.inputControl.value + this.formatChar[opt] + ' ' + this.formatChar[opt]);
    }
  }

}
