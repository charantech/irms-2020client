import { TestBed } from '@angular/core/testing';

import { CollectImportantFieldsService } from './collect-important-fields.service';

describe('CollectImportantFieldsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollectImportantFieldsService = TestBed.get(CollectImportantFieldsService);
    expect(service).toBeTruthy();
  });
});
