import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { BaseModel } from '../section/base.model';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderDataService implements SectionDataService<BaseModel> {

  private url = `${BASE_URL}api/rfiForm`;

  constructor(private http: HttpClient) { }

  getList(filter): any {
    return of();
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}`);
  }

  @Toast('Successfully updated')
  upsert(model) {
    return this.http.post(`${this.url}`, model);
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }

  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
}
