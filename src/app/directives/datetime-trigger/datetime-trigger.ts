import {Directive, ElementRef, HostListener, ViewContainerRef} from '@angular/core';

@Directive({selector: '[irmsDatetimeTrigger]'})
export class DateTimeTriggerDirective {
  @HostListener('click') onClick() {
    this.element.nativeElement.parentNode.childNodes.forEach(n => {
      if (n.attributes && n.attributes['matinput']) {
        n.click();
      }
    });
  }

  constructor(private viewContainer: ViewContainerRef,
              private element: ElementRef) {

  }
}
