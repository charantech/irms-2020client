import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CollectImportantFieldsService {

  private campaignSave = new Subject<any>();
  private campaignName = new BehaviorSubject<any>('');

  saveCampaign(flag: boolean): void {
    this.campaignSave.next(flag);
  }

  getCampaignSave(): Observable<any> {
    return this.campaignSave.asObservable();
  }

  // Campaign name observables
  setCampaignName(name: string): void {
    this.campaignName.next(name);
  }

  getCampaignName(): Observable<any> {
    return this.campaignName.asObservable();
  }

  getInstantCampaignName(): string {
    return this.campaignName.value;
  }

  // Proceed background images for emails here in future
  proceedEmailTemplate(info: any): any {
    return {
      id: info.id,
      image: info.emailImage,
      sender: info.emailSender,
      subject: info.emailSubject
    };
  }

  // Proceed background images for sms here in future
  proceedSmsTemplate(info: any): any {
    return {
      id: info.id,
      image: info.smsImage,
      sender: info.smsSender,
    };
  }

  proceedWhatsappTemplate(info: any): any {
    return {
      id: info.id,
      image: info.whatsappImage,
      sender: info.whatsappSender,
    };
  }

  // Proceed background images for emails here in future
  proceedRfiTemplate(info: any): any {
    if (info.formCreated) {
      return {
        id: info.id,
        image: info.formBackgroundPath,
      };
    } else {
      return null;
    }   
  }
}
