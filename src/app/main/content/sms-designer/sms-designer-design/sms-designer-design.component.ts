import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { SmsDesignerService } from '../sms-designer.service';
import { Validators, FormBuilder } from '@angular/forms';
import { SmsDesignerDataService } from '../sms-designer-data.service';
import { RsvpFormService } from '../../components/shared/rsvp-form/rsvp-form.service';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from '../../services/can-exit.guard';
import { fuseAnimations } from 'app/core/animations';
import { invitationWorkflowStep } from 'app/constants/constants';

@Component({
  selector: 'irms-sms-designer-design',
  templateUrl: './sms-designer-design.component.html',
  styleUrls: ['./sms-designer-design.component.scss'],
  animations: fuseAnimations
})
export class SmsDesignerDesignComponent implements OnInit, OnDestroy, CanExit {

  @Input() invitationId: string;
  @Input() isReview: boolean;
  @Input() isLanding = true;
  @Input() isDialog = false;
  @Input() step;
  thisState = 'component';
  invitationStep = invitationWorkflowStep;
  public smsForm = this.fb.group({
    sender: ['', [Validators.required, Validators.maxLength(100)]],
    toName: [''],
    smsContent: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(320)]]
  });
  senders;
  tagSearchKeyword = '';
  tags;

  hasUnstagedChanges = false;

  currentCursor;
  subscriptions: Subscription[] = [];

  isFormUpdating = false;
  containsTags: boolean;
  model: any;
  templateId: any;
  theme: any;
  constructor(private fb: FormBuilder,
    protected sectionService: SmsDesignerService,
    protected dataService: SmsDesignerDataService,
    protected rsvpService: RsvpFormService,
    private dialog: MatDialog,
  ) {
    this.subscriptions.push(this.sectionService.unstagedChanges.subscribe(data => {
      this.hasUnstagedChanges = data;
      this.smsForm.markAllAsTouched();
    }));
    this.subscriptions.push(this.sectionService.saveObs().subscribe(data => {
      if (this.thisState === this.sectionService.getCurrentComponentState()) {
        this.saveTemplate();
      }
    }));
    this.subscriptions.push(this.sectionService.sendTestObs().subscribe(data => this.sendTestSMS()));
    this.subscriptions.push(this.sectionService.getRefreshTrigger().subscribe(data => {
      if (this.sectionService.template) {
        this.smsForm.setValue(this.sectionService.template);
      }
    }));
  }

  ngOnInit(): void {
    this.senders = [];
    this.dataService.loadSenders()
      .subscribe(result => {
        const i = 0;
        result.forEach(item => {
          this.senders.push({
            id: item,
            value: item
          });
        });

        const template = this.sectionService.template;
        template.senderName = this.senders[0].value;
        this.smsForm.patchValue({
          sender: this.senders[0].id
        });
      });
    if (!this.isDialog && this.invitationId) {
      this.loadSmsTemplate();
    }
    else if (this.isDialog) {
      this.thisState = 'dialog';
      this.smsForm.patchValue(this.sectionService.template);
    }
    this.updateTags();
    this.smsForm.valueChanges.subscribe(data => {
      if (!this.isFormUpdating) {
        this.sectionService.unstagedChanges.next(true);
        this.isFormUpdating = true;
        this.sectionService.setTemplate(data);
        this.sectionService.fieldsFilled.next(this.smsForm.valid);
        this.isFormUpdating = false;
        if (data.smsContent && data.smsContent.includes('{{')) {
          this.containsTags = true;
        } else {
          this.containsTags = false;
        }
      }
    });
    if (this.step === this.invitationStep.Accepted || this.step === this.invitationStep.Rejected) {
      this.isLanding = false;
    }
  }

  loadSmsTemplate(): void {
    if (this.isReview) {
      this.dataService.getDataReview(this.invitationId).subscribe(m => {
        this.model = m;
        if (m) {
          this.templateId = this.model.id;
          const sms = this.sectionService.template;
          sms.senderName = m.senderName,
            sms.body = m.body;
          this.sectionService.setTemplate(sms);
          this.sectionService.rvspFormValue = m;
          this.formInit();
        } else { // form was not loaded
          this.sectionService.initDefaultForm();
          this.formInit();
        }
      });
    } else {
      this.dataService.get(this.invitationId).subscribe(m => {
        this.model = m;
        if (m) {
          this.theme = m.themeJson
          this.templateId = this.model.id;
          const sms = this.sectionService.template;
          this.smsForm.controls['toName'].setValue(m.listName)
          sms.senderName = m.senderName,
            sms.body = m.body;
          this.sectionService.setTemplate(sms);
          this.sectionService.rvspFormValue = m;
          this.formInit();
        } else { // form was not loaded
          this.sectionService.initDefaultForm();
          this.formInit();
        }
      });
    }
  }

  private formInit(): void {
    const template = this.sectionService.template;
    this.smsForm.patchValue({
      sender: template.senderName,
      smsContent: template.body
    });
    if (template.body.includes('{{')) {
      this.containsTags = true;
    } else {
      this.containsTags = false;
    }
    // this.sectionService.getTemplate()
    //   .subscribe(x => {
    //     if (!this.isFormUpdating) {
    //       this.isFormUpdating = true;
    //       this.smsForm.patchValue({
    //         sender: this.senders[0].id,
    //         smsContent: x.body
    //       });
    //       this.isFormUpdating = false;
    //     }
    //   });
  }

  private updateTags(): void {
    const filter = this.sectionService.getFilter();
    this.dataService.getPlaceholders(filter, this.invitationId, false).subscribe(result => {
      this.tags = this.addDefaultValues(result);
    });
  }
  addDefaultValues(tags): any {
    const tagss = [];
    tags.forEach(item => {
      if (item.title == 'Invitation rejected webpage link' || item.title == 'Invitation accepted webpage link' || item.title == 'RFI form link' || item.title == 'RSVP form invitation link') {
        tagss.push({
          id: item.id,
          label: item.title,
          value: item.placeholder
        });

      } else {
        tagss.push({
          id: item.id,
          label: item.title,
          value: [item.placeholder.slice(0, item.placeholder.length - 2), ' | DEFAULT VALUE', item.placeholder.slice(item.placeholder.length - 2)].join(''),
        });
      }
    });
    return tagss;
  }

  landingSms(): void {
    this.sectionService.triggerLandingPage();
  }

  ngOnDestroy(): void {
    if (this.isDialog) {
      this.sectionService.setTemplate(this.smsForm.value);
    } else {
      this.smsForm.reset();
      this.sectionService.setTemplate(this.smsForm.value);
    }
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  patchForm(data): void {
    this.smsForm.patchValue(data);
  }

  // on search tag event
  searchTags(event): void {
    this.tagSearchKeyword = event;
  }

  // adds Tag to the selected content editor
  addTag(link): void {
    const temp = this.smsForm.get('smsContent').value;
    this.currentCursor += link.length;
    if (this.currentCursor && this.currentCursor > 0) {
      const output = [temp.slice(0, this.currentCursor), link, temp.slice(this.currentCursor)].join('');
      this.smsForm.get('smsContent').setValue(output);
    } else {
      this.smsForm.get('smsContent').setValue(link + '' + temp);
    }
  }

  // update current cursor position in content area
  updateCurrentCursor(event): void {
    this.currentCursor = event;
  }

  saveTemplate(): void {
    // call API for SAve template
    // call Save APIs
    console.log(this.sectionService.template)
    if (this.smsForm.valid) {
      const sms = this.smsForm.value;
      this.sectionService.unstagedChanges.next(false);
      let themeJson = {};
      let formTheme;
      let payload;
      if (!this.sectionService.rvspFormValue || !this.sectionService.rvspFormValue.id) {
        formTheme = this.sectionService.rvspFormValue.formTheme;
        themeJson = {
          buttons: formTheme.buttons,
          background: {
            color: formTheme.background.color,
            style: formTheme.background.style,
            image: ''// formTheme.background.image
          }
        };
        payload = {
          id: this.templateId ? this.templateId : '00000000-0000-0000-0000-000000000000',
          campaignInvitationId: this.invitationId,
          senderName: sms.sender,
          body: sms.smsContent,
          welcomeHtml: this.sectionService.rvspFormValue.formContent.welcome.content,
          proceedButtonText: this.sectionService.rvspFormValue.formContent.welcome.button,
          rsvpHtml: this.sectionService.rvspFormValue.formContent.rsvp.content,
          acceptButtonText: this.sectionService.rvspFormValue.formContent.rsvp.acceptButton,
          rejectButtonText: this.sectionService.rvspFormValue.formContent.rsvp.rejectButton,
          acceptHtml: this.sectionService.rvspFormValue.formContent.accept.content,
          rejectHtml: this.sectionService.rvspFormValue.formContent.reject.content,
          themeJson: this.templateId ? themeJson : JSON.stringify(themeJson),
          backgroundImagePath: formTheme.background.image
        };
        
      } else {
        payload = this.sectionService.rvspFormValue;
        payload.senderName = sms.sender;
        payload.body = sms.smsContent;
      }

      const fd = new FormData();
      for (const key in payload) {
        fd.append(key, payload[key]);
      }
      if (this.isReview) {
        this.dataService.createOrUpdateDataReview(fd)
          .subscribe(id => {
            this.templateId = id.toString();
          });
      } else {
        this.dataService.createOrUpdate(fd)
          .subscribe(id => {
            this.templateId = id.toString();
          });
      }
    }

  }

  /// Sed test email template
  sendTestSMS(): void {
  }

  canDeactivate(): any {
    if (this.smsForm.dirty && this.hasUnstagedChanges) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

}
