import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormDesignerComponent } from './data-review-form-designer.component';

describe('DataReviewFormDesignerComponent', () => {
  let component: DataReviewFormDesignerComponent;
  let fixture: ComponentFixture<DataReviewFormDesignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormDesignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormDesignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
