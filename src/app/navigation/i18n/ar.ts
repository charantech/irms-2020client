export const locale = {
    lang: 'ar',
    data: {
        NAV: {
            ADMIN_FUNCTIONS: 'وظائف الإدارة',
            DASHBOARD        : {
                TITLE: 'لوحة التحكم'
            },
            EVENTS : {
                TITLE: 'الفعاليات'
            },
            GLOBAL_CONTACTS : {
                TITLE: 'قائمة جهات الاتصال'
            },
            GLOBAL_GUESTS : {
                TITLE: 'قائمة الضيوف'
            },
        }
    }
};
