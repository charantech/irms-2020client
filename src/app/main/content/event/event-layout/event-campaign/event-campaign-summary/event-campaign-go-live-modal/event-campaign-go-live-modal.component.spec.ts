import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignGoLiveModalComponent } from './event-campaign-go-live-modal.component';

describe('EventCampaignGoLiveModalComponent', () => {
  let component: EventCampaignGoLiveModalComponent;
  let fixture: ComponentFixture<EventCampaignGoLiveModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignGoLiveModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignGoLiveModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
