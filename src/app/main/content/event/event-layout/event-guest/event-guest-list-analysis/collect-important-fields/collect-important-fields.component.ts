import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EventGuestModel } from '../../event-guest.model';
import { Subscription } from 'rxjs';
import { timePeriodUnits, sideDialogConfig } from 'app/constants/constants';import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CollectImportantFieldsService } from '../collect-important-fields.service';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { ImportAnalysisFormService } from '../import-analysis-form.service';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { CampaignPreferredMediaDialogComponent } from 'app/main/content/components/shared/preferred-media/campaign-preferred-media-dialog/campaign-preferred-media-dialog.component';
import { RfiDataReviewModalComponent } from './rfi-data-review-modal/rfi-data-review-modal.component';
import { FormJsonUtilityService } from 'app/main/content/components/shared/form-builder/form-json-utility.service';

@Component({
    selector: 'irms-collect-important-fields',
    templateUrl: './collect-important-fields.component.html',
    styleUrls: ['./collect-important-fields.component.scss']
})
export class CollectImportantFieldsComponent implements OnInit, OnDestroy, CanExit {
    isLoaded = false;
    subscriptions: Subscription[] = [];
    nodeSubscription: Subscription;
    isFirstNode = true;
    public acceptedForm = this.fb.group({
        name: ['', [Validators.required]],
        isInstant: [false, [Validators.required]],
        delay: this.fb.group({
            interval: [0],
            intervalType: [timePeriodUnits.Minutes, [Validators.required]]
        }),
    });
    // campaignId: any;

    smsTemplate: any;
    emailTemplate: any;
    whatsappTemplate: any;
    rfiForm: any;
    model: EventGuestModel;
    isDataLoaded: boolean;
    data: any;
    listId: any;
    fields: any = [];
    activeMedia = {
        email: !false,
        sms: !false,
        whatsapp: !false
    };
    invitationId: any;
    eventId: any;
    isSubmitting: boolean;
    formUpserted = false;
    isBack: boolean;

    constructor(protected fb: FormBuilder,
                private dialog: MatDialog,
                public sectionService: EventGuestService,
                protected dataService: EventGuestDataService,
                public service: CollectImportantFieldsService,
                protected router: Router,
                protected route: ActivatedRoute,
                public formService: ImportAnalysisFormService,
                public animDialog: DialogAnimationService,
                public rfiGenerator: FormJsonUtilityService, ) {
    }

    ngOnInit(): void {
        this.route.params.subscribe((params: Params) => {
            this.listId = params['glid'];
            this.sectionService.listId = this.listId;
            this.eventId = params['id'];
        });

        if (this.formService.selectedFields) {
            this.formService.headers = this.formService.selectedFields.reserved.map(r => r.value).concat(this.formService.selectedFields.custom.map(c => c.label));
            this.createCampaign({
                eventId: this.eventId,
                guestListId: this.listId
            });
        } else {
            let custList = [];
            this.dataService.getSelectedImportantFields(this.listId)
                .subscribe(fields => {
                    // Handle Custom Fields
                    if (fields.customsFields.length > 0) {
                        this.dataService.getAllCustomFields({
                    fields: fields.customsFields.map(c => c.value)
                        }).subscribe(data => {
                            custList = data;
                            // Get previously saved important fields for the list
                            fields.customsFields.forEach(x => {
                        const cObj = custList.find(f => f.id === x.value);
                        if (cObj) {
                            x.customFieldType = cObj.customFieldType;
                            x.maxValue = cObj.maxValue;
                            x.minValue = cObj.minValue;
                            x.label = cObj.name;
                        }
                            });
                            this.formService.setSelectedFields({ reserved: fields.reservedFields, custom: fields.customsFields });
                            this.formService.headers = fields.reservedFields.map(r => r.value).concat(fields.customsFields.map(c => c.label));
                            this.createCampaign({
                                eventId: this.eventId,
                                guestListId: this.listId
                            });
                        });
                    }
                    else {
                    this.formService.setSelectedFields({ reserved: fields.reservedFields, custom: [] });
                    this.formService.headers = fields.reservedFields.map(r => r.value);
                    this.createCampaign({
                            eventId: this.eventId,
                            guestListId: this.listId
                        });
                    }
                });
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
        this.subscriptions = [];
        this.fields = [];
    }

    selectPrefMedium(): void {
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '60vw';
        dialogConfig.data = {
            listId: this.sectionService.listId,
            isCollect: true,
            campaignId: this.sectionService.campaignId,
            // reachability: this.reachability,
            needCheck: true
        };
        const dialogRef = this.animDialog.open(CampaignPreferredMediaDialogComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(x => {
            if (x) {
                this.activeMedia = x;
            }
        });
    }

    createCampaign(model): void {
        this.dataService.createListCampaign(model).subscribe(result => {
            if (result) {
                
                console.log(result)
                this.formService.headers.forEach(element => {
                    this.fields.push({
                        field: element,
                        question: `Enter ${element}`
                    });
                });
                this.sectionService.invitationId = result.campaignInvitationId;
                this.sectionService.campaignId = result.campaignId;
                this.sectionService.rfiFormId = result.rfiFormId;
                this.loadTemplates();
            }
        });

    }
    handleFormChanged(invitationId): void {
        this.dataService.getRFI(invitationId).subscribe(m => {
            const model = m;
            let autoForm: any;
            if (m) {
                model['rfiFormQuestions'] = m.rfiFormQuestions.sort((obj1, obj2) => {
                    if (obj1.sortOrder > obj2.sortOrder) {
                        return 1;
                    }

                    if (obj1.sortOrder < obj2.sortOrder) {
                        return -1;
                    }

                    return 0;
                });
                autoForm = this.rfiGenerator.getMissingRFIFormQuestions(this.formService.getSelectedFields(), model.rfiFormQuestions);
                delete model.rfiFormQuestions,
                    model.questionJson = JSON.stringify(autoForm.questions);
                model.campaignInvitationId = this.emailTemplate.id;
                model.id = this.sectionService.rfiFormId;
                this.generateRFIForm(model, autoForm.isFieldSetChanged, autoForm.deletedFields, autoForm.addedFields);
            } else {
                autoForm = this.rfiGenerator.getMissingRFIFormQuestions(this.formService.getSelectedFields());
                const formObj = JSON.parse(JSON.stringify(this.rfiGenerator.formBase));
                formObj.campaignInvitationId = this.emailTemplate.id;
                formObj.id = this.sectionService.rfiFormId;
                formObj.questionJson = JSON.stringify(autoForm.questions);
                this.generateRFIForm(formObj);
            }
        });
    }

    generateRFIForm(formModel, isChanged = false, delFields = null, addFields = null): void {
        const fd = new FormData();
        for (const key in formModel) {
            fd.append(key, formModel[key]);
        }
        this.dataService.upsertRFI(fd).subscribe(id => {
            this.sectionService.rfiFormId = id.toString();
            this.rfiForm = {
                id: id.toString()
            };
            this.formUpserted = true;
            if (isChanged) {
                this.formChangedDialog(delFields, addFields);
            }
        });
    }

    loadTemplates(): void {
        this.isLoaded = true;
        this.invitationId = this.sectionService.invitationId;
        this.dataService.getReviewFormDetails(this.invitationId).subscribe(m => {
            this.model = m;
            this.isLoaded = true;
            if (m) {
                if(m.activeMedia.email || m.activeMedia.sms || m.activeMedia.whatsapp){
                    this.activeMedia = m.activeMedia;
                }
                m.id = this.invitationId;
                this.acceptedForm.controls['name'].setValue(m['title']);
                this.isDataLoaded = true;
                this.patchModelInForms();

                this.smsTemplate = this.service.proceedSmsTemplate(m);
                this.emailTemplate = this.service.proceedEmailTemplate(m);
                this.whatsappTemplate = this.service.proceedWhatsappTemplate(m);

                this.handleFormChanged(this.sectionService.invitationId);
            }
        });
    }

    canDeactivate(): any {
        if (this.acceptedForm.dirty) {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
            dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
            dialogRef.componentInstance.confirmButton = 'Leave page';
            dialogRef.componentInstance.cancelButton = 'Stay';
            dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
            return dialogRef.afterClosed();
        }
        else {
            return true;
        }
    }

    // patch form
    patchModelInForms(): void {
        this.acceptedForm.patchValue(this.model);
        if (!this.model['isInstant']) {
            this.acceptedForm.controls.delay.patchValue(this.model);
        } else {
            this.acceptedForm.controls.delay.patchValue({ interval: 0, intervalType: 2 });
        }
    }

    /// save 
    save(): void {
        if (this.acceptedForm.valid) {
            this.sectionService.loading = true;
            const model = this.acceptedForm.controls.delay.value;
            model.id = this.model.id;
            model.isInstant = this.acceptedForm.value.isInstant;
            if (model.isInstant) {
                model.interval = 0;
                model.intervalType = timePeriodUnits.Minutes;
            }

            model.title = this.acceptedForm.controls['name'].value;
        }
    }

    /// Create RFI form
    createForm(): void {
        // this.router.navigateByUrl(`/data-review/${this.model.id}`);
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '80vw';
        const selectedFields = this.formService.getSelectedFields();
        const fields = this.formService.headers;
        // if (fields.indexOf('nationalityId') > -1){
        //     fields[fields.indexOf('nationalityId')] = 'Nationality';
        // }
        dialogConfig.data = {
            id: this.emailTemplate.id,
            fields: fields,
            rfiFormId: this.sectionService.rfiFormId,
        };


        const dialogRef = this.animDialog.open(RfiDataReviewModalComponent, dialogConfig);
    }

    goBack(): void {
        this.isBack = true;
        const model = {
            listId: this.listId,
            eventId: this.eventId,
            listAnalysisStep: 1,
            listAnalysisPageNo: null,
            listAnalysisPageSize: null
        };
        this.dataService.setStep(model).subscribe(res => {
            if (res) {
                this.isBack = false;
                this.formService.linear = false;
                this.formService.setStepperIndex(1);
                setTimeout(() => { this.formService.linear = true; }, 1000);
            }

        });
    }

    formChangedDialog(delFields, addFields): void {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
            dialogRef.componentInstance.confirmHeading = 'Important questions updated';
            dialogRef.componentInstance.confirmButton = 'Yes, open form';
            dialogRef.componentInstance.cancelButton = 'No';
            dialogRef.componentInstance.confirmMessage = `Form has been updated to reflect the updated set of important fields
            ${addFields !== null && addFields.length > 0 ? '\nFields added are :\n' : ''} ${addFields !== null && addFields.length > 0 ? addFields.join(', ') : ''}
            ${delFields !== null && delFields.length > 0 ? '\nFields deleted are :\n' : ''} ${delFields !== null && delFields.length > 0 ? delFields.join(', ') : ''} 
            Do you want to have a look at the updated form.`;
            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.createForm();
                }
            });
        }

    cancel(): void {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.disableClose = true;
            const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
            dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
            dialogRef.componentInstance.confirmButton = 'Leave page';
            dialogRef.componentInstance.cancelButton = 'Stay';
            dialogRef.componentInstance.confirmMessage = 'Are you sure you want to cancel? Cancel at this step will reset your content.\n All changes will be lost.';
            dialogRef.afterClosed().subscribe(res => {
                if (res) {
                    this.dataService.cancelListAnalysis({ guestListId: this.sectionService.listId, eventId: this.eventId }).subscribe(x => {
                        if (x) {
                            this.formService.finalStep = false;
                            this.router.navigate(['../'], { relativeTo: this.route });
                        }
                    });

                }
            });
        }

    submit(): void {
            this.isSubmitting = true;
            const model = {
                listId: this.sectionService.listId,
                eventId: this.sectionService.eventId,
                ListAnalysisStep: 3,
                ListAnalysisPageNo: null,
                ListAnalysisPageSize: null
            };

            this.dataService.setStep(model).subscribe(res => {
                if (res) {
                    this.dataService.sendCollectForm(this.sectionService.campaignId).subscribe(result => {
                        if (result) {
                            this.formService.linear = false;
                            this.isSubmitting = false;
                            this.formService.setStepperIndex(3);
                            setTimeout(() => {
                                this.formService.linear = true;
                            });
                        }
                    }, error => {
                        model.ListAnalysisStep = 2;
                        this.isSubmitting = false;
                        this.dataService.setStep(model).subscribe(s => { this.isSubmitting = false; });
                    });
                }
            });

        }
}
