import { Injectable } from "@angular/core";
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { IEventList, IResponseModel } from 'types';
import { Observable, of } from 'rxjs';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { EventDataModel } from './event-layout/event-data/event-data.model';

@Injectable({
    providedIn: 'root'
})
export class DataModuleDataService implements SectionDataService<EventDataModel> {
    
    private url = `${BASE_URL}api/dataModule`
    constructor(private http: HttpClient) {}

    @Loader()
    getList(filterParam: any): Observable<IResponseModel> {
        return this.http.get<IResponseModel>(`${this.url}/campaigns-list/${filterParam.eventId}`);
    }

    get(id): Observable<EventDataModel> {
        throw new Error('Method not implemented');
    }
    
    getListAnalysis(model): Observable<IResponseModel> {
        return of({
            "items": [
              {
                "id": "9f717c5d-e9a1-42c8-b3e5-0391159092e9",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "c61c9064-3170-4ea5-b1d1-132c06e696af",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "20088b9b-2b3f-4d6e-9fc3-4ff9ee0e043b",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "30aaa321-94a7-4440-a5ba-58311a2fdf04",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "979cc4e0-f137-426d-9b3e-7311abf4844a",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "c272fafa-031e-4c11-8f04-7fd16ccd993b",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "82ca2ede-c8c5-422c-953a-8eeb1a41f56a",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "c816504c-5748-4b99-bf12-a4f96ba79497",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              },
              {
                "id": "f90c6f7a-0987-4796-8e3d-aa88ee58a51e",
                "name": "Dummy",
                "status": "3",
                "lastUpdate": "2020-10-31T06:52:37.000Z"
              }
            ],
            "totalCount": 9,
            "isReadonly": false
          });
      }

    create(model: any): Observable<string> {
        throw new Error('Method not implemented');
    }

    update(model): Observable<any> {
        throw new Error('Method not implemented');
    }

    delete(model): Observable<any> {
        throw new Error('Method not implemented');
    }
}