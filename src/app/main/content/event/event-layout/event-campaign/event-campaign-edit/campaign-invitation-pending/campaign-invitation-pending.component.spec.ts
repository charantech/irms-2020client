import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignInvitationPendingComponent } from './campaign-invitation-pending.component';

describe('CampaignInvitationPendingComponent', () => {
  let component: CampaignInvitationPendingComponent;
  let fixture: ComponentFixture<CampaignInvitationPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignInvitationPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignInvitationPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
