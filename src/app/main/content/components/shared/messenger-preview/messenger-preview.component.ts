import { Component, OnInit, Input } from '@angular/core';
import { SmsDesignerService } from 'app/main/content/sms-designer/sms-designer.service';

@Component({
  selector: 'irms-messenger-preview',
  templateUrl: './messenger-preview.component.html',
  styleUrls: ['./messenger-preview.component.scss']
})
export class MessengerPreviewComponent implements OnInit {
  public timeNow: Date = new Date();
  @Input() sender = 'Sender';
  @Input() messages = [];
  constructor(
    private sectionService: SmsDesignerService
  ) { 
    const smsInfo = this.sectionService.template
    // this.sender = smsInfo.senderName
    // this.messages = [
    //   smsInfo.body
    // ]

  }

  ngOnInit() {
  }

}
