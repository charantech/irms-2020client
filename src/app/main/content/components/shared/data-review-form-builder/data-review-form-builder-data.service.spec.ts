import { TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderDataService } from './data-review-form-builder-data.service';

describe('DataReviewFormBuilderDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataReviewFormBuilderDataService = TestBed.get(DataReviewFormBuilderDataService);
    expect(service).toBeTruthy();
  });
});
