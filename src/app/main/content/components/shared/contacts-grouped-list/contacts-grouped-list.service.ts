import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactsGroupedListService {
  onSelectAllChange = new Subject<any>();
  constructor() { }
  
  getSelectToggle(): Observable<any> {
    return this.onSelectAllChange.asObservable();
  }
  selectAll(): void{
    this.onSelectAllChange.next(true);
  }
  deselectAll(): void{
    this.onSelectAllChange.next(false);
  }
}
