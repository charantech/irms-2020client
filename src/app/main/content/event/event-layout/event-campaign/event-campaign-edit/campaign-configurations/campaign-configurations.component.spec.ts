import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignConfigurationsComponent } from './campaign-configurations.component';

describe('CampaignConfigurationsComponent', () => {
  let component: CampaignConfigurationsComponent;
  let fixture: ComponentFixture<CampaignConfigurationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignConfigurationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
