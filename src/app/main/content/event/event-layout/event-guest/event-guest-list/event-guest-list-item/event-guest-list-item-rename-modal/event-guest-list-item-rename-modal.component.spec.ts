import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestListItemRenameModalComponent } from './event-guest-list-item-rename-modal.component';

describe('EventGuestListItemRenameModalComponent', () => {
  let component: EventGuestListItemRenameModalComponent;
  let fixture: ComponentFixture<EventGuestListItemRenameModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestListItemRenameModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestListItemRenameModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
