import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/constants/constants';
import { Observable } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationViewPendingDataService implements SectionDataService<EventCampaignModel> {
  private url = `${BASE_URL}api/CampaignInvitation`;
  constructor(private http: HttpClient) { }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/pending/${id}`);
  }

  getList(campaignId): Observable<any> {
    return this.http.get(`${this.url}/${campaignId.campaignId || campaignId}/pending`);
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }
  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
}
