import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactListItemComponent } from './global-contact-list-item.component';

describe('GlobalContactListItemComponent', () => {
  let component: GlobalContactListItemComponent;
  let fixture: ComponentFixture<GlobalContactListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
