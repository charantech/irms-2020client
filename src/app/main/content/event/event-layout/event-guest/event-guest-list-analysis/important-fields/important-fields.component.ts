import { Component, OnInit } from '@angular/core';
import { EventGuestModel } from '../../event-guest.model';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { reservedFieldsWithValidation, Section, sideDialogConfig } from 'app/constants/constants';
import { fuseAnimations } from '@fuse/animations';
import { ImportAnalysisFormService } from '../import-analysis-form.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { AddCustomFieldModalComponent } from 'app/main/content/components/shared/add-custom-field-modal/add-custom-field-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { GlobalContactDataService } from 'app/main/content/global-contact/global-contact-data.service';

@Component({
  selector: 'irms-important-fields',
  templateUrl: './important-fields.component.html',
  styleUrls: ['./important-fields.component.scss'],
  animations: fuseAnimations
})
export class ImportantFieldsComponent extends SectionCreateComponent<EventGuestModel> implements OnInit {
  selectFields = this.fb.group({
    selectedReservedFields: [],
  });
  selectedReservedFields = ['title', 'fullName', 'documentNumber', 'organization', 'position', 'email', 'mobileNumber', 'gender'];

  reservedFields: any = [];
  customFieldNames = [];
  custList: any = [];
  selectedCustomFields = [];
  submitting: boolean;
  filter = '';
  loadedAndPreSelected = false;
  listId: any;
  filteredReservedFields = [];
  filteredCustomFields = [];
  eventId: any;

  allFields = [];

  importantFields = [];
  dialogConfig = sideDialogConfig;


  constructor(
    private fb: FormBuilder,
    private formService: ImportAnalysisFormService,
    public sectionService: EventGuestService,
    protected dataService: EventGuestDataService,
    protected router: Router,
    public route: ActivatedRoute,
    public dialog: MatDialog,
    public animDialog: DialogAnimationService,
    public customFieldDataService: GlobalContactDataService) {
    super(Section.EventGuests, sectionService, dataService, router);
  }

  get f(): any { return this.selectFields.controls; }

  get t(): any { return this.f.custFields as FormArray; }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.listId = params['glid'];
      this.sectionService.listId = this.listId;
      this.eventId = params['id'];
    });
    this.dataService.getAllCustomFields({ fields: [] })
      .subscribe(data => {
        data.forEach(x => {
          this.custList.push({
            value: x.id,
            selected: false,
            label: x.name,
            isRequired: false,
            type: 1, // Field type custom
            customFieldType: x.customFieldType,
            maxValue: x.maxValue,
            minValue: x.minValue
          });
        });

        //  Get previously saved important fields for the list
        this.dataService.getSelectedImportantFields(this.listId)
          .subscribe(fields => {
            //  Handle Reserved Fields
            const isDefaultSelection = fields.reservedFields.length === 0;
            this.formService.reservedFieldsWithValidations.forEach(x => {
              let isResSel = false;
              let isRequired = false;
              if (isDefaultSelection) {
                isResSel = this.selectedReservedFields.indexOf(x.name) > -1;
                isRequired = true;
              } else {
                const tempSelected = fields.reservedFields.find(r => r.value === x.name);
                if (tempSelected) {
                  isResSel = true;
                  isRequired = tempSelected.isRequired;
                }
              }
              this.reservedFields.push({
                value: x.name,
                selected: isResSel,
                label: x.label,
                isRequired: isRequired,
                type: 0,  // Field type reserved
                // Added just to balance json object type with custom fields
                customFieldType: 0,
                maxValue: null,
                minValue: null
              });
            });

            //  Handle Custom Fields
            if (fields.customsFields.length > 0) {
              fields.customsFields.forEach(x => {
                const tempSelected = this.custList.find(f => f.value === x.value);
                if (tempSelected) {
                  tempSelected.selected = true;
                  tempSelected.isRequired = x.isRequired;
                }
              });
            }
            const all = this.reservedFields.concat(this.custList);
            this.allFields = all.filter(x => !x.selected);
            this.importantFields = all.filter(x => x.selected);
            this.loadedAndPreSelected = true;
            //  this.cdRef.detectChanges();
          });
      });
  }

  drop(event: CdkDragDrop<string[]>, dest): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      if (dest === 'imp') {
        this.importantFields[event.currentIndex].isRequired = true;
        this.importantFields[event.currentIndex].selected = true;
      } else {
        this.allFields[event.currentIndex].isRequired = false;
        this.allFields[event.currentIndex].selected = false;
      }
    }
  }

  confirm(): void {
    this.submitting = true;
    let model = {
      listId: this.sectionService.listId,
      eventId: this.sectionService.eventId,
      listAnalysisStep: 1,
      listAnalysisPageNo: null,
      listAnalysisPageSize: null
    }
    this.dataService.setStep(model).subscribe(res => {
      if (res) {
        this.dataService.setSelectedImportantFields({
          reservedFields: this.importantFields.filter(f => f.selected && f.type === 0).map(sel => ({ value: sel.value, isRequired: sel.isRequired })),
          customFields: this.importantFields.filter(f => f.selected && f.type === 1).map(sel => ({ value: sel.value, isRequired: sel.isRequired })),
          listId: this.listId
        })
          .subscribe(success => {
            this.submitting = false;
            const selectedReservedFields = this.importantFields.filter(f => f.selected && f.type === 0).map(sel => ({ value: sel.value, isRequired: sel.isRequired }));
            const selectedCustomFields = this.importantFields.filter(f => f.selected && f.type === 1).map(sel => ({
              value: sel.value,
              customFieldType: sel.customFieldType,
              maxValue: sel.maxValue,
              minValue: sel.minValue,
              isRequired: sel.isRequired,
              label: sel.label
            }
            ));
            this.formService.setSelectedFields({ reserved: selectedReservedFields, custom: selectedCustomFields });
            this.formService.finalStep = true;
            this.formService.setStepperIndex(1);
          });
      }
    },err => {this.submitting = false})


  }

  cancel(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
    dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
    dialogRef.componentInstance.confirmButton = 'Leave page';
    dialogRef.componentInstance.cancelButton = 'Stay';
    dialogRef.componentInstance.confirmMessage = 'Are you sure you want to cancel? Cancel at this step will reset your content.\n All changes will be lost.';
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.dataService.cancelListAnalysis({ guestListId: this.sectionService.listId, eventId: this.eventId }).subscribe(x => {
          if (x) {
            this.formService.finalStep = false;
            this.router.navigate(['../'], { relativeTo: this.route });
          }
        });

      }
    });
  }

  createCustomField() {
    let dialogConfig: any = this.dialogConfig;
    const dialogRef = this.animDialog.open(AddCustomFieldModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.customFieldDataService.getAllCustomFields()
        .subscribe(res => {
          if (result) {
            let field = res.find(x => x.name == result.name)
            this.importantFields.push({
              value: field.id,
              selected: true,
              label: field.name,
              isRequired: true,
              maxValue: res.maxValue,
              minValue: res.minValue,
              customFieldType: res.customFieldType,
              type: 1
            });
          }
        });
    });
  }

  searchQueryChange(data: string): void {
    this.filter = data;
  }
}
