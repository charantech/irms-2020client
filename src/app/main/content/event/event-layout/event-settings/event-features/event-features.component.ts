import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { EventDataService } from '../../../event-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { EventService } from '../../../event.service';

@Component({
  selector: 'irms-event-features',
  templateUrl: './event-features.component.html',
  styleUrls: ['./event-features.component.scss']
})
export class EventFeaturesComponent implements OnInit {
  editFeatureMode = false;
  @Output() public onEditMode: EventEmitter<boolean> = new EventEmitter();
  public featuresList: any[];
  eventId: string;
  loading = false;

  public featuresForm = this.fb.group({
    features: ['', [Validators.required]]
  });
  constructor(protected fb: FormBuilder,
              private sectionService: EventService,
              private dataService: EventDataService,
              protected route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];
      this.getEventFeatures();
    });
  }

  getEventFeatures() {
    this.dataService.getEventFeatures(this.eventId).subscribe(data => {
      this.featuresList = data;
      this.patchModelInForms();
    });
  }

  private patchModelInForms() {
    this.featuresForm.patchValue(this.featuresList);
  }

  /// Handle Edit Features Tab 
  public editForm(): void {
    this.editFeatureMode = true;
    this.onEditMode.emit(true);
  }

  public viewEventFeatures(): void {
    this.editFeatureMode = false;
    this.onEditMode.emit(false);
  }

  ////////////////// Select Features////////////////////
  public featuresChange(event): void {
    this.featuresForm.controls['features'].setValue(event);
  }

  ////////////////// Save Changes ////////////////////
  public updateFeatures(): void {
    if (this.featuresForm.valid) {
      const model = this.sectionService.trimValues(this.featuresForm.value);
      model.id = this.eventId;

      this.loading = true;
      this.dataService.updateEventFeatures(model).subscribe(() => {
        this.loading = false;
        this.viewEventFeatures();
        this.getEventFeatures();
      }, () => {
        this.loading = false;
      }
      );
    }
  }
}
