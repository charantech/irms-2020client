import { Component, OnInit, OnDestroy } from '@angular/core';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { EventCampaignModel } from '../../event-campaign.model';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { CampaignInvitationPendingService } from './campaign-invitation-pending.service';
import { CampaignInvitationPendingDataService } from './campaign-invitation-pending-data.service';
import { Section } from 'app/constants/constants';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { EventCampaignEditService } from '../event-campaign-edit.service';
import { CampaignConfigurationsDataService } from '../campaign-configurations/campaign-configurations-data.service';

@Component({
  selector: 'irms-campaign-invitation-pending',
  templateUrl: './campaign-invitation-pending.component.html',
  styleUrls: ['./campaign-invitation-pending.component.scss']
})
export class CampaignInvitationPendingComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit {

  nodes = [];
  campaignId: any;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    private campaignDataService: CampaignConfigurationsDataService,
    private campaignService: EventCampaignEditService,
    public sectionService: CampaignInvitationPendingService,
    protected dataService: CampaignInvitationPendingDataService) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.sectionService.setFilter({ campaignId: this.campaignId });
      this.updateNodes();
      this.reloadCampaignName();
    });
  }
  
  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }

  createNode() {
    this.dataService.create({ eventCampaignId: this.campaignId, sortOrder: this.nodes.length + 1, title: `Message ${this.nodes.length + 1}` }).subscribe(id => {
      this.nodes.push({ id: id });
      this.updateNodes();
    });
  }

  updateNodes() {
    this.dataService.getList(this.campaignId).subscribe(result => {
      this.nodes = result;
    })
  }

  @PopUp('Are you sure want to duplicate a reminder?')
  copyNode(event) {
    this.dataService.duplicate({ invitationId: event })
      .subscribe(() => {
        this.updateNodes();
      })
  }

  @PopUp('Are you sure want to delete a reminder?')
  deleteNode(event) {
    this.dataService.delete({ id: event })
      .subscribe(() => {
        this.updateNodes();
        this.router.navigate(['../invitation-pending'], { relativeTo: this.route })
      })
  }
}
