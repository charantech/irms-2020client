import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-event-guest-associated-lists',
  templateUrl: './event-guest-associated-lists.component.html',
  styleUrls: ['./event-guest-associated-lists.component.scss'],
  animations: fuseAnimations
})
export class EventGuestAssociatedListsComponent implements OnInit {
  guestId;
  assocList = [];
  constructor(protected fb: FormBuilder,
              public sectionService: EventGuestService,
              protected dataService: EventGuestDataService,
              protected route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.guestId = params['gid'];
      const filter = {
        guestId: params['gid']
      };
      this.dataService.getGuestAssociatedLists(filter).subscribe(data => {
        this.assocList = data;
      });
    });
  }
}
