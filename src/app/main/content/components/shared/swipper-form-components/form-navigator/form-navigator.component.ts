import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'irms-form-navigator',
  templateUrl: './form-navigator.component.html',
  styleUrls: ['./form-navigator.component.scss']
})
export class FormNavigatorComponent implements OnInit {
  @Input() theme: any;
  @Input() isArabic: boolean;
  @Output() scrollNext = new EventEmitter<any>();
  @Output() scrollPrevious = new EventEmitter<any>();
  @Input() mockup = false;
  @Input()  progress: Subject<any>;
  fraction = 0;
  determinate: any;
  progressPercent = 0;

  constructor() { }

  ngOnInit(): void{
    this.progress.subscribe(event => {
      this.fraction = event.done / event.total;
      this.progressPercent = this.fraction * 100;
    });
  }
  gotoNext(): void{
    this.scrollNext.emit();
  }
  gotoPrevious(): void{
    this.scrollPrevious.emit();
  }
}
