import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsCustomFieldsComponent } from './global-guest-details-custom-fields.component';

describe('GlobalGuestDetailsCustomFieldsComponent', () => {
  let component: GlobalGuestDetailsCustomFieldsComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsCustomFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsCustomFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsCustomFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
