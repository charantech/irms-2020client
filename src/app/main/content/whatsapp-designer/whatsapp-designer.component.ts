import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'irms-whatsapp-designer',
  templateUrl: './whatsapp-designer.component.html',
  styleUrls: ['./whatsapp-designer.component.scss']
})
export class WhatsappDesignerComponent implements OnInit {

  templateId;
  constructor(private dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute ) {
      this.activatedRoute.params.subscribe(params => {
        this.templateId = params['tempId'];
      });
    }

  ngOnInit() {
  }

  sendTestWhatsapp(): void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '60vw';
    dialogConfig.maxWidth = '80vw';
    dialogConfig.data = {
      invitationId: this.templateId
    }
  }

}
