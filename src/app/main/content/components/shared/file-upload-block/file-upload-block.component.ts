import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-file-upload-block',
  templateUrl: './file-upload-block.component.html',
  styleUrls: ['./file-upload-block.component.scss']
})
export class FileUploadBlockComponent implements OnInit {

  @Input() public inputControl: FormControl;
  @Input()  fileTypes: any;
  @Input() maxFileSize;
  @Output() update = new EventEmitter<string>();
  @Input() label = 'Choose file or drag here';
  @Input() public id = '';
  file: any;
  fileName: any;

  constructor() { }

  ngOnInit(): void{
  }

  uploadFile(event): void {
    // check if file size is valid' 1 MB = 1048576 Bytes
    if (event && event[0] && event[0].size <= (this.maxFileSize * 1048576)) {
      this.fileName = event[0].name;
      this.file = event[0];
      this.inputControl.setValue(event[0]);
    }
  }
  deleteAttachment(): void {
    this.file = null;
  }


}
