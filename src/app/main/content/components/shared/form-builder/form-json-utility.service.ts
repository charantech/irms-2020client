import { Injectable } from '@angular/core';
import { customFieldType, lookupReservedFields, reservedFieldsWithValidation } from 'app/constants/constants';
import { AppStateService } from 'app/main/content/services/app-state.service';
import * as uuid from 'uuid';
import { questionObj } from '../swipper-form-components/form-constants/form-questions';

@Injectable({
  providedIn: 'root'
})
export class FormJsonUtilityService {
  private reserveFieldsValidation = reservedFieldsWithValidation;
  private lookups = lookupReservedFields;
  private customFieldType = customFieldType;
  public formBase = {
    campaignInvitationId: '9f70e419-cc58-4ed6-975b-dd3b491aef79',
    welcomeHtml: '{"content":"<p style=\\"text-align: left;\\"><span style=\\"font-family: arial, helvetica, sans-serif;\\">You are invited to</span></p>\\n      <h2 style=\\"text-align: left;\\"><span style=\\"background-color: #236FA1; color: #ECF0F1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: left;\\"><span style=\\"color: #000000; font-family: tahoma, arial, helvetica, sans-serif;\\">on 23rd April 2020, 6:00 PM</span></p>\\n      <h5 style=\\"text-align: left;\\"><span style=\\"color: #34495E;\\"><em><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">\\n      Please let us know if you will be joining us</span></em></span></h5>","contentArabic":"<p style=\\"text-align: left;\\"><span style=\\"font-family: arial, helvetica, sans-serif;\\">You are invited to</span></p>\\n      <h2 style=\\"text-align: left;\\"><span style=\\"background-color: #236FA1; color: #ECF0F1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: left;\\"><span style=\\"color: #000000; font-family: tahoma, arial, helvetica, sans-serif;\\">on 23rd April 2020, 6:00 PM</span></p>\\n      <h5 style=\\"text-align: left;\\"><span style=\\"color: #34495E;\\"><em>\\n      <span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">Please let us know if you will be joining us</span></em></span></h5>","button":"Proceed","buttonArabic":"تقدم"}',
    submitHtml: '{"submitMessage":"Everything seems good,\\nYou can submit now!","submitMessageArabic":"جميع البيانات صحيحة،\\n قم بتقديم الطلب الآن!","submitButton":"Submit","submitButtonArabic":"تقدم"}',
    thanksHtml: '{"content":"<p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">Thank you for your response!</span></p>\\n      <p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">We will see you at the&nbsp;</span></p>\\n      <h2 style=\\"text-align: center;\\"><span style=\\"background-color: #236FA1; color: #ECF0F1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: center;\\">&nbsp;</p>","contentArabic":"<p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">Thank you for your response!</span></p>\\n      <p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">We will see you at the&nbsp;</span></p>\\n      <h2 style=\\"text-align: center;\\"><span style=\\"background-color: #236FA1; color: #ECF0F1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: center;\\">&nbsp;</p>","button":null,"buttonArabic":null}',
    formTheme: '{"buttons":{"background":"#576268","text":"#fff"},"questions":{"color":"#576268"},"answers":{"color":"#576268"},"background":{"color":"rgb(255,255,255)","style":""}}',
    formSettings: '{"title":"Information Form","defaultLanguage":"en","languageOption":false}',
    questionJson: []
  };
  caseFix = ['organization', 'mobilenumber', 'gender', 'nationality'];
  docs = []
  issuingCountryIds = []
  nats = []


  constructor(public appStateService: AppStateService) {
    this.appStateService.GetDocumentTypes().subscribe(res => this.docs = res);
    this.appStateService.getCountries().subscribe(res => this.issuingCountryIds = res);
    this.appStateService.GetNationalities().subscribe(res => this.nats = res);

  }

  getMissingRFIFormQuestions(importantQuestions, formQuestions: Array<any> = null): any {
    const customFieldNameOnly = importantQuestions.custom.map(c => c.label);
    const isExistingForm = formQuestions && formQuestions.length > 0;
    let isChanged = false;
    if (isExistingForm) {
      const importantFieldNames = importantQuestions.reserved.map(r => r.value).concat(customFieldNameOnly);
      const importantFieldsExisting = [];
      const toDeleteFields = [];
      let addedAsImportant = [];
      formQuestions.forEach((q, i) => {
        const parsedQ = JSON.parse(q.question);
        if (parsedQ.mapping) {
          const isReserved = importantQuestions.reserved.find(r => r.value === q.mappedField);
          const isCustom = importantQuestions.custom.find(c => c.label === q.mappedField);
          if (isReserved) {
            parsedQ.required = isReserved.isRequired;
          } else if (isCustom) {
            parsedQ.required = isCustom.isRequired;
          }
          if (parsedQ.important && importantFieldNames.includes(parsedQ.mappingField)) {
            importantFieldsExisting.push(parsedQ.mappingField);
          } else if (!parsedQ.important && importantFieldNames.includes(parsedQ.mappingField)) {
            parsedQ.important = true;
            isChanged = true;
            importantFieldsExisting.push(parsedQ.mappingField);
            addedAsImportant.push(parsedQ.mappingField);
          } else if (parsedQ.important && !importantFieldNames.includes(parsedQ.mappingField)) {
            parsedQ.important = false;
            toDeleteFields.push(parsedQ.mappingField);
            isChanged = true;
          }
          q.question = JSON.stringify(parsedQ);
        }
      });
      // Delete any important fields in form that are no more important
      formQuestions = formQuestions.filter(q => !toDeleteFields.includes(q.mappedField));
      // Find newly added important fields and append to form
      const difference = importantFieldNames.filter(x => !importantFieldsExisting.includes(x));
      if (difference && difference.length > 0) {
        isChanged = true;
        addedAsImportant = addedAsImportant.concat(difference);
        difference.forEach(q => {
          const isReserved = importantQuestions.reserved.find(r => r.value === q);
          const isCustom = importantQuestions.custom.find(c => c.label === q);
          if (isReserved) {
            formQuestions.push(this.getQuestion(isReserved, true, formQuestions.length));
          } else if (isCustom) {
            formQuestions.push(this.getQuestion(isCustom, false, formQuestions.length));
          }
        });
      }
      return ({
        questions: formQuestions,
        isFieldSetChanged: isChanged,
        deletedFields: toDeleteFields,
        addedFields: addedAsImportant
      });
    } else {
      const questions4ImpFields = [];
      // console.log(importantQuestions)
      importantQuestions.reserved.forEach((q, i) => {
        questions4ImpFields.push(this.getQuestion(q, true, i + 1));
      });
      importantQuestions.custom.forEach((q, i) => {
        questions4ImpFields.push(this.getQuestion(q, false, importantQuestions.reserved.length > 0 ? questions4ImpFields.length : i + 1));
      });
      return {
        questions: questions4ImpFields,
        isFieldSetChanged: isChanged
      };
    }
  }

  getQuestion(question, isReserved = false, index = 0): any {
    if (isReserved) {
      const questionValidation = this.reserveFieldsValidation.find(x => x.name.toLowerCase() === question.value.toLowerCase());
      let questTypeName = null;
      if (questionValidation.type === this.customFieldType.text || questionValidation.type === this.customFieldType.number) {
        questTypeName = 'singleLineText';
      } else if (questionValidation.type === this.customFieldType.date) {
        questTypeName = 'date';
      } else if (questionValidation.type === this.customFieldType.email) {
        questTypeName = 'email';
      } else if (questionValidation.type === this.customFieldType.phone) {
        questTypeName = 'phone';
      }
      if (this.lookups.includes(question.value.toLowerCase())) {
        questTypeName = 'select';
      }
      const questionObject = JSON.parse(JSON.stringify(questionObj[questTypeName]));
      questionObject.id = uuid.v4();
      questionObject.headline = `Enter ${question.value}`;
      questionObject.mapping = true;
      questionObject.required = question.isRequired;
      questionObject.mappingField = question.value;
      questionObject.sortOrder = index;
      questionObject.important = true;

      if (questionValidation.type === this.customFieldType.number) {
        questionObject.isNumber = true;
      }
      if (questionValidation.type === this.customFieldType.date) {
        questionObject.maxDate = questionValidation.maxLength;
        questionObject.minDate = questionValidation.minLength;
      } else {
        questionObject.maxLength = questionValidation.maxLength;
        questionObject.minLength = questionValidation.minLength;
      }
      if (questTypeName === 'select' || questTypeName === 'selectSearch') {
        questionObject.choices = [];
        if (this.lookups.includes(question.value.toLowerCase())) {
          // console.log("@@@#@#@@#@#")
          switch (question.value.toLowerCase()) {
            case 'gender':
              // console.log('gender')
              questionObject.choices = [
                {
                  label: 'Male',
                  labelArabic: 'ذكر',
                  value: 0
                },
                {
                  label: 'Female',
                  labelArabic: 'انثى',
                  value: 1
                }
              ];
              break;
            case 'documenttypeid':
              // console.log('doc')
              this.docs.forEach(element => {
                questionObject.choices.push({
                  label: element.name,
                  labelArabic: element.name,
                  value: element.id
                });
              });
              break;
            case 'issuingcountryid':
              // console.log('iss')
                this.issuingCountryIds.forEach(element => {
                  questionObject.choices.push({
                    label: element.value,
                    labelArabic: element.value,
                    value: `${element.id}`
                  });
                });
              break;
            case 'nationalityid':
              // console.log('nat')
                this.nats.forEach(element => {
                  questionObject.choices.push({
                    label: element.nationality,
                    labelArabic: element.nationality,
                    selected: false,
                    value: `${element.id}`
                  });
                });
              break;
          }
        }
      }

      return {
        id: questionObject.id,
        question: JSON.stringify(questionObject),
        mapped: questionObject.mapping,
        mappedField: questionObject.mappingField,
      };
    } else {
      let questTypeName = null;
      if (question.customFieldType === this.customFieldType.text || question.customFieldType === this.customFieldType.number) {
        questTypeName = 'singleLineText';
      } else if (question.customFieldType === this.customFieldType.date) {
        questTypeName = 'date';
      } else if (question.customFieldType === this.customFieldType.email) {
        questTypeName = 'email';
      } else if (question.customFieldType === this.customFieldType.phone) {
        questTypeName = 'phone';
      }
      const questionObject = JSON.parse(JSON.stringify(questionObj[questTypeName]));
      questionObject.id = uuid.v4();
      questionObject.headline = `Enter ${question.label}`;
      questionObject.mapping = true;
      questionObject.required = question.isRequired;
      questionObject.mappingField = question.label;
      questionObject.sortOrder = index;
      questionObject.important = true;

      if (question.customFieldType === this.customFieldType.number) {
        questionObject.isNumber = true;
      }
      if (question.customFieldType === this.customFieldType.date) {
        questionObject.maxDate = question.maxValue;
        questionObject.minDate = question.minValue;
      } else {
        questionObject.maxLength = question.maxValue;
        questionObject.minLength = question.minValue;
      }
      if (questTypeName === 'select' || questTypeName === 'selectSearch') {
        questionObject.choices = [];
        if (this.lookups.includes(question.value.toLowerCase())) {
          // console.log("@@@#@#@@#@#")
          switch (question.value.toLowerCase()) {
            case 'gender':
              // console.log('gender')
              questionObject.choices = [
                {
                  label: 'Male',
                  labelArabic: 'ذكر',
                  value: 0
                },
                {
                  label: 'Female',
                  labelArabic: 'انثى',
                  value: 1
                }
              ];
              break;
            case 'documenttypeid':
              // console.log('doc')
              this.docs.forEach(element => {
                questionObject.choices.push({
                  label: element.name,
                  labelArabic: element.name,
                  value: element.id
                });
              });
              break;
            case 'issuingcountryid':
              // console.log('iss')
                this.issuingCountryIds.forEach(element => {
                  questionObject.choices.push({
                    label: element.value,
                    labelArabic: element.value,
                    value: `${element.id}`
                  });
                });
              break;
            case 'nationalityid':
              // console.log('nat')
                this.nats.forEach(element => {
                  questionObject.choices.push({
                    label: element.nationality,
                    labelArabic: element.nationality,
                    selected: false,
                    value: `${element.id}`
                  });
                });
              break;
          }
        }
      }
      return {
        id: questionObject.id,
        question: JSON.stringify(questionObject),
        mapped: questionObject.mapping,
        mappedField: questionObject.mappingField,
      };
    }
  }


}
