import { TestBed } from '@angular/core/testing';

import { EventData.DataService } from './event-data.data.service';

describe('EventData.DataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventData.DataService = TestBed.get(EventData.DataService);
    expect(service).toBeTruthy();
  });
});
