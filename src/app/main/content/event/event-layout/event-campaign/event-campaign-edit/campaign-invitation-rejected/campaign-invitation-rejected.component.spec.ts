import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignInvitationRejectedComponent } from './campaign-invitation-rejected.component';

describe('CampaignInvitationRejectedComponent', () => {
  let component: CampaignInvitationRejectedComponent;
  let fixture: ComponentFixture<CampaignInvitationRejectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignInvitationRejectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignInvitationRejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
