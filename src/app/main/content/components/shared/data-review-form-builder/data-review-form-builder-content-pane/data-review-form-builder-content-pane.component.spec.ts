import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderContentPaneComponent } from './data-review-form-builder-content-pane.component';

describe('DataReviewFormBuilderContentPaneComponent', () => {
  let component: DataReviewFormBuilderContentPaneComponent;
  let fixture: ComponentFixture<DataReviewFormBuilderContentPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormBuilderContentPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormBuilderContentPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
