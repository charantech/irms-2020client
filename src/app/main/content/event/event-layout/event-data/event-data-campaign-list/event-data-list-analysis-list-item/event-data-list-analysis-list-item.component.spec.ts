import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataListAnalysisListItemComponent } from './event-data-list-analysis-list-item.component';

describe('EventDataListAnalysisListItemComponent', () => {
  let component: EventDataListAnalysisListItemComponent;
  let fixture: ComponentFixture<EventDataListAnalysisListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataListAnalysisListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataListAnalysisListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
