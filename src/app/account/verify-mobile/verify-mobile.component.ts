import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CurrentUserService } from 'app/main/content/services/current-user.service';
import { AccountService } from '../account.service';
import { TokenService } from 'app/auth/services/token.service';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-verify-mobile',
  templateUrl: './verify-mobile.component.html',
  styleUrls: ['./verify-mobile.component.scss'],
  animations: fuseAnimations
})
export class VerifyMobileComponent implements OnInit {
  logoLink: any;
  loading = false;
  logoLoad = true;

  private returnUrl: string;
  user: any;
  phoneNum = '+966505458789';
  enableResend: boolean;


  constructor(
    private router: Router,
    private currentUserService: CurrentUserService,
    private accountService: AccountService,
    private tokenService: TokenService,
    private route: ActivatedRoute,
    private  appState: AppStateService) {
    
  }
  ngOnInit(): void {
    
    this.currentUserService.getCurrentUser().subscribe((user) => {
      this.user = user;
      this.phoneNum = '*'.repeat(this.user.phoneNo.length - 3) + '' + this.user.phoneNo.substr(this.user.phoneNo.length - 3);
    });
    this.logoLoad = true;
    this.appState.getTenantLogo().subscribe(result => {
        this.logoLink = result.logoPath;
        this.logoLoad = false;
      }, error => {
        this.logoLoad = false;
        this.logoLink = null;
      });
  }

}
