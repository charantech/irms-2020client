import { BaseModel } from '../components/shared/section/base.model';

export class WhatsappDesignerModel extends BaseModel {
    name: string;
}
