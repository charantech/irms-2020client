import { TestBed } from '@angular/core/testing';

import { GlobalContactService } from './global-contact.service';

describe('GlobalContactService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalContactService = TestBed.get(GlobalContactService);
    expect(service).toBeTruthy();
  });
});
