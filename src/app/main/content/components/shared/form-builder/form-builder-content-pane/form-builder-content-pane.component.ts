import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormService } from '../../form/form.service';
import { tinyMceKey, sideDialogConfig, customFieldType, reservedFields, reservedFieldsWithValidation, lookupReservedFields } from 'app/constants/constants';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { QuestionTypeList, questionObj, QuestionColorCode, QuestionIcon } from '../../swipper-form-components/form-constants/form-questions';
import * as uuid from 'uuid';
import { cloneDeep, lastIndexOf } from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { GlobalContactDataService } from 'app/main/content/global-contact/global-contact-data.service';
import { map } from 'rxjs/operators/map';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddCustomFieldModalComponent } from '../../add-custom-field-modal/add-custom-field-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { FormBuilderService } from '../form-builder.service';
@Component({
  selector: 'irms-form-builder-content-pane',
  templateUrl: './form-builder-content-pane.component.html',
  styleUrls: ['./form-builder-content-pane.component.scss']
})
export class FormBuilderContentPaneComponent implements OnInit, OnDestroy {
  customFieldForm: FormGroup;
  public loader = false;
  disabled = false;
  required = false;
  type: any;

  customFieldType = customFieldType;
  currentEditor;
  questionTypeList = QuestionTypeList;
  questionColorCode = QuestionColorCode;
  questionIcon = QuestionIcon;
  tinyMceKey = tinyMceKey;
  tinyMceConfig = {
    menubar: false,
    statusbar: false,
    branding: false,
    height: 400,
    inline: false,
    plugins: [
      'advlist charmap lists link image imagetools directionality',
      'searchreplace visualblocks visualchars media table paste pagebreak code emoticons'
    ],
    toolbar_mode: 'floating',
    toolbar: 'insertComponent | styleselect fontselect fontsizeselect |bold italic underline | superscript subscript | forecolor backcolor | alignleft aligncenter alignright alignjustify | numlist bullist | image link emoticons | table hr pagebreak code',
    image_advtab: true,
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
    paste_data_images: !0,
    importcss_append: !0,
    images_upload_handler(e, t, a): any {
      t('data:' + e.blob().type + ';base64,' + e.base64());
    },
  };
  @Input() defaultLanguage = 'en';
  @Input() languageOption = false;
  @Input() welcome: FormGroup;
  @Input() form: FormArray;
  @Input() submit: FormGroup;
  @Input() thanks: FormGroup;
  currentQuestion: FormGroup;
  currentType: string;
  guestProfileFields: string[];
  defaultProfileFields = ['Title', 'FullName', 'PrefferedName', 'Gender', 'Email', 'AlternateEmail', 'MobileNumber', 'SecondaryMobileNumber', 'Nationality', 'Position'];
  lookups = lookupReservedFields;
  // TODO: make the attribute name (minLength,MaxVal) unified across customfields & reserved fields
  allProfileFields: any = {
    singleLineText: [],
    date: [],
    email: [],
    phone: []
  };
  reservedFieldsWithValidation: any = reservedFieldsWithValidation;
  customs = [];

  CustFieldValidator: any;
  selectedFields = [];
  subscriptions: Subscription[] = [];

  constructor(protected previewService: FormService,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private customFieldDataService: GlobalContactDataService,
              public animDialog: DialogAnimationService,
              public service: FormBuilderService
  ) { }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.allProfileFields.singleLineText.forEach(element => element.isSelected = false);
    this.allProfileFields.date.forEach(element => element.isSelected = false);
    this.allProfileFields.email.forEach(element => element.isSelected = false);
    this.allProfileFields.phone.forEach(element => element.isSelected = false);
    this.service.selectedFieldArr = [];
  }

  ngOnInit(): void {
    this.reservedFieldsWithValidation.forEach(element => {
      element.isSelected = false;
      if (element.type === 0 || element.type === 1) {
        this.allProfileFields.singleLineText.push(element);
      } else if (element.type === 2) {
        this.allProfileFields.date.push(element);
      } else if (element.type === 3) {
        this.allProfileFields.email.push(element);
      } else if (element.type === 4) {
        this.allProfileFields.phone.push(element);
      }
    });
    this.form.valueChanges.subscribe(val => {
      this.service.setFormContent(val);
    });
    this.subscriptions.push(this.service.getIsFormLoaded().subscribe(loaded => {
      if (loaded) {
        this.customFieldDataService.getAllCustomFields().subscribe(x => {
          this.CustFieldValidator = x;
          this.CustFieldValidator.forEach(element => {
            element.isSelected = false;
            if (element.customFieldType === 0) {
              this.allProfileFields.singleLineText.push(element);
            } else if (element.customFieldType === 1) {
              this.allProfileFields.singleLineText.push(element);
            } else if (element.customFieldType === 2) {
              this.allProfileFields.date.push(element);
            } else if (element.customFieldType === 3) {
              this.allProfileFields.email.push(element);
            } else if (element.customFieldType === 4) {
              this.allProfileFields.phone.push(element);
            }

          });
          const m = this.service.selectedFieldArr;
          m.forEach(element => {
            this.allProfileFields[element.type === 'select' ? 'singleLineText' : element.type].find(f => f.name.toLowerCase() === element.field.toLowerCase()).isSelected = true;
          });
        });
        this.customFieldFormInit();
      }
    }));

  }

  confirmAddCreateCust(mapping, i, val): void {
    this.loader = true;
    const value = this.customFieldForm.value;
    // console.log(value)
    let min = null;
    let max = null;
    if (value.type === 'text') {
      min = value.text.min;
      max = value.text.max;
    }
    if (value.type === 'number') {
      min = value.number.min;
      max = value.number.max;
    }

    if (value.type === 'date') {
      if (value.date.min) {
        const minDate = new Date(value.date.min);
        min = minDate.getTime();
      }
      if (value.date.max) {
        const maxDate = new Date(value.date.max);
        max = maxDate.getTime();
      }
    }

    const newField = {
      name: value.name,
      customFieldType: value.type,
      minValue: min,
      maxValue: max
    };
    this.customFieldDataService.createCustomField(newField).subscribe(resp => {
      if (resp) {
        this.loader = false;
        this.customs[this.customs.indexOf(i)] = -1;
        this.customFieldDataService.getAllCustomFields()
          .subscribe(result => {
            const lastItemIndex = result.length - 1;
            result[lastItemIndex].isSelected = true;
            if (result[lastItemIndex].customFieldType === customFieldType.text) {
              this.allProfileFields.singleLineText.push(result[lastItemIndex]);
              // this.allProfileFields.singleLineText[result.name].isNumber = false;
            } else if (result[lastItemIndex].customFieldType === customFieldType.number) {
              this.allProfileFields.singleLineText.push(result[lastItemIndex]);
              // this.allProfileFields.singleLineText[result.name].isNumber = true;
            } else if (result[lastItemIndex].customFieldType === customFieldType.date) {
              this.allProfileFields.date.push(result[lastItemIndex]);
            } else if (result[lastItemIndex].customFieldType === customFieldType.email) {
              this.allProfileFields.email.push(result[lastItemIndex]);
            } else if (result[lastItemIndex].customFieldType === customFieldType.phone) {
              this.allProfileFields.phone.push(result[lastItemIndex]);
            }
            this.questions.controls[i].controls.mappingField.setValue(result[lastItemIndex].name);
          });
      }
    }, error => {
      this.loader = false;
    });

  }
  invalidate(type: string): void {
    switch (type) {
      case 'text':

        this.form.get(['number', 'min']).setValue('');
        this.form.get(['number', 'max']).setValue('');
        this.form.get(['date', 'min']).setValue('');
        this.form.get(['date', 'max']).setValue('');
        break;
      case 'number':

        this.form.get(['text', 'min']).setValue('');
        this.form.get(['text', 'max']).setValue('');
        this.form.get(['date', 'min']).setValue('');
        this.form.get(['date', 'max']).setValue('');
        break;

      case 'date':
        this.form.get(['number', 'min']).setValue('');
        this.form.get(['number', 'max']).setValue('');
        this.form.get(['text', 'min']).setValue('');
        this.form.get(['text', 'max']).setValue('');
        break;

      default:
        break;
    }
  }

  customFieldFormInit() {
    this.customFieldForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      type: ['text', [Validators.required]],
      text: this.fb.group({
        min: [''],
        max: ['']
      }),
      date: this.fb.group({
        min: [''],
        max: ['']
      }),
      number: this.fb.group({
        min: [''],
        max: ['']
      })
    });

    this.customFieldForm.get('type').valueChanges.subscribe(x => {
      this.invalidate(x);
    });

    this.customFieldForm.get(['text', 'min']).valueChanges.subscribe(value => {
      const maxValue = this.customFieldForm.get(['text', 'max']).value;

      if (maxValue && maxValue < value) {
        this.customFieldForm.get(['text', 'min']).setErrors({ rangeError: true });
      }
      else {
        this.customFieldForm.get(['text', 'max']).setErrors(null);
        this.customFieldForm.get(['text', 'min']).setErrors(null);
      }
    });

    this.customFieldForm.get(['text', 'max']).valueChanges.subscribe(value => {
      const minValue = this.customFieldForm.get(['text', 'min']).value;
      if (minValue && minValue > value) {
        this.customFieldForm.get(['text', 'max']).setErrors({ rangeError: true });
      }
      else {
        this.customFieldForm.get(['text', 'min']).setErrors(null);
        this.customFieldForm.get(['text', 'max']).setErrors(null);
      }
    });
    this.customFieldForm.get(['number', 'min']).valueChanges.subscribe(value => {
      const maxValue = this.customFieldForm.get(['number', 'max']).value;
      if (maxValue && maxValue < value) {
        this.customFieldForm.get(['number', 'min']).setErrors({ rangeError: true });
      }
      else {
        this.customFieldForm.get(['number', 'max']).setErrors(null);
        this.customFieldForm.get(['number', 'min']).setErrors(null);
      }
    });
    this.customFieldForm.get(['number', 'max']).valueChanges.subscribe(value => {
      const minValue = this.customFieldForm.get(['number', 'min']).value;
      if (minValue && minValue > value) {
        this.customFieldForm.get(['number', 'max']).setErrors({ rangeError: true });
      }
      else {
        this.customFieldForm.get(['number', 'min']).setErrors(null);
        this.customFieldForm.get(['number', 'min']).setErrors(null);
      }
    });

    this.customFieldForm.get(['date', 'min']).valueChanges.subscribe(value => {
      const minDate = new Date(value);
      const maxDate = new Date(this.customFieldForm.get(['date', 'max']).value);
      if (maxDate && (maxDate.getTime() - minDate.getTime() < 0)) {
        this.customFieldForm.get(['date', 'min']).setErrors({ rangeError: true });
      }
      else {
        this.customFieldForm.get(['date', 'max']).setErrors(null);
        this.customFieldForm.get(['date', 'min']).setErrors(null);
      }
    });

    this.customFieldForm.get(['date', 'max']).valueChanges.subscribe(value => {
      const minDate = new Date(this.customFieldForm.get(['date', 'min']).value);
      const maxDate = new Date(value);
      if (minDate && (maxDate.getTime() - minDate.getTime() < 0)) {
        this.customFieldForm.get(['date', 'max']).setErrors({ rangeError: true });
      }
      else {
        this.customFieldForm.get(['date', 'min']).setErrors(null);
        this.customFieldForm.get(['date', 'min']).setErrors(null);
      }
    });
  }

  get questions(): any { return this.form as FormArray; }

  /// sets current editor flag
  setCurrentEditor(event): void {
    this.currentEditor = event;
    this.previewService.setCurrentSection(event);
  }

  loadCustomFields(): Observable<any> {
    this.guestProfileFields = this.defaultProfileFields;
    return this.customFieldDataService.getAllCustomFields()
      .pipe(
        map(x => {
          // this.CustFieldValidator = x;
          x.forEach(field => this.guestProfileFields.push(field.name));
        })
      );
  }

  addQuestion(questI, type): void {
    const question = JSON.parse(JSON.stringify(questionObj[type]));
    question.id = uuid.v4();
    if (type === 'select' || type === 'selectSearch') {
      question.choices = this.fb.array([]);
    }
    this.form.insert(questI + 1, this.fb.group(question));
  }
  removeQuestion(questI): void {
    if (this.form.controls[questI].value.mappingField !== '') {
      this.allProfileFields[this.form.controls[questI].value.type === 'select' ? 'singleLineText' : this.form.controls[questI].value.type].find(x => x.name.toLowerCase() === this.form.controls[questI].value.mappingField.toLowerCase()).isSelected = false;
    }
    this.form.removeAt(questI);

    // this.checkDuplicates('headline');
    // this.checkDuplicates('headlineArabic');
  }
  duplicateQuestion(questI): void {
    const question = cloneDeep(this.questions.controls[questI]);
    question.controls.id.setValue(uuid.v4());
    if (question.controls.choices.controls.length > 0) {
      if (this.lookups.indexOf(question.value.mappingField.toLowerCase()) > -1) {
        while (question.controls.choices.value.length > 0) {
          question.controls.choices.removeAt(0);
        }
      }
    }

    question.controls.mappingField.setValue('');

    this.form.insert(questI + 1, question);
  }
  setCurrent(current, index): void {
    this.currentQuestion = current;
    this.previewService.setCurrentQuestion(index);
  }
  setCurrentAsSubmit(): void {
    this.previewService.setCurrentSection('submit');
  }

  checkDuplicates(propertyName): void {
    // this.questionChanges.emit(propertyName);
  }
  addOption(questI, optI): void {
    const choices = this.questions.controls[questI].controls.choices as FormArray;
    choices.insert(optI + 1, this.fb.group({
      label: '',
      labelArabic: '',
      value: '',
      selected: false
    }));
    this.updateOptionIndices(questI);
  }
  // To update indices of options on add or delete
  updateOptionIndices(questI): void {
    this.questions.controls[questI].controls.choices.controls.forEach((choice, i) => {
      choice.controls.value.setValue(i + 1);
    });
  }
  removeOption(questI, optI): void {
    this.questions.controls[questI].controls.choices.removeAt(optI);
    this.updateOptionIndices(questI);
  }

  addNewCustomField(header, i, type): void {
    this.customs.push(i);
    const dialogConfig: any = sideDialogConfig;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.Width = '30vw';
    dialogConfig.data = { type: type };

    const fieldBefore = this.form.controls[i].value.mappingField;

    // const dialogRef = this.animDialog.open(AddCustomFieldModalComponent, dialogConfig);
    // dialogRef.afterClosed().subscribe(result => {
    //   this.customFieldDataService.getAllCustomFields()
    //     .subscribe(result => {
    //       const lastItemIndex = result.length - 1;
    //       result[lastItemIndex].isSelected = true;
    //       if (result[lastItemIndex].customFieldType === customFieldType.text) {
    //         this.allProfileFields.singleLineText.push(result[lastItemIndex]);
    //         // this.allProfileFields.singleLineText[result.name].isNumber = false;
    //       } else if (result[lastItemIndex].customFieldType === customFieldType.number) {
    //         this.allProfileFields.singleLineText.push(result[lastItemIndex]);
    //         // this.allProfileFields.singleLineText[result.name].isNumber = true;
    //       } else if (result[lastItemIndex].customFieldType === customFieldType.date) {
    //         this.allProfileFields.date.push(result[lastItemIndex]);
    //       } else if (result[lastItemIndex].customFieldType === customFieldType.email) {
    //         this.allProfileFields.email.push(result[lastItemIndex]);
    //       } else if (result[lastItemIndex].customFieldType === customFieldType.phone) {
    //         this.allProfileFields.phone.push(result[lastItemIndex]);
    //       }
    //       this.questions.controls[i].controls.mappingField.setValue(result[lastItemIndex].name);

    //     });

    // });
  }
  setValidator(event, i, oldValue, type): void {

    if (event === '' || event == null) {
      return;
    }
    let custom = null;
    this.CustFieldValidator.forEach(element => {

      if (element.name === event) {
        custom = element;
        return;
      }
    });
    if (custom != null || custom) {
      if (this.questions.controls[i].controls.hasOwnProperty('minLength')) {
        this.questions.controls[i].controls['minLength'].setValue(custom.minValue);
        if (this.questions.controls[i].controls.hasOwnProperty('maxLength')) {
          this.questions.controls[i].controls['maxLength'].setValue(custom.hasOwnProperty('maxValue') ? custom.maxValue : 50);
        }
        if (custom.customFieldType === customFieldType.text) {
          this.questions.controls[i].controls['pattern'].setValue('');
        } else if (custom.customFieldType === customFieldType.number) {
          this.questions.controls[i].controls['pattern'].setValue(`^[0-9]*$`);
        }
        // this.form.get(`${i}.minLength`).setValue(10);
      } else if (this.questions.controls[i].controls.hasOwnProperty('minDate')) {
        custom.minValue != null ? this.questions.controls[i].controls['minDate'].setValue(new Date(custom.minValue).toISOString()) : this.questions.controls[i].controls['minDate'].setValue(null);
        custom.maxValue != null ? this.questions.controls[i].controls['maxDate'].setValue(new Date(custom.maxValue).toISOString()) : this.questions.controls[i].controls['maxDate'].setValue(null);
      }
      // if (custom.customFieldType === 1) {
      //   this.questions.controls[i].controls['isNumber'].setValue(true);
      // } else {
      //   this.questions.controls[i].controls['isNumber'].setValue(false);
      // }
    } else {
      if (this.questions.controls[i].controls.hasOwnProperty('minLength')) {
        let field = null;
        this.allProfileFields.singleLineText.forEach(element => {
          if (element.name.toLowerCase() === event.toLowerCase()) {
            field = element;
            return;
          }
        });
        if (field != null) {
          this.questions.controls[i].controls['maxLength'].setValue(field.maxLength);
          this.questions.controls[i].controls['minLength'].setValue(field.minLength);
        }
      } else if (this.questions.controls[i].controls.hasOwnProperty('minDate')) {
        let field = null;
        this.allProfileFields.date.forEach(element => {
          if (element.name === event) {
            field = element;
            return;
          }
        });
        if (field != null) {
          this.questions.controls[i].controls['maxDate'].setValue(new Date(field.maxLength).toISOString());
          this.questions.controls[i].controls['minDate'].setValue(new Date(field.minLength).toISOString());
        }
      }

    }
    if (this.questions.controls[i].controls.hasOwnProperty('minLength')) {
      this.questions.controls[i].controls['minLength'].disable();
      if (this.questions.controls[i].controls.hasOwnProperty('maxLength')) { this.questions.controls[i].controls['maxLength'].disable(); }

    } else if (this.questions.controls[i].controls.hasOwnProperty('minDate')) {
      this.questions.controls[i].controls['minDate'].disable();
      this.questions.controls[i].controls['maxDate'].disable();
    }




    if (oldValue) { this.allProfileFields[type === 'select' ? 'singleLineText' : type].find(x => x.name === oldValue).isSelected = false; }
    if (this.lookups.indexOf(oldValue.toLowerCase()) > -1) {
      while (this.questions.controls[i].controls.choices.value.length > 0) {
        this.questions.controls[i].controls.choices.removeAt(0);
      }
    }
    this.allProfileFields[type === 'select' ? 'singleLineText' : type].find(x => x.name === event).isSelected = true;
    if (event === 'expirationDate') {
      this.questions.controls[i].controls['maxDate'].setValue(null);
      this.questions.controls[i].controls['minDate'].setValue(null);
      this.questions.controls[i].controls['minDate'].enable();
      this.questions.controls[i].controls['maxDate'].enable();
    }

    this.questions.controls[i].controls['mappingField'].setValue(event);
    // console.log(this.questions,"THIS.QUESTIONS")

    this.form.updateValueAndValidity();
  }
  confirm(mapping, i, val): void {
    // console.log(mapping,"-",i,"-",val)
    // return
    this.loader = true;
    const value = this.customFieldForm.value;
    // console.log(value)
    let min = null;
    let max = null;
    if (value.type === 'text') {
      min = value.text.min;
      max = value.text.max;
    }
    if (value.type === 'number') {
      min = value.number.min;
      max = value.number.max;
    }

    if (value.type === 'date') {
      if (value.date.min) {
        const minDate = new Date(value.date.min);
        min = minDate.getTime();
      }
      if (value.date.max) {
        const maxDate = new Date(value.date.max);
        max = maxDate.getTime();
      }
    }

    const newField = {
      name: value.name,
      customFieldType: value.type,
      minValue: min,
      maxValue: max
    };

    this.customFieldDataService.createCustomField(newField).subscribe(result => {
      if (result) {
        this.loader = false;

      }
    }, error => {
      this.loader = false;
    });


  }
}
