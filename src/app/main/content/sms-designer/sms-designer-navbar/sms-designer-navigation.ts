import { FuseNavigation } from '@fuse/types';

export const SmsDesignerNavigation: FuseNavigation[] = [
    {
        id: 'form',
        title: 'Form',        
        translate: '',
        type: 'item',
        icon: 'ballot',
        url: `rsvp-form`
    },
    {
        id: 'design',
        title: 'Design',        
        translate: '',
        type: 'item',
        icon: 'edit',
        url: `design`
    },
    {
        id: 'preview',
        title: 'Preview',        
        translate: '',
        type: 'item',
        icon: 'visibility',
        url: `preview`
    }
];

export const SmsDesignerNavigationWithoutRsvp: FuseNavigation[] = [
    {
        id: 'design',
        title: 'Design',        
        translate: '',
        type: 'item',
        icon: 'edit',
        url: `design`
    },
    {
        id: 'preview',
        title: 'Preview',        
        translate: '',
        type: 'item',
        icon: 'visibility',
        url: `preview`
    }
]