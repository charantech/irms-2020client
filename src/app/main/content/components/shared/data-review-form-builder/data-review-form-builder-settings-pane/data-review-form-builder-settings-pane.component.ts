import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'irms-data-review-form-builder-settings-pane',
  templateUrl: './data-review-form-builder-settings-pane.component.html',
  styleUrls: ['./data-review-form-builder-settings-pane.component.scss']
})
export class DataReviewFormBuilderSettingsPaneComponent implements OnInit {
  @Input() settings: FormGroup;
  constructor() { }

  ngOnInit() {
  }

}
