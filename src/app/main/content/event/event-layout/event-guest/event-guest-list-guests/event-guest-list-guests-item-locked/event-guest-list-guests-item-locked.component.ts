import { Component, OnInit, Input } from '@angular/core';
import { invitationStatus } from 'app/constants/constants';

@Component({
  selector: 'irms-event-guest-list-guests-item-locked',
  templateUrl: './event-guest-list-guests-item-locked.component.html',
  styleUrls: ['./event-guest-list-guests-item-locked.component.scss']
})
export class EventGuestListGuestsItemLockedComponent implements OnInit {
  @Input() data;
  public invitationStatus = invitationStatus;
  constructor() { }

  ngOnInit() {
  }

}
