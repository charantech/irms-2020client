import { Injectable } from '@angular/core';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { EventModel } from './event.model';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { IEventList, IResponseModel } from 'types';
import { Observable } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class EventDataService implements SectionDataService<EventModel> {

  private url = `${BASE_URL}api/event`;
  constructor(private http: HttpClient) { }

  @Loader()
  getList(filterParam: IEventList): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/list`, filterParam);
  }

  get(id): Observable<EventModel> {
    return this.http.get<EventModel>(`${this.url}/${id}/info`);
  }


  @Toast('Successfully created')
  create(model: any): Observable<string> {
    const header = new HttpHeaders();
    return this.http.post<string>(`${this.url}`, model, { headers: header });
  }


  @Toast('Successfully updated')
  update(model) {
    return this.http.put(`${this.url}`, model);
  }

  @Toast('Successfully updated')
  updateEventFeatures(model) {
    return this.http.put(`${this.url}/event-features`, model);
  }

  @Toast('Successfully deleted')
  delete(model) {
    return this.http.request('delete', `${this.url}`, { body: model });
  }

  //get product features
  getFeatures(): Observable<any> {
    return this.http.get<any>(`${this.url}/features`);
  }

  //get product features
  getEventFeatures(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}/event-features`);
  }
}
