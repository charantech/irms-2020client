import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from 'app/core/animations';
import { EventCampaignEditService } from './event-campaign-edit.service';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CampaignConfigurationsService } from './campaign-configurations/campaign-configurations.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'irms-event-campaign-edit',
  templateUrl: './event-campaign-edit.component.html',
  styleUrls: ['./event-campaign-edit.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignEditComponent implements OnInit, OnDestroy {

  public tabGroup = [
    {
      label: 'Campaign Configurations',
      isOptional: false,
      link: './config'
    },
    {
      label: 'Invitation (RSVP)',
      isOptional: false,
      link: './invitation'
    },
    {
      label: 'Pending',
      isOptional: true,
      link: './invitation-pending'
    },
    {
      label: 'Accepted',
      isOptional: true,
      link: './invitation-accepted'
    },
    {
      label: 'Rejected',
      isOptional: true,
      link: './invitation-rejected'
    },
    {
      label: 'Done',
      isOptional: false,
      link: './done'
    }

  ];
  public campaignName: string;
  subscription: Subscription;
  saveLoading = false;
  campaignId;
  private unsubscribeAll: Subject<any>;

  constructor(private service: EventCampaignEditService, public configStepService: CampaignConfigurationsService, protected route: ActivatedRoute) {
    this.unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    // campaign id param
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
    });
    // set page title as campaign name
    this.subscription = this.service.getCampaignName().pipe(takeUntil(this.unsubscribeAll)).subscribe(x => {
      this.campaignName = x;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  save(): void {
    this.service.saveCampaign(true);
  }

}
