import { Component, EventEmitter, Inject, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MediaType } from 'app/constants/constants';
import { fuseAnimations } from 'app/core/animations';
import { EmailDesignerService } from 'app/main/content/email-designer/email-designer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'irms-email-template-card',
  templateUrl: './email-template-card.component.html',
  styleUrls: ['./email-template-card.component.scss'],
  animations: fuseAnimations
})
export class EmailTemplateCardComponent implements OnInit, OnDestroy {
  @Input() editEmail = false;
  @Input() emailTemplate;
  @Input() showRsvp;
  @Input() disabled;
  @Input() isDialog;
  @Input() step;
  @Input() isReview = false;
  @Output() editEmailEvent: EventEmitter<any> = new EventEmitter();
  @Output() emailPreview: EventEmitter<any> = new EventEmitter();
  @Output() landingEmail: EventEmitter<any> = new EventEmitter();
  
  subscriptions: Subscription[] = [];
  mediaType = MediaType;
  private dialogRef = null;
  private dialogData;
  constructor(
    private dialog: MatDialog,
    private injector: Injector,
    private emailDesignerService: EmailDesignerService) {
      this.dialogRef = this.injector.get(MatDialogRef, null);
      this.dialogData = this.injector.get(MAT_DIALOG_DATA, null);
  }
  ngOnInit(): void {
    this.subscriptions.push(this.emailDesignerService.triggerPreviewObs().subscribe(data => {
      this.emailPreview.emit();
      this.closeDialogView();
    }));
    this.subscriptions.push(this.emailDesignerService.triggerLandingPageObs().subscribe(data => {
      this.landingEmail.emit();
      this.closeDialogView();
    }));
    
    if (this.dialogData) {
      this.isDialog = this.dialogData.isDialog;
    }
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  expandForEdit(): void {
    this.editEmailEvent.emit();
  }

  openDialogView(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.minWidth = '70vw';
    dialogConfig.data = {
      isDialog: true
    };
    const dialogRef = this.dialog.open(EmailTemplateCardComponent, dialogConfig);
    dialogRef.componentInstance.editEmail = this.editEmail;
    dialogRef.componentInstance.emailTemplate = this.emailTemplate;
    dialogRef.componentInstance.showRsvp = this.showRsvp;
    dialogRef.componentInstance.disabled = this.disabled;
    dialogRef.componentInstance.step = this.step;
    dialogRef.componentInstance.isDialog = true;
    dialogRef.afterOpened().subscribe(() => {
      this.emailDesignerService.setCurrentComponentState('dialog');
    });
    dialogRef.afterClosed().subscribe( ex => {
      this.emailDesignerService.triggerRefresh();
      this.emailDesignerService.setCurrentComponentState('component');
    });

  }

  closeDialogView(): void {
    if (this.dialogRef) {
      this.dialogRef.close(false);
    }
  }

  emailSave(): void {
    this.emailDesignerService.triggerSave();
  }
  emailPreviewMethod(): void {
    this.emailDesignerService.triggerPreview();
  }

}
