import { Injectable } from '@angular/core';
import { SectionService } from '../section/section.service';
import { BaseModel } from '../section/base.model';
import { DataReviewFormBuilderDataService } from './data-review-form-builder-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataReviewFormBuilderService extends SectionService<BaseModel> {
  
  public takenFields = [];
  private selectedFields = new Subject<any>();
  public isFormLoaded = new Subject<boolean>();
  public selectedFieldArr = [];
  private formContent = new Subject<any>();
  importantFields: any;
  
  constructor(
    protected dataService: DataReviewFormBuilderDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }

  setIsFormLoaded(val: boolean): void{
    this.isFormLoaded.next(val);
  }

  getIsFormLoaded(): Observable<boolean>{
    return this.isFormLoaded.asObservable();
  }

  setSelectedFields(data: any): void {
    this.selectedFields.next(data);
    this.selectedFieldArr = data;
  }

  getSelectedFields(): Observable<any> {
    return this.selectedFields.asObservable();
  }

  setFormContent(data: any): void {
    this.formContent.next(data);
  }

  getFormContent(): Observable<any> {
    return this.formContent.asObservable();
  }
  
}

