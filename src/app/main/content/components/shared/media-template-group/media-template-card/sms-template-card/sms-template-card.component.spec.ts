import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsTemplateCardComponent } from './sms-template-card.component';

describe('SmsTemplateCardComponent', () => {
  let component: SmsTemplateCardComponent;
  let fixture: ComponentFixture<SmsTemplateCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsTemplateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsTemplateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
