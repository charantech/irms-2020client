import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'irms-sms-preview',
  templateUrl: './sms-preview.component.html',
  styleUrls: ['./sms-preview.component.scss']
})
export class SmsPreviewComponent implements OnInit {

  sendTest = false;
  id: any;

  constructor(
    public dialogRef: MatDialogRef<SmsPreviewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

  ngOnInit() {
    this.id = this.data.id;
  }


  close(){
    this.dialogRef.close();
  }

}
