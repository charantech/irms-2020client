import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-global-guest-list-guests-item',
  templateUrl: './global-guest-list-guests-item.component.html',
  styleUrls: ['./global-guest-list-guests-item.component.scss']
})
export class GlobalGuestListGuestsItemComponent implements OnInit {
@Input() data;
  constructor() { }

  ngOnInit() {
  }

}
