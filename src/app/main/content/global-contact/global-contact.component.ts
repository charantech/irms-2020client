import { Component, OnInit } from '@angular/core';
import { SectionComponent } from '../components/shared/section/section.component';
import { GlobalContactModel } from './global-contact.model';
import { Section } from 'app/constants/constants';
import { GlobalContactService } from './global-contact.service';
import { GlobalContactDataService } from './global-contact-data.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'irms-global-contact',
  templateUrl: './global-contact.component.html',
  styleUrls: ['./global-contact.component.scss']
})
export class GlobalContactComponent extends SectionComponent<GlobalContactModel> implements OnInit {
  
  constructor(public sectionService: GlobalContactService,
              protected dataService: GlobalContactDataService,
              protected router: Router, public dialog: MatDialog) {
super(Section.GlobalContact, sectionService, dataService, router);
}

  ngOnInit() {
  }


}
