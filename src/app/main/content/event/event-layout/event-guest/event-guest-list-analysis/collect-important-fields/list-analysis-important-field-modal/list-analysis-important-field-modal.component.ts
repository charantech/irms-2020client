import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'irms-list-analysis-important-field-modal',
  templateUrl: './list-analysis-important-field-modal.component.html',
  styleUrls: ['./list-analysis-important-field-modal.component.scss']
})
export class ListAnalysisImportantFieldModalComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    dialogRef: MatDialogRef<ListAnalysisImportantFieldModalComponent>
  ) { }

  ngOnInit() {
  }

}
