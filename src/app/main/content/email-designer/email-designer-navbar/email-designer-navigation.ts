import { FuseNavigation } from '@fuse/types';

export const EmailDesignerNavigation: FuseNavigation[] = [
    {
        id: 'design',
        title: 'Design',        
        translate: '',
        type: 'item',
        icon: 'edit',
        url: `design`
    },
    {
        id: 'preview',
        title: 'Preview',        
        translate: '',
        type: 'item',
        icon: 'visibility',
        url: `preview`
    },
    {
        id: 'response',
        title: 'Responses Design',        
        translate: '',
        type: 'item',
        icon: 'reply',
        url: `response`
    }
];

export const EmailDesignerNavigationWithoutRsvp: FuseNavigation[] = [
    {
        id: 'design',
        title: 'Design',        
        translate: '',
        type: 'item',
        icon: 'edit',
        url: `design`
    },
    {
        id: 'preview',
        title: 'Preview',        
        translate: '',
        type: 'item',
        icon: 'visibility',
        url: `preview`
    }
]