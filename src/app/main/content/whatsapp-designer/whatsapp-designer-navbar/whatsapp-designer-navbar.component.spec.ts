import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappDesignerNavbarComponent } from './whatsapp-designer-navbar.component';

describe('WhatsappDesignerNavbarComponent', () => {
  let component: WhatsappDesignerNavbarComponent;
  let fixture: ComponentFixture<WhatsappDesignerNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappDesignerNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappDesignerNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
