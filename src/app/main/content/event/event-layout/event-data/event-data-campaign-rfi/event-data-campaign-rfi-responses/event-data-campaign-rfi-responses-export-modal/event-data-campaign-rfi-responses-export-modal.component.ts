import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormArray, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EventDataDataService } from '../../../event-data.data.service';

@Component({
  selector: 'irms-event-data-campaign-rfi-responses-export-modal',
  templateUrl: './event-data-campaign-rfi-responses-export-modal.component.html',
  styleUrls: ['./event-data-campaign-rfi-responses-export-modal.component.scss']
})
export class EventDataCampaignRfiResponsesExportModalComponent implements OnInit {
  public qList = [];
  importFieldsForm = this.fb.group({
    guestName: [true],
    submissionDate: [true],
    questions: new FormArray([])
  });
  id: string;

  constructor(
    private dialogRef: MatDialogRef<EventDataCampaignRfiResponsesExportModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected router: Router,
    protected route: ActivatedRoute,
    protected dataService: EventDataDataService,
    public fb: FormBuilder) { }

  ngOnInit() {
    this.id = this.data.id;
    this.dataService.getFormFields(this.id).subscribe(questions => {
      this.qList = questions
      this.addFields(this.qList);
    });
  }

  addFields(data) {
    data.forEach(element => {
      this.t.controls.push(new FormControl(false));
    });
  }

  close(): void {
    this.dialogRef.close();
  }
  confirm(): void{
    let result = {
      hasGuestNameSelected: false,
      hasSubmissionDateSelected: false,
      questions : []
    }

    if (this.importFieldsForm.controls.guestName.value) {
      result.hasGuestNameSelected = true;
    }
    if (this.importFieldsForm.controls.submissionDate.value) {
      result.hasSubmissionDateSelected = true;
    }

    this.t.controls.forEach((element, i) => {
      if (element.value) {
        result.questions.push(this.qList[i].id);
      }
    });

    this.dialogRef.close(result)
  }

  onAllChange(event): void{
    this.importFieldsForm.controls.guestName.setValue(event);
    this.importFieldsForm.controls.submissionDate.setValue(event);
    this.t.controls.forEach(element => {
      element.setValue(event);
    });
  }
  
  // convenience getters for easy access to form fields
  get f(): any { return this.importFieldsForm.controls; }
  get t(): any { return this.f.questions as FormArray; }
}
