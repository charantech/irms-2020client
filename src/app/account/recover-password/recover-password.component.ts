import {FormBuilder, Validators} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {fuseAnimations} from '@fuse/animations';
import {FuseConfigService} from '../../core/services/config.service';
import {AccountService} from '../account.service';
import {RegexpPattern} from '../../constants/constants';
import { AppStateService } from '../../main/content/services/app-state.service';
import { VerifyOtpComponent } from '../veirfy-otp/veiry-otp.component';

@Component({
  selector: 'mvms-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss'],
  animations : fuseAnimations
})
export class RecoverPasswordComponent implements OnInit {
  logoLink: any;
  loading = false;
  logoLoad = true;
  public currentState = 'initial';
  private intervalId = 0;
  public timer = 60;
  private seconds = 60;
  private currentPhoneNumber: string;
  private currentUserId: any;
  public resend = false;
  public captchaKey: string;
  public resetByTypes = [
    {
    method: 'SMS',
    icon: 'sms'
    },
    {
      method: 'Email',
      icon: 'email'
    }
  ];

  public blockTitle = 'Forgot your password';

  private mailChecked: boolean;

  public nextStateForm = this.fb.group({
    currentRecoveryType: ['', [Validators.required]]
  });

  public recoverPassByEmailForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern(new RegExp(RegexpPattern.email))]],
    recaptchaResponse: [null, [Validators.required]]
  });

  public recoverPassBySmsForm = this.fb.group({
    phoneNumber: ['', [Validators.required, Validators.minLength(8), Validators.pattern(new RegExp(RegexpPattern.phone))]],
    recaptchaResponse: [null, [Validators.required]]
  });

  public codeForm = this.fb.group({
    codeNumber: ['', [Validators.required,  Validators.minLength(6), Validators.maxLength(6),  Validators.pattern(new RegExp(RegexpPattern.digits))]],
  });
  email: any;


  constructor(public fb: FormBuilder,
              private accountService: AccountService,
              private router: Router,
              private  appState: AppStateService) {
  }

  ngOnInit() {
    this.captchaKey = window.location.host.startsWith('localhost') || window.location.host.indexOf('mvmsx.net') >= 0 || window.location.host.indexOf('default.takamul.com') >= 0
      ? '6LdqgGgUAAAAAHONCepha6XOc20zYC_XQ-3RPkCs' : '6LfxB7sUAAAAAKOEczAOX45UWy95RD7bQFkKbtTM';
      
    this.logoLoad = true;
    this.appState.getTenantLogo().subscribe(result => {
        this.logoLink = result.logoPath;
        this.logoLoad = false;
      }, error => {
        this.logoLoad = false;
        this.logoLink = null;
      });
  }

  public nextState() {
    if (this.nextStateForm.dirty && this.nextStateForm.valid) {
      if (this.nextStateForm.value.currentRecoveryType === this.resetByTypes[0]) {
        this.currentState = 'bySms';
      } else {
        this.currentState = 'byEmail';
      }
    }
  }

  public recoverPassByEmail() {
    if (this.recoverPassByEmailForm.dirty && this.recoverPassByEmailForm.valid && this.mailChecked) {
      this.accountService.recoverPasswordByEmail(this.recoverPassByEmailForm.value).subscribe(data => {
        this.currentState = 'info';
        this.blockTitle = 'Please check your email';
        this.email = this.recoverPassByEmailForm.controls.email.value;
      });
    }
  }

  public recoverPassBySms() {
    if (this.recoverPassBySmsForm.dirty && this.recoverPassBySmsForm.valid) {
      const phoneNumber =  this.recoverPassBySmsForm.value.phoneNumber;
      this.accountService.recoverPasswordBySms(this.recoverPassBySmsForm.value).subscribe(data => {
      this.currentPhoneNumber = phoneNumber;
      this.currentUserId  = data;
      this.router.navigateByUrl(`/account/verify-otp`);
      this.currentState = 'reset';
      this.countDown();
      });
    }
  }

  public resendCodeBySms() {
    if (this.currentPhoneNumber) {
      const model: any = {
        phoneNumber: this.currentPhoneNumber
      };
      this.accountService.resendCodeBySms(model).subscribe(data => {
      this.resend = false;
      this.timer = 60;
      this.countDown();
      });
    }
  }
  public submitCode() {
    if (this.codeForm.dirty && this.codeForm.valid) {
      const model = {
        phoneNumber: this.currentPhoneNumber,
        code: this.codeForm.value.codeNumber
      };
      this.accountService.getTokenByCode(model).subscribe((resp: any) => {
        if (resp && resp.token && resp.userId) {
          this.router.navigateByUrl(`/account/change-password?userId=${resp.userId}&token=${resp.token}`);
        }
      });
    }
  }

  private clearTimer() {
    window.clearInterval(this.intervalId);
    this.seconds = 60;
  }

  private countDown() {
    this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.seconds -= 1;
      this.timer = this.seconds;
      if (this.seconds === 0) {
        this.timer = 0;
        this.clearTimer();
        this.resend = true;
      }
    }, 1000);
  }

  checkEmail(event) {
    if (this.recoverPassByEmailForm.controls.email.dirty && this.recoverPassByEmailForm.controls.email.valid) {
      this.appState.checkEmail({email: this.recoverPassByEmailForm.controls.email.value}).subscribe((res: any) => {
        if (res.isUnique) {
          this.recoverPassByEmailForm.controls.email.setErrors({notRegistered: true});
        } else if (res.isUnavailable) {
          this.recoverPassByEmailForm.controls.email.setErrors({notRegistered: true});
        } else {
          this.mailChecked = true;
          let target = event.relatedTarget;
          if (target === null) {
            target = document.activeElement;
          }
          if (target && target.nodeName === 'BUTTON') {
            this.recoverPassByEmail();
          }
        }
      });
    }
  }

  checkPhone() {
    if (this.recoverPassBySmsForm.controls.phoneNumber.valid && this.recoverPassBySmsForm.controls.phoneNumber.dirty) {
      this.appState.checkPhone({phoneNo: this.recoverPassBySmsForm.controls.phoneNumber.value}).subscribe((res: any) => {
        if (!res.isValid) {
          this.recoverPassBySmsForm.controls.phoneNumber.setErrors({notValid: true});
        } else if (res.isUnique) {
          this.recoverPassBySmsForm.controls.phoneNumber.setErrors({notUnique: true});
        } else if (res.isUnavailable) {
          this.recoverPassBySmsForm.controls.phoneNumber.setErrors({notUnique: true});
        } else {
          this.recoverPassBySmsForm.patchValue({phoneNumber: res.formattedPhoneNo});
          this.recoverPassBySmsForm.updateValueAndValidity();
        }
      });
    }
  }

}
