import { Component, OnInit, Input } from '@angular/core';
import { campaignStatus } from 'app/constants/constants';

@Component({
  selector: 'irms-event-data-campaign-list-item',
  templateUrl: './event-data-campaign-list-item.component.html',
  styleUrls: ['./event-data-campaign-list-item.component.scss']
})  
export class EventDataCampaignListItemComponent implements OnInit {
  @Input() data;
  status = campaignStatus;
  constructor() { }

  ngOnInit() {
  }
}
