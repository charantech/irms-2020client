import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormDesignerNavBarComponent } from './data-review-form-designer-nav-bar.component';

describe('DataReviewFormDesignerNavBarComponent', () => {
  let component: DataReviewFormDesignerNavBarComponent;
  let fixture: ComponentFixture<DataReviewFormDesignerNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormDesignerNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormDesignerNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
