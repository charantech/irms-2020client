import { Component, Input, OnInit } from '@angular/core';
import { campaignStatus } from 'app/constants/constants';

@Component({
  selector: 'irms-event-data-list-analysis-list-item',
  templateUrl: './event-data-list-analysis-list-item.component.html',
  styleUrls: ['./event-data-list-analysis-list-item.component.scss']
})
export class EventDataListAnalysisListItemComponent implements OnInit {
  data;
  status = campaignStatus;
  constructor() { }

  ngOnInit() {
  }

}
