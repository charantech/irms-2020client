import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventCampaignViewService {
  private campaignName = new BehaviorSubject<any>('');

  setCampaignName(name: string): void {
    this.campaignName.next(name);
  }

  getCampaignName(): Observable<any> {
    return this.campaignName.asObservable();
  }

  getInstantCampaignName(): string {
    return this.campaignName.value;
  }

  constructor() { }

}
