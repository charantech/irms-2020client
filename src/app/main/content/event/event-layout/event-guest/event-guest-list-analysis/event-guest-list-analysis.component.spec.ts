import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestListAnalysisComponent } from './event-guest-list-analysis.component';

describe('EventGuestListAnalysisComponent', () => {
  let component: EventGuestListAnalysisComponent;
  let fixture: ComponentFixture<EventGuestListAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestListAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestListAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
