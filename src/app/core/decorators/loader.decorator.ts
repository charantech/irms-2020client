import { appInjector } from '../../main/app-injector';
import { Observable } from 'rxjs/Observable';
import { GenericLoaderService } from '../../main/content/components/shared/generic-loader/generic-loader.service';
import { tap } from 'rxjs/operators';
import { MainModule } from 'app/main/main.module';

export function Loader() {
    return function(target: any, key: string, descriptor: any) {
        const originalMethod = descriptor.value;
        descriptor.value = function(...args: any[]) {
            const loaderService = appInjector().get(GenericLoaderService);
            if (!(args[0] && args[0]['searchText'])) { loaderService.show(); }
            const result = originalMethod.apply(this, args);
            return (result as Observable<any>).pipe(tap(() => {
                loaderService.hide();
            }));
        };

        return descriptor;
    };
}
