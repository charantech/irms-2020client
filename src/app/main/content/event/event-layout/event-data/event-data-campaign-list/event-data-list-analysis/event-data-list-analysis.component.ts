import { Component, Input, OnInit } from '@angular/core';
import { DataModuleDataService } from 'app/main/content/event/data-module-data.service';
import { Observable } from 'rxjs';
import { IResponseModel } from 'types';
import { EventDataListAnalysisListItemComponent } from '../event-data-list-analysis-list-item/event-data-list-analysis-list-item.component';

@Component({
  selector: 'irms-event-data-list-analysis',
  templateUrl: './event-data-list-analysis.component.html',
  styleUrls: ['./event-data-list-analysis.component.scss']
})
export class EventDataListAnalysisComponent implements OnInit {
  public data: Observable<IResponseModel>;
  public listItemComponentListAnalysis: Object = EventDataListAnalysisListItemComponent;
  @Input() eventId;
  
  constructor(protected dataService: DataModuleDataService) { }

  ngOnInit() {
    this.data = this.dataService.getListAnalysis(this.eventId);
  }

}
