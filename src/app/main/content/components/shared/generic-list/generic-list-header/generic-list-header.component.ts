import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GenericListService } from '../generic-list.service';

@Component({
  selector: 'irms-generic-list-header',
  templateUrl: './generic-list-header.component.html',
  styleUrls: ['./generic-list-header.component.scss']
})
export class GenericListHeaderComponent implements OnInit {
  @Input() selectable: boolean;
  @Input() actionMenu: boolean;
  @Input() listHeaders;
  @Input() NumOfMaxSelectables: number;
  @Input() checked: boolean;
  @Input() indeterminate: boolean;
  @Output() 
  selectAll: EventEmitter<boolean> = new EventEmitter();
  constructor(protected genericListService: GenericListService) { }

  ngOnInit() {
  }
  toggleSelectAll(event): void {
    this.selectAll.emit(event);
  }

}
