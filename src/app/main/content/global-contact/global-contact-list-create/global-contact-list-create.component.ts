import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'irms-global-contact-list-create',
  templateUrl: './global-contact-list-create.component.html',
  styleUrls: ['./global-contact-list-create.component.scss']
})
export class GlobalContactCreateComponent implements OnInit {
  public newContactListForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]]
    });
  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<GlobalContactCreateComponent>) { }

  ngOnInit() {
  }
  create(): void {
    this.dialogRef.close(this.newContactListForm.value);
  }

  close(): void {
    this.dialogRef.close();
  }

}
