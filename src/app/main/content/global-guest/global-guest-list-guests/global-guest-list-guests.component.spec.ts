import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestListGuestsComponent } from './global-guest-list-guests.component';

describe('GlobalGuestListGuestsComponent', () => {
  let component: GlobalGuestListGuestsComponent;
  let fixture: ComponentFixture<GlobalGuestListGuestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestListGuestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestListGuestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
