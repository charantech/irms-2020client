import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsComponent } from './global-guest-details.component';

describe('GlobalGuestDetailsComponent', () => {
  let component: GlobalGuestDetailsComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
