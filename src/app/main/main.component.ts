import {
    Component,
    ElementRef,
    HostBinding,
    Inject,
    OnDestroy,
    OnInit,
    Renderer2,
    ViewEncapsulation
} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';
import { Platform } from '@angular/cdk/platform';
import { DOCUMENT } from '@angular/common';
import { fuseAnimations } from '@fuse/animations';
import { CurrentUserService } from './content/services/current-user.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseNavigation } from '@fuse/types';
import { Router } from '@angular/router';
import { TokenService } from '../auth/services/token.service';
import { RoleType } from '../constants/constants';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { Subject } from 'rxjs';

@Component({
    selector: 'irms-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit, OnDestroy {
    onSettingsChanged: Subscription;
    fuseConfig: any;
    navigation: FuseNavigation;

    // Private
    private _unsubscribeAll: Subject<any>;
    @HostBinding('attr.fuse-layout-mode') layoutMode;

    constructor(
        private router: Router,
        private _renderer: Renderer2,
        private _elementRef: ElementRef,
        private platform: Platform,
        @Inject(DOCUMENT) private document: any,
        private tokenService: TokenService,
        private currentUserService: CurrentUserService,

        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService
    ) {
        // Set the navigation model
        let tokenRole: RoleType;
        const token = this.tokenService.getToken();
        if (token && token.token) {
            const decodeToken = token.decodeToken();
            tokenRole = RoleType[<string>decodeToken.role];
        } else {
            return;
        }
    }

    ngOnInit() { }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        if (this._unsubscribeAll) {
            this._unsubscribeAll.next();
            this._unsubscribeAll.complete();
        }
    }

    addClass(className: string) {
        this._renderer.addClass(this._elementRef.nativeElement, className);
    }

    removeClass(className: string) {
        this._renderer.removeClass(this._elementRef.nativeElement, className);
    }
}
