import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Location} from '@angular/common';
import { FormDesignerService } from '../form-designer.service';
@Component({
  selector: 'irms-form-designer-navbar',
  templateUrl: './form-designer-navbar.component.html',
  styleUrls: ['./form-designer-navbar.component.scss']
})
export class FormDesignerNavbarComponent implements OnInit {

  @Output() saveForm: EventEmitter<any> = new EventEmitter();
  constructor(protected location: Location, protected designerService: FormDesignerService ) { }

  ngOnInit(): void {
  }

  onClickSave(): void{
    this.designerService.triggerSave(true);
  }

  // Go back
  back(): void{
    this.location.back();
  }

}
