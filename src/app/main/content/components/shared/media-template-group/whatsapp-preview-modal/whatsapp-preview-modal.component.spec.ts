import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappPreviewModalComponent } from './whatsapp-preview-modal.component';

describe('WhatsappPreviewModalComponent', () => {
  let component: WhatsappPreviewModalComponent;
  let fixture: ComponentFixture<WhatsappPreviewModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappPreviewModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappPreviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
