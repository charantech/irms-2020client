import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-contacts-list-item',
  templateUrl: './contacts-list-item.component.html'
})
export class ContactsListItemComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
