import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'irms-sms-template-expanded-view',
  templateUrl: './sms-template-expanded-view.component.html',
  styleUrls: ['./sms-template-expanded-view.component.scss']
})
export class SmsTemplateExpandedViewComponent implements OnInit {
  
  @Input() invitationId: string;
  @Input() isDialog;
  @Input() step;

  constructor() { }

  ngOnInit(): void {
  }

}
