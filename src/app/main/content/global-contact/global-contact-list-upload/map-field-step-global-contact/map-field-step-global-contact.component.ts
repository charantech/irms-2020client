import { Component, OnInit, Input } from '@angular/core';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { GlobalContactModel } from '../../global-contact.model';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { GlobalContactService } from '../../global-contact.service';
import { GlobalContactDataService } from '../../global-contact-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Section } from 'app/constants/constants';
import { GlobalContactListUploadFormService } from '../global-contact-list-upload-form.service';
import { MatStepper, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';
import { List } from 'lodash';
import { AddCustomFieldModalComponent } from 'app/main/content/components/shared/add-custom-field-modal/add-custom-field-modal.component';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as XLSX from 'xlsx';
@Component({
  selector: 'irms-map-field-step-global-contact',
  templateUrl: './map-field-step-global-contact.component.html',
  styleUrls: ['./map-field-step-global-contact.component.scss']
})
export class MapFieldStepGlobalContactComponent extends SectionCreateComponent<GlobalContactModel> implements OnInit {
  @Input() stepperRef: MatStepper;
  excelFileData: any;
  mapFieldStep: FormGroup;
  headers = [];
  previewValues = [];
  parsedData = [];
  unmappedCount = 0;
  defaultProfileFields = ['Title', 'FullName', 'PrefferedName', 'Gender', 'Email', 'AlternativeEmail', 'MobileNumber', 'WorkNumber', 'SecondaryMobileNumber', 'Nationality', 'IssuingCountry', 'ExpirationDate', 'DocumentType', 'DocumentNumber', 'Organization', 'Position'];
  guestProfileFields = [];
  availableFields = []; // array of objects generated to track taken(mapped) unmapped fields
  fieldsDropdown = []; // dropdown of unmapped (available) fields
  isEmailOrPhone = false;
  constructor(private fb: FormBuilder,
              private dialog: MatDialog,
              private formService: GlobalContactListUploadFormService,
              public sectionService: GlobalContactService,
              protected dataService: GlobalContactDataService,
              protected router: Router,
              public route: ActivatedRoute) {
    super(Section.GlobalContact, sectionService, dataService, router);
    this.mapFieldStep = this.fb.group({
      fields: new FormArray([])
    });
    this.formService.stepReady(this.mapFieldStep, 'mapFields');
  }

  ngOnInit(): void {
    this.initAvailableFields()
      .subscribe(x => {
        const reader = new FileReader();
        reader.readAsBinaryString(this.formService.getExcelFile());
        reader.onload = (e: any) => {
          /* read workbook */
          const bstr: string = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

          /* grab first sheet */
          const wsname: string = wb.SheetNames[0];
          const ws: XLSX.WorkSheet = wb.Sheets[wsname];

          /* save data */
          this.excelFileData = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
          this.processData();
        };
        this.fields.valueChanges.subscribe(val => {
          this.mappedTotalUpdate();
        });
      });
  }

  // convenience getters for easy access to form fields
  get f(): any { return this.mapFieldStep.controls; }
  get fields(): FormArray { return this.f.fields as FormArray; }

  initAvailableFields(): Observable<any> {
    if (this.availableFields.length === 0) {
      this.guestProfileFields = this.defaultProfileFields;
      this.guestProfileFields.forEach(field => {
        this.availableFields.push({
          title: field,
          taken: false
        });
      });
    }

    return this.dataService.getAllCustomFields()
      .pipe(
        map(x => {
          x.forEach(field => {
            // console.log('field', field)

            if (!this.availableFields.some(y => y.title === field.name)) {
              this.availableFields.push({
                title: field.name,
                taken: false
              });
              this.guestProfileFields.push(field.name);
            }
          });
        })
      );
  }
  updateDropdown(): void {
    const taken = [];
    this.fields.controls.forEach((field: FormGroup) => {
      if (field.controls.mapped.value && !field.controls.skip.value) {
        const ind = this.guestProfileFields.indexOf(field.controls.mapped.value);
        if (ind > -1) {
          this.availableFields[ind].taken = true;
          taken.push(ind);
        }
      }
    });
    this.availableFields.forEach((f, index) => {
      if (taken.indexOf(index) === -1) {
        this.availableFields[index].taken = false;
      }
    });
    // Checks if either phone or email is select
    this.isEmailOrPhone = this.availableFields.find(x => x.title === 'Email').taken || this.availableFields.find(x => x.title === 'MobileNumber').taken;
  }
  updateDropdownOnSkip(i): void {
    if (this.fields.controls[i].get('mapped').value) {
      const ind = this.guestProfileFields.indexOf(this.fields.controls[i].get('mapped').value);
      if (ind > -1) { this.availableFields[ind].taken = false; }
    }
    this.fields.controls[i].get('mapped').setValue('');
    this.isEmailOrPhone = this.availableFields.find(x => x.title === 'Email').taken || this.availableFields.find(x => x.title === 'MobileNumber').taken;
  }

  processData(): void {
    // console.log(this.excelFileData);
    this.headers = this.excelFileData[0];
    // fix for important fields asterisk
    const emailCol = this.headers.indexOf('* Email');
    if (emailCol > -1) { this.headers[emailCol] = 'Email'; }
    const mobileCol = this.headers.indexOf('* MobileNumber');
    if (mobileCol > -1) { this.headers[mobileCol] = 'MobileNumber'; }
    const fullnameCol = this.headers.indexOf('* FullName');
    if (fullnameCol > -1) { this.headers[fullnameCol] = 'FullName'; }
    const lines = [];
    for (let i = 1; i < this.excelFileData.length; i++) {
      const tarr = {};
      for (let j = 0; j < this.headers.length; j++) {
        tarr[this.headers[j]] = this.excelFileData[i][j];
      }
      lines.push(tarr);
    }
    this.parsedData = lines;
    console.log(this.headers, this.parsedData);
    this.mappingInit();
    this.getPreviewData();
  }
  mappingInit(): void {
    this.headers.forEach(header => {
      let matchedFieldIndex = this.guestProfileFields.indexOf(header);
      if (matchedFieldIndex > -1 && this.availableFields[matchedFieldIndex].taken) {
        matchedFieldIndex = -1; // set to -1 because cannot map to already mapped field
      } else if (matchedFieldIndex > -1 && !this.availableFields[matchedFieldIndex].taken) {
        this.availableFields[matchedFieldIndex].taken = true;
      }
      this.fields.push(this.fb.group({
        title: [header],
        mapped: [matchedFieldIndex === -1 ? '' : this.guestProfileFields[matchedFieldIndex]],
        skip: [false]
      }));
    });
    this.mappedTotalUpdate();
    // Checks if either phone or email is select
    this.isEmailOrPhone = this.availableFields.find(x => x.title === 'Email').taken || this.availableFields.find(x => x.title === 'MobileNumber').taken;
  }

  mappedTotalUpdate(): void {
    let mappedCount = 0;
    let unmappedCount = 0;
    this.fields.controls.forEach((field: FormGroup) => {
      if (field.controls.mapped.value && !field.controls.skip.value) {
        mappedCount += 1;
      } else if (!field.controls.mapped.value && !field.controls.skip.value) {
        unmappedCount += 1;
      }
    });
    this.unmappedCount = unmappedCount;
  }
  removeAvailableField(index): void {
    // if (index > -1) {
    //   this.availableFields.splice(index, 1);
    // }
  }
  addAvailableField(index): void {
    // if (index > -1) {
    //   this.availableFields.splice(index, 1);
    // }
  }

  getPreviewData(): void {
    for (let i = 0; i < this.headers.length; i++) {
      /// Get list of first three values of given column for preview purpose
      const previewSize = this.parsedData.length >= 3 ? 3 : this.parsedData.length;
      this.previewValues[i] = this.parsedData.slice(0, previewSize).map(item => {
        return item[this.headers[i]];
      });
    }
  }

  skipUnmapped(): void {
    this.fields.controls.forEach((field: FormGroup) => {
      if (!field.controls.mapped.value) {
        field.controls.skip.setValue(true);
      }
    });
  }

  renameWithMapped(): void {
    const finalizedHeaders = [];
    this.fields.controls.forEach((field: FormGroup) => {
      this.parsedData.forEach(entry => {
        if (field.controls.skip.value) {
          delete entry[field.controls.title.value];
        }
        else if (!entry.hasOwnProperty(field.controls.mapped.value)) {
          entry[field.controls.mapped.value] = entry[field.controls.title.value];
          delete entry[field.controls.title.value];
        }
      });
      if (!field.controls.skip.value) {
        finalizedHeaders.push(field.controls.mapped.value);
      }
    });
    const data = new FormArray([]);
    this.parsedData.forEach(entry => {
      data.push(this.fb.group(entry));
    });
    this.formService.setContactsData(data, finalizedHeaders);
  }

  addContact(): void {
    this.renameWithMapped();
    this.stepperRef.next();
  }

  addNewCustomField(header, i): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30vw';
    dialogConfig.data = {};

    const dialogRef = this.dialog.open(AddCustomFieldModalComponent, dialogConfig);

    const fieldBefore = header.value;

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.dataService.createCustomField(result)
            .subscribe(x => {
              this.initAvailableFields()
                .subscribe(() => {
                  const lastItemIndex = this.availableFields.length - 1;
                  const newValue = {
                    title: header.value.title,
                    skip: header.value.skip,
                    mapped: this.availableFields[lastItemIndex].title
                  };
                  this.availableFields[lastItemIndex].taken = true;
                  header.setValue(newValue);
                });

            });
        } else {
          header.setValue(fieldBefore);
        }
      });
  }
}
