import { TestBed } from '@angular/core/testing';

import { DataReviewFormDesignerService } from './data-review-form-designer.service';

describe('DataReviewFormDesignerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataReviewFormDesignerService = TestBed.get(DataReviewFormDesignerService);
    expect(service).toBeTruthy();
  });
});
