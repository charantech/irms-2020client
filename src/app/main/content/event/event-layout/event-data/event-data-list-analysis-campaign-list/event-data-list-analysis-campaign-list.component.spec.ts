import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataListAnalysisCampaignListComponent } from './event-data-list-analysis-campaign-list.component';

describe('EventDataListAnalysisCampaignListComponent', () => {
  let component: EventDataListAnalysisCampaignListComponent;
  let fixture: ComponentFixture<EventDataListAnalysisCampaignListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataListAnalysisCampaignListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataListAnalysisCampaignListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
