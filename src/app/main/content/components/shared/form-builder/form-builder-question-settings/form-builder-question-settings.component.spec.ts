import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderQuestionSettingsComponent } from './form-builder-question-settings.component';

describe('FormBuilderQuestionSettingsComponent', () => {
  let component: FormBuilderQuestionSettingsComponent;
  let fixture: ComponentFixture<FormBuilderQuestionSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBuilderQuestionSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderQuestionSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
