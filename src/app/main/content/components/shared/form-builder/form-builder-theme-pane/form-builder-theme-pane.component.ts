import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'irms-form-builder-theme-pane',
  templateUrl: './form-builder-theme-pane.component.html',
  styleUrls: ['./form-builder-theme-pane.component.scss']
})
export class FormBuilderThemePaneComponent implements OnInit {

  @Input() theme: FormGroup;
  croppieOptions = {
    viewport: { width: 100, height: 100 },
    boundary: { width: 300, height: 300 },
    showZoomer: false,
    enableResize: true,
    enableOrientation: true,
    mouseWheelZoom: 'ctrl'
  };
  constructor() { }

  ngOnInit(): void{
  }

  deleteBackground(): void{
    this.theme.get('background.image').reset();
  }

}
