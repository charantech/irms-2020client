import { Component, OnInit, OnDestroy, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { GenericLoaderService } from './generic-loader.service';
import { LoaderState } from './generic-loader';
import { Observable, Observer } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  // tslint:disable-next-line: component-selector
  selector: 'irms-generic-loader',
  templateUrl: './generic-loader.component.html',
  styleUrls: ['./generic-loader.component.scss']
})
export class GenericLoaderComponent implements OnInit, OnDestroy, AfterContentChecked {

  show = false;

  private subscription: Subscription;
  // loaderState: Observable<any>;

  constructor(
      private loaderService: GenericLoaderService,
      private changeDetector: ChangeDetectorRef
  ) { 
    
  }

  ngOnInit() { 

      this.subscription = this.loaderService.loaderState// .pipe(map((i) => i.show));
          .subscribe((state: LoaderState) => {
              this.show = state.show;
          });
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }
  ngAfterContentChecked(): void {
    this.changeDetector.detectChanges();
  }
}
