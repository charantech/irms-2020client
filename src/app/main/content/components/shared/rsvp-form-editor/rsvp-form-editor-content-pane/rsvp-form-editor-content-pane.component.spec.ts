import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsvpFormEditorContentPaneComponent } from './rsvp-form-editor-content-pane.component';

describe('RsvpFormEditorContentPaneComponent', () => {
  let component: RsvpFormEditorContentPaneComponent;
  let fixture: ComponentFixture<RsvpFormEditorContentPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsvpFormEditorContentPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsvpFormEditorContentPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
