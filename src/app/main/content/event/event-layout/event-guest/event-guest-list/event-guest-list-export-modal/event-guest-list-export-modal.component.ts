import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EventGuestDataService } from '../../event-guest-data.service';

@Component({
  selector: 'irms-event-guest-list-export-modal',
  templateUrl: './event-guest-list-export-modal.component.html',
  styleUrls: ['./event-guest-list-export-modal.component.scss']
})
export class EventGuestListExportModalComponent implements OnInit {
  formGroup: FormGroup;
  all = true;
  fieldNames = ['Title', 'FullName', 'PrefferedName', 'Gender', 'Email',
    'AlternativeEmail', 'MobileNumber', 'SecondaryMobileNumber',
    'Nationality', 'DocumentType', 'DocumentNumber', 'IssuingCountry',
    'ExpirationDate', 'Organization', 'Position', 'IsGuest',
    'CreatedBy', 'CreatorEmail', 'CreatedOn', 'ModifiedBy',
    'ModifierEmail', 'ModifiedOn'];
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<EventGuestListExportModalComponent>,
    private dataService: EventGuestDataService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.formGroup = this.formBuilder.group({
      title: [true],
      fullName: [true],
      prefferedName: [true],
      gender: [true],
      email: [true],
      alternativeEmail: [true],
      mobileNumber: [true],
      workNumber: [true],
      eventName: [true],
      secondaryMobileNumber: [true],
      nationality: [true],
      documentType: [true],
      documentNumber: [true],
      issuingCountry: [true],
      expirationDate: [true],
      organization: [true],
      position: [true],
      isGuest: [true],
      createdBy: [true],
      creatorEmail: [true],
      createdOn: [true],
      modifiedBy: [true],
      modifierEmail: [true],
      modifiedOn: [true],
    });
    
  }

  ngOnInit(): void {
    console.log(this.data)
  }


  close(): void {
    this.dialogRef.close();
  }

  onAllChange(event): void {
    if (!event) {
      this.formGroup.reset();
    } else {
      Object.keys(this.formGroup.controls).forEach(key => {
        this.formGroup.controls[key].setValue(true);
      });
    }
  }

  confirm(): void {
    this.loading=true;
    const columns = [];

    Object.keys(this.formGroup.controls).forEach(key => {
      if (this.formGroup.controls[key].value) {
        columns.push(key[0].toUpperCase() + key.substring(1));
      }
    });


    this.dataService
      .exportContacts({
        contactListId: this.data.contactListId,
        filename: this.data.filename,
        columns: columns,
        eventId: this.data.eventId,
        contactIds: this.data.contactIds
      }).subscribe(resp => {
        this.saveExportedContacts(resp);
        if(resp){
          this.dialogRef.close();
        }
      }, error =>{
        this.loading = false;
      })
      
  }

  private saveExportedContacts(resp: any) {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.data.filename}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

}
