import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { noImagePreview, MediaType, transparentSource } from 'app/constants/constants';
import { Router } from '@angular/router';
import { fuseAnimations } from 'app/core/animations';

@Component({
  selector: 'irms-template-thumbnail',
  templateUrl: './template-thumbnail.component.html',
  styleUrls: ['./template-thumbnail.component.scss'],
  animations: fuseAnimations
})
export class TemplateThumbnailComponent implements OnInit {
  @Input() hasTemplate;
  @Input() thumbnail;  
  @Input() templateId;
  @Input() loading;
  @Input() disabled;
  @Input() onlyEmitOnCreate;
  @Input() addContentText = 'Add Content';
  @Input() templateType: MediaType;
  @Input() toRsvp: boolean;
  @Input() isReview = false;
  @Output() create: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();

  public transparentSource = transparentSource;
  constructor(private router: Router) { 
  }
  ngOnInit(): void {
  }

  doPathUrl(path): string {
    return `url(${path})`;
  }

  newTemplate(): void{
    if (this.onlyEmitOnCreate) {
      this.create.emit(true); /// emits an event when create is clicked but does not route....used when invitatioon not created
      return;
    }
    if (this.templateType === MediaType.Email) {
        this.create.emit('email');
      }
    if (this.templateType === MediaType.Sms) {
     this.create.emit('sms');
    }
    if (this.templateType === MediaType.whatsapp) {
    this.create.emit('whatsapp');
    }
  }

  editTemplate(): void{
    if (this.templateType === MediaType.Email) {
      this.edit.emit('email');
    }
    if (this.templateType === MediaType.Sms) {
      this.edit.emit('sms');
     }
    if (this.templateType === MediaType.whatsapp) {
      this.edit.emit('whatsapp');
     }
    if (this.templateType === MediaType.Rfi) {
      this.isReview ? this.router.navigate([`/data-review/form-designer/${this.templateId}`]) : this.edit.emit("rfi");
    }
  }
}
