import { Component, OnInit } from '@angular/core';
import { GlobalContactListUploadFormService } from './global-contact-list-upload-form.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from '../../services/can-exit.guard';

@Component({
  selector: 'irms-global-contact-list-upload',
  templateUrl: './global-contact-list-upload.component.html',
  styleUrls: ['./global-contact-list-upload.component.scss']
})
export class GlobalContactListUploadComponent implements OnInit{
  constructor() {
   }

  ngOnInit() {
  }

}
