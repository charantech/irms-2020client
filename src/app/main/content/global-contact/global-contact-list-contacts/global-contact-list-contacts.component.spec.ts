import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactListContactsComponent } from './global-contact-list-contacts.component';

describe('GlobalContactListContactsComponent', () => {
  let component: GlobalContactListContactsComponent;
  let fixture: ComponentFixture<GlobalContactListContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListContactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
