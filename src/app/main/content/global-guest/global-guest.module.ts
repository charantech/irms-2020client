import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalGuestComponent } from './global-guest.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { RouterModule } from '@angular/router';
import { MainModule } from 'app/main/main.module';
import { GlobalGuestListComponent } from './global-guest-list/global-guest-list.component';
import { GlobalGuestListItemComponent } from './global-guest-list/global-guest-list-item/global-guest-list-item.component';
import { GlobalGuestListGuestsComponent } from './global-guest-list-guests/global-guest-list-guests.component';
import { GlobalGuestListGuestsItemComponent } from './global-guest-list-guests/global-guest-list-guests-item/global-guest-list-guests-item.component';
import { GlobalGuestDetailsComponent } from './global-guest-details/global-guest-details.component';
import { GlobalGuestDetailsPersonalInfoComponent } from './global-guest-details/global-guest-details-personal-info/global-guest-details-personal-info.component';
import { GlobalGuestDetailsCustomFieldsComponent } from './global-guest-details/global-guest-details-custom-fields/global-guest-details-custom-fields.component';
import { GlobalGuestDetailsAssociatedListsComponent } from './global-guest-details/global-guest-details-associated-lists/global-guest-details-associated-lists.component';
import { GlobalGuestDetailsOrganizationInfoComponent } from './global-guest-details/global-guest-details-organization-info/global-guest-details-organization-info.component';
import { GlobalGuestDetailsEventsHistoryComponent } from './global-guest-details/global-guest-details-events-history/global-guest-details-events-history.component';
import { GlobalGuestDetailsEventsHistoryListItemComponent } from './global-guest-details/global-guest-details-events-history/global-guest-details-events-history-list-item/global-guest-details-events-history-list-item.component';
import { GlobalGuestDetailsEventsHistoryDialogComponent } from './global-guest-details/global-guest-details-events-history/global-guest-details-events-history-dialog/global-guest-details-events-history-dialog.component';

const routes = [
  {
    path: '',
    component: GlobalGuestComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        component: GlobalGuestListComponent
      },
      {
        path: ':lid',
        canActivateChild: [AuthGuard],
        component: GlobalGuestListGuestsComponent
      },
      {
        path: ':lid/guest/:gid',
        canActivateChild: [AuthGuard],
        component: GlobalGuestDetailsComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        children: [
          {
            path: 'personal-info',
            canActivateChild: [AuthGuard],
            component: GlobalGuestDetailsPersonalInfoComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] }
          },
          {
            path: 'custom-fields',
            canActivateChild: [AuthGuard],
            component: GlobalGuestDetailsCustomFieldsComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] }
          },
          {
            path: 'associated-lists',
            canActivateChild: [AuthGuard],
            component: GlobalGuestDetailsAssociatedListsComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] }
          },
          {
            path: 'organization-info',
            canActivateChild: [AuthGuard],
            component: GlobalGuestDetailsOrganizationInfoComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] }
          },
          {
            path: 'event-history',
            canActivateChild: [AuthGuard],
            component: GlobalGuestDetailsEventsHistoryComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] }
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    GlobalGuestDetailsEventsHistoryComponent,
    GlobalGuestDetailsEventsHistoryListItemComponent,
    GlobalGuestComponent,
    GlobalGuestListComponent,
    GlobalGuestListItemComponent,
    GlobalGuestListGuestsComponent,
    GlobalGuestListGuestsItemComponent,
    GlobalGuestDetailsComponent,
    GlobalGuestDetailsPersonalInfoComponent,
    GlobalGuestDetailsCustomFieldsComponent,
    GlobalGuestDetailsAssociatedListsComponent,
    GlobalGuestDetailsOrganizationInfoComponent,
    GlobalGuestDetailsEventsHistoryDialogComponent],
  imports: [
    CommonModule,
    MainModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    GlobalGuestDetailsEventsHistoryDialogComponent,
    GlobalGuestDetailsEventsHistoryComponent,
    GlobalGuestDetailsEventsHistoryListItemComponent,
    GlobalGuestListItemComponent, GlobalGuestListGuestsItemComponent, GlobalGuestDetailsComponent]
})
export class GlobalGuestModule { }
