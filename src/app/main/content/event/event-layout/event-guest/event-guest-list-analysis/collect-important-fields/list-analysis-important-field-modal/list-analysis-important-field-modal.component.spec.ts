import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAnalysisImportantFieldModalComponent } from './list-analysis-important-field-modal.component';

describe('ListAnalysisImportantFieldModalComponent', () => {
  let component: ListAnalysisImportantFieldModalComponent;
  let fixture: ComponentFixture<ListAnalysisImportantFieldModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAnalysisImportantFieldModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAnalysisImportantFieldModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
