import { BaseModel } from 'app/main/content/components/shared/section/base.model';

export class EventGuestInfoModel extends BaseModel {
}

export class EventGuestInfoPersonalInfoModel extends BaseModel {
    title: string;
    fullName: string;
    preferredName: string;
    gender: string;
    documentNumber: string;
    expirationDate: string;
    organization: string;
    position: string;
    nationalityId: string;
    documentTypeId: string;
    issuingCountryId: string;
    email: string;
    alternativeEmail: string;
    mobileNumber: string;
    secondaryMobileNumber: string;
    workNumber: string;
}
