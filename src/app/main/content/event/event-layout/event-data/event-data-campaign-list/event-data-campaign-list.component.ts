import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventDataDataService } from '../event-data.data.service';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { EventDataCampaignListItemComponent } from './event-data-campaign-list-item/event-data-campaign-list-item.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Section } from 'app/constants/constants';
import { EventDataModel } from '../event-data.model';
import { EventDataModService } from '../event-data.service';
import { fuseAnimations } from 'app/core/animations';
import { DataModuleDataService } from '../../../data-module-data.service';
import { EventDataListAnalysisListItemComponent } from './event-data-list-analysis-list-item/event-data-list-analysis-list-item.component';
import { Observable } from 'rxjs';
import { IResponseModel } from 'types';

@Component({
  selector: 'irms-event-data-campaign-list',
  templateUrl: './event-data-campaign-list.component.html',
  styleUrls: ['./event-data-campaign-list.component.scss'],
  animations: fuseAnimations
})
export class EventDataCampaignListComponent extends SectionListComponent<EventDataModel> implements OnInit, OnDestroy {

  listHeaders = ['Campaign'];
  listAnalysisHeaders = ['List Analysis'];
  public listItemComponent: Object = EventDataCampaignListItemComponent;
  public listAnalysisData: Observable<IResponseModel>;
  public listItemComponentListAnalysis: Object = EventDataListAnalysisListItemComponent;
  eventId: any;
  constructor(public sectionService: EventDataModService,
              protected dataService: DataModuleDataService,
              router: Router, public route: ActivatedRoute) {
    super(Section.EventDataCampaign, sectionService, dataService, router);
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.sectionService.setFilter({eventId: params['id']});
      this.eventId = params['id'];
      super.ngOnInit();
      this.sectionService.doRefreshList();
    });

  }
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

}
