import { Component, OnInit, Inject, Input } from '@angular/core';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegexpPattern, ForbiddenPhoneCodes } from 'app/constants/constants';
import { SmsDesignerDataService } from '../sms-designer-data.service';
import { ActivatedRoute } from '@angular/router';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { CurrentUserService } from '../../services/current-user.service';

@Component({
  selector: 'irms-sms-designer-send-test-sms',
  templateUrl: './sms-designer-send-test-sms.component.html',
  styleUrls: ['./sms-designer-send-test-sms.component.scss']
})
export class SmsDesignerSendTestSmsComponent implements OnInit {

  public separatorKeysCodes = [ENTER, COMMA, SPACE];
  public phoneNumbersList = [];
  removable = true;
  testSmsForm: FormGroup;
  @Input() invitationId;

  constructor(
    private fb: FormBuilder,
    private sectionService: SmsDesignerDataService,
    private dialogRef: MatDialogRef<SmsDesignerSendTestSmsComponent>,
    private currentUserService: CurrentUserService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  forbiddenCodes = ForbiddenPhoneCodes;
  ngOnInit(): void {
    this.testSmsForm = this.fb.group({
      phoneNumbers: this.fb.array([]),
    });

    this.currentUserService.getCurrentUser().subscribe((user) => {
      this.phoneNumbersList.push({ value: user.phoneNo, invalid: false });
    });
  }

  addPhoneNumber(event): void {
    if (event.value) {
      if (this.validatePhoneNumber(event.value)) {
        this.phoneNumbersList.push({ value: event.value, invalid: false });
      } else {
        this.phoneNumbersList.push({ value: event.value, invalid: true });
      }
    }
    if (event.input) {
      event.input.value = '';
    }
    this.checkAllValid();
    if (this.testSmsForm.hasError('noPhoneNumbers')) {
      this.testSmsForm.setErrors({ noPhoneNumbers: null });
      this.testSmsForm.updateValueAndValidity();
    }
  }


  removePhoneNumber(data: any): void {
    if (this.phoneNumbersList.indexOf(data) >= 0) {
      this.phoneNumbersList.splice(this.phoneNumbersList.indexOf(data), 1);
    }
    this.checkAllValid();
    if (this.phoneNumbersList.length === 0) {
      this.testSmsForm.setErrors({ noPhoneNumbers: true });
    }
  }

  sendPhoneNumbers(): void {
    const testPhoneNumbers = this.phoneNumbersList.map(phoneNumber => phoneNumber.value);
    this.sectionService
      .sendTestSms({
        smsList: testPhoneNumbers,
        invitationId: this.invitationId
      }).subscribe(result => {
        const toasterService = appInjector().get(ToasterService);
        toasterService.pop('success', null, 'Successfully sent.');
      })

    this.dialogRef.close();
  }

  private checkAllValid(): void {
    let invalidFound = false;
    this.phoneNumbersList.forEach(phoneNumbers => {
      if (phoneNumbers.invalid) {
        invalidFound = true;
        this.testSmsForm.setErrors({ incorrectPhoneNumber: true });
      }
    });
    if (!invalidFound && this.testSmsForm.hasError('incorrectPhoneNumber')) {
      this.testSmsForm.setErrors({ incorrectPhoneNumber: null });
      this.testSmsForm.updateValueAndValidity();
    }

    if (this.phoneNumbersList.length > 10) {
      this.testSmsForm.setErrors({ overflow: true });
    } else if (this.testSmsForm.hasError('overflow')) {
      this.testSmsForm.setErrors({ overflow: null });
      this.testSmsForm.updateValueAndValidity();
    }
  }

  private validatePhoneNumber(phoneNumber: string): any {
    let isForbidden = false;
    this.forbiddenCodes.forEach(code => {
      if (phoneNumber.includes(code)) {
        isForbidden = true;
      }
    });
    if (isForbidden) {
      return false;
    }
    const pattern = new RegExp(RegexpPattern.intlPhone);
    return pattern.test(String(phoneNumber).toLowerCase());
  }

  /// On dialog close
  close(): void {
    this.dialogRef.close();
  }
}
