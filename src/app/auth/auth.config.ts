export interface IAuthConfig {
  globalHeaders?: Array<Object>;
  headerName?: string;
  headerPrefix?: string;
  noJwtError?: boolean;
  noTokenScheme?: boolean;
  loginEndPoint?: string;
  loginTokenName?: string;
  loginParams?: ILoginParams;
  guards?: {
    loggedInGuard: {
      redirectUrl: string;
    },
    loggedOutGuard: {
      redirectUrl: string;
    },
  };
}
export interface ILoginParams {
  [key: string]: string;
}

export class AuthConfig implements IAuthConfig {
  public headerName: string;
  public headerPrefix: string;
  public loginEndPoint: string;
  public guards?: {
    loggedInGuard: {
      redirectUrl: string;
    },
    loggedOutGuard: {
      redirectUrl: string;
    },
  };

  constructor(config: IAuthConfig = {}) {
    for (const option in config) {
      this[option] = config[option];
    }

    for (const option in AUTH_CONFIG_DEFAULTS)
      this[option] = config[option] != null ? config[option] : AUTH_CONFIG_DEFAULTS[option];
  }
}

export const AUTH_CONFIG_DEFAULTS: IAuthConfig = {
  headerName: 'Authorization',
  loginTokenName: 'access_token',
  headerPrefix: 'Bearer',
  noTokenScheme: false,
  noJwtError: false,
};
