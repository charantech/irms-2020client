import { Injectable } from '@angular/core';
import { SectionService } from '../components/shared/section/section.service';
import { EventModel } from './event.model';
import { EventDataService } from './event-data.service';
import { QueryService } from '../services/query.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EventService extends SectionService<EventModel> {

  constructor(
      protected dataService: EventDataService,
      queryService: QueryService,
      protected router: Router
  ) {
      super(dataService, queryService, router);
  }
}
