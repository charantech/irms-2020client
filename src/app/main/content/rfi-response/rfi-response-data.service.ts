import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { BASE_URL } from 'app/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class RfiResponseDataService {

  private url = `${BASE_URL}api/CampaignInvitationResponse`;

  constructor(private http: HttpClient) { }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/rfi/${id}`);
  }

  save(model): Observable<any> {
    return this.http.post<any>(`${this.url}/save-rfi`, model);
  }
}
