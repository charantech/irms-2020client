import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-contact-list-item',
  templateUrl: './contact-list-item.component.html',
  styleUrls: ['./contact-list-item.component.scss']
})
export class ContactListItemComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
