import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestListItemComponent } from './global-guest-list-item.component';

describe('GlobalGuestListItemComponent', () => {
  let component: GlobalGuestListItemComponent;
  let fixture: ComponentFixture<GlobalGuestListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
