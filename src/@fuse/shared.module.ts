import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { FuseDirectivesModule } from '@fuse/directives/directives';
import { FusePipesModule } from '@fuse/pipes/pipes.module';
import { DateTimeTriggerDirective } from 'app/directives/datetime-trigger/datetime-trigger';
import { EnforcedInputsDirective } from 'app/directives/enforced-input/enforced-input';

@NgModule({
    declarations: [
        DateTimeTriggerDirective,
        EnforcedInputsDirective,
    ],
    imports  : [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        FuseDirectivesModule,
        FusePipesModule
    ],
    exports  : [
        CommonModule,
        FormsModule,
        DateTimeTriggerDirective,
        EnforcedInputsDirective,
        ReactiveFormsModule,
        FlexLayoutModule,
        FuseDirectivesModule,
        FusePipesModule
    ]
})
export class FuseSharedModule
{
}
