import { Injectable } from '@angular/core';
import { EventCampaignModel } from './event-campaign.model';
import { EventCampaignDataService } from './event-campaign-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';
import { SectionService } from 'app/main/content/components/shared/section/section.service';

@Injectable({
  providedIn: 'root'
})
export class EventCampaignService extends SectionService<EventCampaignModel> {

  constructor(
    protected dataService: EventCampaignDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
