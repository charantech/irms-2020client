import { TestBed } from '@angular/core/testing';

import { DataReviewFormDesignerDataService } from './data-review-form-designer-data.service';

describe('DataReviewFormDesignerDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataReviewFormDesignerDataService = TestBed.get(DataReviewFormDesignerDataService);
    expect(service).toBeTruthy();
  });
});
