import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderThemePaneComponent } from './data-review-form-builder-theme-pane.component';

describe('DataReviewFormBuilderThemePaneComponent', () => {
  let component: DataReviewFormBuilderThemePaneComponent;
  let fixture: ComponentFixture<DataReviewFormBuilderThemePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataReviewFormBuilderThemePaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataReviewFormBuilderThemePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
