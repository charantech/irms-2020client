import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestListComponent } from './global-guest-list.component';

describe('GlobalGuestListComponent', () => {
  let component: GlobalGuestListComponent;
  let fixture: ComponentFixture<GlobalGuestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
