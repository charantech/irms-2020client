import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class GlobalContactListUploadFormService {
  private excelUploadStepSource: Subject<FormGroup> = new Subject();
  excelUploadStep: Observable<FormGroup> = this.excelUploadStepSource.asObservable();

  private mapFieldsStepSource: Subject<FormGroup> = new Subject();
  mapFieldsStep: Observable<FormGroup> = this.mapFieldsStepSource.asObservable();

  private validateStepSource: Subject<FormGroup> = new Subject();
  validateStep: Observable<FormGroup> = this.validateStepSource.asObservable();

  public contactListUploadForm: FormGroup = this.fb.group({
    listForm: this.fb.group({
      list: ['add', [Validators.required]],
      listName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
      listId: [''],
      eventId: [''],
      eventGuestId: [''],
      excelFile: ['hello', [Validators.required]]
    }),
    fieldsForm: this.fb.group({
      fields : []
    }),
    validateForm: this.fb.group({
      headers: [],
      contacts : []
    }) 
  });

  constructor(
    private fb: FormBuilder
  ) {
    this.excelUploadStep.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.contactListUploadForm.controls.listForm.patchValue(val);
      })
    );
    this.mapFieldsStep.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.contactListUploadForm.controls.fieldsForm.patchValue(val);
      })
    );
    this.validateStep.subscribe(form =>
      form.valueChanges.subscribe(val => {
        this.contactListUploadForm.controls.validateForm.patchValue(val);
      })
    );
  }

  stepReady(form: FormGroup, part): void {
    switch (part) {
      case 'excelUpload': { this.excelUploadStepSource.next(form); }
      case 'mapFields': { this.mapFieldsStepSource.next(form); }
      case 'validate': { this.validateStepSource.next(form); }
    }
  }

  setContactsData(data: FormArray, headers): void {
    this.contactListUploadForm.controls.validateForm.get('contacts').patchValue(data);
    this.contactListUploadForm.controls.validateForm.get('headers').patchValue(headers);
  }

  getContactsData(): any {
    return this.contactListUploadForm.controls.validateForm.value;
  }

  public getExcelFile(): any {
    return this.contactListUploadForm.get('listForm.excelFile').value ;
  }
}
