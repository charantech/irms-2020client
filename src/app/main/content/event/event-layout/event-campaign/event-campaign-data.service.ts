import { Injectable } from '@angular/core';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { EventCampaignModel } from './event-campaign.model';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { IEventList, IResponseModel } from 'types';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class EventCampaignDataService implements SectionDataService<EventCampaignModel> {

  private url = `${BASE_URL}api/campaign`;
  private campaignUrl = `${BASE_URL}api/campaigninvitation`;
  constructor(private http: HttpClient) { }

  @Loader()
  getList(filterParam: IEventList): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/list`, filterParam);
  }

  get(id): Observable<EventCampaignModel> {
    return this.http.get<EventCampaignModel>(`${this.url}/${id}/info`);
  }

  @Toast('Successfully created')
  create(model: any): Observable<any> {
    return this.http.post<any>(`${this.url}`, model);
  }

  @Toast('Successfully updated')
  update(model) {
    return this.http.put(`${this.url}`, model);
  }

  @Toast('Successfully deleted')
  delete(model) {
    return this.http.request('delete', `${this.url}`, { body: model });
  }

  loadCampaignSummary(id): Observable<any> {
    return this.http.get(`${this.campaignUrl}/campaign-summary/${id}`);
  }

  @Toast("Successfully sent")
  goLive(id): Observable<any> {
    return this.http.get(`${this.url}/go-live/${id}`);
  }

  testCampaign(data): Observable<any> {
    return this.http.post(`${this.url}/test-campaign`, data);
  }
}
