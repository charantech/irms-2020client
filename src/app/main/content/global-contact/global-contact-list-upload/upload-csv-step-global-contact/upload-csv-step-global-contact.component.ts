import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AsyncValidatorFn } from '@angular/forms';
import { GlobalContactService } from '../../global-contact.service';
import { GlobalContactDataService } from '../../global-contact-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { GlobalContactModel } from '../../global-contact.model';
import { Section } from 'app/constants/constants';
import { debounceTime, distinctUntilChanged, switchMap, map, first } from 'rxjs/operators';
import { GlobalContactListUploadFormService } from '../global-contact-list-upload-form.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'irms-upload-csv-step-global-contact',
  templateUrl: './upload-csv-step-global-contact.component.html',
  styleUrls: ['./upload-csv-step-global-contact.component.scss']
})
export class UploadCsvStepGlobalContactComponent extends SectionCreateComponent<GlobalContactModel> implements OnInit {

  contactImportListStep: FormGroup;
  lists = [];
  eventList = [];
  eventGuestLists = [];
  isListPreset = false;
  listId = '';
  eventId = '';
  isValid = true;
  state: number;
  eventGuestId: any;

  constructor(private fb: FormBuilder,
              private formService: GlobalContactListUploadFormService,
              public sectionService: GlobalContactService,
              protected dataService: GlobalContactDataService,
              protected router: Router,
              public route: ActivatedRoute) {
    super(Section.GlobalContact, sectionService, dataService, router);
    this.contactImportListStep = this.fb.group({
      list: ['add', [Validators.required]],
      listName: [''],
      listId: [''],
      eventId: [''],
      eventGuestId: [''],
      excelFile: ['', [Validators.required]]
    });
    this.formService.stepReady(this.contactImportListStep, 'excelUpload');
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];
      // check if activated route params have list id
      if (history.state.data && history.state.data.listId) {
        this.listId = history.state.data.listId;
        this.isListPreset = true;
        this.state = 1; // State 1 = User being redirected from contact list to Upload Excel
                        // The contact list is selected for the user
      } else if (history.state.data && history.state.data.eventId && !history.state.data.eventGuestId) {
        this.eventId = history.state.data.eventId;
        this.isListPreset = true;
        this.state = 2; // State 2 = User being redirected from event list to upload Excel
                        // Event is Selected but guest list is not
      } else if (history.state.data && history.state.data.eventId && history.state.data.eventGuestId) {
        this.eventId = history.state.data.eventId;
        this.eventGuestId = history.state.data.eventGuestId;
        this.isListPreset = true;
        this.state = 3; // State 3 = User being redirected from a Guest List to upload Excel
                        // Both Event & Guest List is Selected
      }
    });
    const data = {
      pageNo: 1,
      pageSize: 100,
      searchText: '',
      eventId: ''
    };
    this.dataService.getList(data).subscribe(res => {
      this.lists = res.items;
      if (this.isListPreset && this.state === 1) {
        this.contactImportListStep.controls.list.setValue('existing');
        const index = this.lists.map(e => e.id).indexOf(this.listId); // get index of preset list from routing
        const list = { id: this.lists[index].id, name: this.lists[index].name };
        this.contactImportListStep.controls['listId'].setValue(list);
      }
    });
    this.dataService.getEvents().subscribe(res => {
      this.eventList = res.items;
      if (this.isListPreset && (this.state === 2 || this.state === 3)) {
        this.contactImportListStep.controls.list.setValue('eventGuest');
        const index = this.eventList.map(e => e.id).indexOf(this.eventId); // get index of preset list from routing
        const list = { id: this.eventList[index].id, name: this.eventList[index].name };
        this.contactImportListStep.controls['eventId'].setValue(list);
      }
    });
  }
  ngOnInit(): void {
    // Select list form custom validator
    this.contactImportListStep.get('list').valueChanges
      .subscribe((value: string) => {
        if (value === 'new') {
          this.setValidators('contactImportListStep', 'listName', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]);
          this.contactImportListStep.controls['listName'].setAsyncValidators(this.validateListNameUniqueness());
          this.contactImportListStep.controls['listName'].valueChanges.subscribe(x => {
            this.isValid = false;
          });
          this.clearValidators('contactImportListStep', ['listId', 'eventId', 'eventGuestId']);
        } else if (value === 'existing') {
          this.setValidators('contactImportListStep', 'listId', [Validators.required]);
          this.clearValidators('contactImportListStep', ['listName', 'eventId', 'eventGuestId']);
          this.contactImportListStep.controls['listName'].setValue('');
        } else if (value === 'eventGuest') {
          this.setValidators('contactImportListStep', 'eventId', [Validators.required]);
          this.setValidators('contactImportListStep', 'eventGuestId', [Validators.required]);
          this.clearValidators('contactImportListStep', ['listName', 'listId']);
          this.contactImportListStep.controls['listName'].setValue('');
        } else {
          this.clearValidators('contactImportListStep', ['listName', 'listId', 'eventId', 'eventGuestId']);
        }
        
      });
    this.contactImportListStep.get('eventId').valueChanges.subscribe(val => {
        if (val && val.id ){
        this.dataService.getGuestList({ pageNo: 1, pageSize: 100, eventId: val.id }).subscribe(lists => {
          this.eventGuestLists = lists.items;
          if (this.state === 3){
            const index = this.eventGuestLists.map(e => e.id).indexOf(this.eventGuestId); // get index of preset list from routing
            if (index > -1) {
              this.contactImportListStep.controls['eventGuestId'].setValue(this.eventGuestLists[index]);
            }
          }
        });
      }
      });
  }


  validateListNameUniqueness(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(value => this.dataService.isListNameUnique(control.value)),
        map((unique: boolean) => {
          this.isValid = !unique;
          return !unique ? null : { listNameTaken: true };
        }),
        first());
  }

  loadTemplate(): void {
    this.dataService.loadImportTemplate()
      .subscribe(x => {
        this.saveImportResult(x);
      });
  }

  saveImportResult(resp: any): void {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `template.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
}
