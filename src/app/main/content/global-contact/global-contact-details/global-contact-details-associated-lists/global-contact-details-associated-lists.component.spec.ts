import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactDetailsAssociatedListsComponent } from './global-contact-details-associated-lists.component';

describe('GlobalContactDetailsAssociatedListsComponent', () => {
  let component: GlobalContactDetailsAssociatedListsComponent;
  let fixture: ComponentFixture<GlobalContactDetailsAssociatedListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactDetailsAssociatedListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactDetailsAssociatedListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
