import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignFlowNodesService {
  private parentNode = new Subject<any>();
  firstNode = false;
  constructor() { }
  setFirstNode(data: any): void {
    this.parentNode.next(data);
    this.firstNode = data;
  }

  isFirstNode(): Observable<any> {
    return this.parentNode.asObservable();
  }
}
