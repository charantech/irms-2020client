import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EventCampaignModel } from '../../event-campaign.model';
import { CampaignInvitationDataService } from './campaign-invitation-data.service';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { QueryService } from 'app/main/content/services/query.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationService extends SectionService<EventCampaignModel> {

  constructor(
    protected dataService: CampaignInvitationDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
