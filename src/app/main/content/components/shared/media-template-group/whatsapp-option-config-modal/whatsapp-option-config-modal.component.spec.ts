import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappOptionConfigModalComponent } from './whatsapp-option-config-modal.component';

describe('WhatsappOptionConfigModalComponent', () => {
  let component: WhatsappOptionConfigModalComponent;
  let fixture: ComponentFixture<WhatsappOptionConfigModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappOptionConfigModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappOptionConfigModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
