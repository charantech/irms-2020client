import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportantFieldsComponent } from './important-fields.component';

describe('ImportantFieldsComponent', () => {
  let component: ImportantFieldsComponent;
  let fixture: ComponentFixture<ImportantFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportantFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportantFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
