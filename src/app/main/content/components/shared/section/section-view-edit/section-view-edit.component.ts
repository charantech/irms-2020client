import {Router, Params, ActivatedRoute} from '@angular/router';
import {SectionDataService} from '../section-data.service';
import {SectionService} from '../section.service';
import {BaseModel} from '../base.model';
import {ViewChild, OnInit, OnDestroy} from '@angular/core';
import {Section} from '../../../../../../constants/constants';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';
import {FormArray, FormGroup} from '@angular/forms';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseWidgetComponent } from '@fuse/components/widget/widget.component';

export class SectionViewEditComponent<T extends BaseModel> implements OnInit, OnDestroy {
  constructor(protected sectionName: Section,
              public sectionService: SectionService<T>,
              protected dataService: SectionDataService<T>,
              protected router: Router,
              protected route: ActivatedRoute) {
  }

  @ViewChild(FusePerfectScrollbarDirective, {static: false}) scrollBar;

  public isDataLoaded = false;
  protected componentDestroyed: Subject<any> = new Subject();
  public model: T;

  subscriptions: Subscription[] = [];

  @ViewChild(FuseWidgetComponent, {static: false}) fuseWidget: FuseWidgetComponent;

  ngOnInit(): void {
    this.subscriptions.push(this.route.params.subscribe((params: Params) => {
      this.dataService.get(params['id']).subscribe(e => {
        this.sectionService.onCurrentChanged.next(e);
        this.isDataLoaded = true;
      });
    }));
    this.subscriptions.push(this.sectionService.onCurrentChanged.subscribe(m => {
      this.model = m;
      if (m) {
        this.patchModelInForms();
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  setValidators(form, field, validators): void {
    if (this[form].get(field)) {
      this[form].get(field).setValidators(validators);
      this[form].get(field).updateValueAndValidity();
    }
  }

  setRequiredValidator(form , fieldsArr, validatorsArr): void {
    for (let i = 0; i < fieldsArr.length; i++) {
      if (this[form].get(fieldsArr[i])) {
        this[form].get(fieldsArr[i]).setValidators(validatorsArr);
        this[form].get(fieldsArr[i]).updateValueAndValidity();
      }
    }
  }

  clearValidators(form , fieldsArr): void {
    for (let i = 0; i < fieldsArr.length; i++) {
      if (this[form].get(fieldsArr[i])) {
        this[form].get(fieldsArr[i]).clearValidators();
        this[form].get(fieldsArr[i]).updateValueAndValidity();
      }
    }
  }

  changeFieldRequiredValidator(form, setFields, clearFields, validatorsArr): void {
    this.setRequiredValidator(form, setFields, validatorsArr);
    this.clearValidators(form, clearFields);
  }

  toggleValidators(form, fieldsArr, condition, validators): void {
    if (condition) {
      for (let i = 0; i < fieldsArr.length; i++) {
        this.setValidators(form, fieldsArr[i], validators);
      }
    }
    else {
      this.clearValidators(form, fieldsArr);
    }
  }

  public touchedFormFields(form): void {
    for (const inner in form.controls) {
      if (form.controls.hasOwnProperty(inner)) {
        form.get(inner).markAsTouched({onlySelf: true});
        form.get(inner).updateValueAndValidity();
      }
    }
  }
  public markFormGroupTouched(formGroup: FormGroup): void {
    (Object as any).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }


  public patchModelInForms(): void {
  }

  public isEmptyObject(obj): any {
    return (obj && (Object.keys(obj).length === 0));
  }
  public closeEntity(): void {
    this.router.navigate([`/${this.sectionName}`]);
    this.sectionService.onCurrentChanged.next(null);
  }

  public toggleCollapseDetails(): void {
    this.sectionService.isVisibleListBox = !this.sectionService.isVisibleListBox;
  }

  public resetFormValue(form): void {
    for (const name in this[form].controls) {
      if (this[form].controls.hasOwnProperty(name)) {
        this[form].controls[name].setValue('');
        this[form].controls[name].setErrors(null);
      }
    }
  }

  public resetFormControlValue(form, controlName): void {
    if (this[form].controls.hasOwnProperty(controlName)) {
      this[form].controls[controlName].setValue('');
      this[form].controls[controlName].setErrors(null);
    }
  }

  public clearFormArrayValidators(form, fieldName): void {
    const formArray = this[form]['controls']['items'] as FormArray;
    for (let i = 0; i < formArray.controls.length; i++) {
      formArray.at(i).get(fieldName).clearValidators();
      formArray.at(i).get(fieldName).updateValueAndValidity();
    }
  }

  public setFormArrayValidators(form, fieldName, validators): void {
    const formArray = this[form]['controls']['items'] as FormArray;
    for (let i = 0; i < formArray.controls.length; i++) {
      formArray.at(i).get(fieldName).setValidators(validators);
      formArray.at(i).get(fieldName).updateValueAndValidity();
    }
  }
}
