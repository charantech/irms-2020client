import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestListCreateComponent } from './event-guest-list-create.component';

describe('EventGuestListCreateComponent', () => {
  let component: EventGuestListCreateComponent;
  let fixture: ComponentFixture<EventGuestListCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestListCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestListCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
