import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmailDesignerDataService } from 'app/main/content/email-designer/email-designer-data.service';

@Component({
  selector: 'irms-email-landing-design',
  templateUrl: './email-landing-design.component.html',
  styleUrls: ['./email-landing-design.component.scss']
})
export class EmailLandingDesignComponent implements OnInit {
  public rsvpForm = this.fb.group(
    {
      formContent: this.fb.group({
        accept: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`, [Validators.required]]
        }),
        reject: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`, [Validators.required]]
        })
      }),
      formTheme: this.fb.group({
        buttons: this.fb.group({
          background: '#576268',
          text: '#fff',
        }),
        background: this.fb.group({
          color: '#fff',
          image: '',
          style: '',
        }),
      }),
      formSettings: this.fb.group({
        title: ['']
      })
    });
  constructor(private fb: FormBuilder, 
              public dialogRef: MatDialogRef<EmailLandingDesignComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dataService: EmailDesignerDataService) { }

  ngOnInit() {
    this.rsvpForm.get('formContent.accept.content').setValue(this.data.formAccept);
    this.rsvpForm.get('formContent.reject.content').setValue(this.data.formReject);
    this.rsvpForm.get('formTheme').patchValue(this.data.formTheme);
    if (this.data['backgroundImagePath']) {
      this.rsvpForm.controls['formTheme']['controls'].background.controls.image.setValue(this.data['backgroundImagePath']);
    }
    // this.rsvpForm.get('formTheme.background.image').setValue(this.data.backgroundImagePath);
  }

  save(): void {
    /// Save RSVP response form

    const background = this.rsvpForm.get('formTheme') && this.rsvpForm.get('formTheme.background');
    const theme = this.rsvpForm.get('formTheme');

    let img = '';
    img = background && background.get('image').value;
    if (background && !background.get('image').value) {
      img = theme.get('background.image').value;
    }
    if (!img) {
      img = '';
    }

    // theme.get('background.image').setValue('');
    if (this.data.tempId) {
      const model = {
        id: this.data.tempId,
        acceptHtml: this.rsvpForm.controls.formContent['controls'].accept.controls.content.value,
        rejectHtml: this.rsvpForm.controls.formContent['controls'].reject.controls.content.value,
        themeJson: JSON.stringify(theme.value),
        formSettings: JSON.stringify(this.rsvpForm.controls.formSettings.value),
        backgroundImage: img
      };

    //  delete model/themeJson.background.image'];

      if (this.rsvpForm.value.formTheme && this.rsvpForm.value.formTheme.background){
        model['backgroundImage'] = this.rsvpForm.value.formTheme.background.image;
      }

      const fd = new FormData();
      for (const key in model) {
        fd.append(key, model[key]);
      }
      

      this.dataService.updateResponseForm(fd).subscribe(id => {
        this.rsvpForm.markAsPristine();
      });
    }
  }

  close(): void {
    this.dialogRef.close(true);
    this.dialogRef.afterClosed().subscribe(x => this.dialogRef = null);
  }
}
