import { Injectable } from '@angular/core';
import { EventDataDataService } from './event-data.data.service';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';
import { EventDataModel } from './event-data.model';
import { DataModuleDataService } from '../../data-module-data.service';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventDataModService extends SectionService<EventDataModel> {
  targetRFIFormId;
  private responsesCount = new Subject();
  constructor(
    protected dataService: DataModuleDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }

  getResponsesCount(): Observable<any> {
    return this.responsesCount.asObservable();
  }

  setResponsesCount(count: number): void {
    this.responsesCount.next(count);
  }
}
