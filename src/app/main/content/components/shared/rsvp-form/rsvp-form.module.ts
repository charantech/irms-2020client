import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RsvpFormComponent } from './rsvp-form.component';
import { MainModule } from 'app/main/main.module';
import { RsvpFormService } from './rsvp-form.service';
import { SwipperFormComponentsModule } from '../swipper-form-components/swipper-form-components.module';
import { RsvpFormEditorTagsComponent } from '../rsvp-form-editor/rsvp-form-editor-content-pane/rsvp-form-editor-tags/rsvp-form-editor-tags.component';



@NgModule({
  declarations: [
    RsvpFormComponent,
    RsvpFormEditorTagsComponent
  ],
  imports: [
    CommonModule,
    MainModule,
    SwipperFormComponentsModule
  ],
  providers: [RsvpFormService],
  exports: [RsvpFormComponent, RsvpFormEditorTagsComponent]
})
export class RsvpFormModule { }
