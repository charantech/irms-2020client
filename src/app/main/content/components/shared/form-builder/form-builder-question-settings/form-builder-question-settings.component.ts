import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { QuestionIcon, QuestionColorCode, QuestionTypeTitle } from '../../swipper-form-components/form-constants/form-questions';

@Component({
  selector: 'irms-form-builder-question-settings',
  templateUrl: './form-builder-question-settings.component.html',
  styleUrls: ['./form-builder-question-settings.component.scss']
})
export class FormBuilderQuestionSettingsComponent implements OnInit {
  @Input() public question: FormGroup;

  questionTypeTitle = QuestionTypeTitle;
  questionColorCode = QuestionColorCode;
  questionIcon = QuestionIcon;
  showMinMaxLength = ['singleLineText', 'multilineText'];
  commonPatterns = [
    {
      name: 'General',
      patterns: [
        {
          regex: `^[0-9]*$`,
          title: 'Numbers only ',
          hint: '0, 1, 3, ...'
        },
        {
          regex: `^[a-zA-Z\u0621-\u064A]*$`,
          title: 'Letters only ',
          hint: 'abcABc'
        },
        {
          regex: `^[0-9a-zA-Z\u0621-\u064A]*$`,
          title: 'Numbers + Letters ',
          hint: 'Abc123'
        },
        {
          regex: `^[a-zA-Z\u0621-\u064A]+[ a-zA-Z\u0621-\u064A]*[a-zA-Z\u0621-\u064A]+$`,
          title: 'Full name ',
          hint: 'John Doe Bob'
        },
        {
          regex: `^[0-9a-zA-Z\u0621-\u064A]*$`,
          title: 'Numbers + Letters ',
          hint: 'Abc123'
        },
        {
          regex: `/^((?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4})|((?!219 09 9999|078 05 1120)(?!666|000|9\d{2})\d{3} (?!00)\d{2} (?!0{4})\d{4})|((?!219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4})$/`,
          title: 'Social Security Number (SSN) ',
          hint: '123456789'
        },
      ]
    },
    {
      name: 'URL',
      patterns: [
        {
          regex: `/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#()?&//=]*)/`,
          title: 'Url with http(s):// ',
          hint: 'https://www.hello.com'
        },
        {
          regex: `/(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/`,
          title: 'Any Url ',
          hint: 'example.com.we'
        },
        {
          regex: `^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+$`,
          title: 'Youtube link ',
          hint: 'https://www.youtube.com/abc123'
        }
      ]
    }
  ];
  constructor() { }
  ngOnInit() {
    // this.question.valueChanges.subscribe(x => console.log("x",x))
  }

  change(){
  }

}
