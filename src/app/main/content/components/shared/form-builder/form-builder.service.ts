import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { QueryService } from 'app/main/content/services/query.service';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { BaseModel } from '../section/base.model';
import { FormBuilderDataService } from './form-builder-data.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormBuilderService extends SectionService<BaseModel> {
  private selectedFields = new Subject<any>();
  public isFormLoaded = new Subject<boolean>();
  public selectedFieldArr = [];
  private formContent = new Subject<any>();

  
  constructor(
    protected dataService: FormBuilderDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }

  setIsFormLoaded(val: boolean){
    this.isFormLoaded.next(val);
  }

  getIsFormLoaded(): Observable<boolean>{
    return this.isFormLoaded.asObservable();
  }

  setSelectedFields(data: any): void {
    this.selectedFields.next(data);
    this.selectedFieldArr = data;
  }

  getSelectedFields(): Observable<any> {
    return this.selectedFields.asObservable();
  }

  setFormContent(data: any): void {
    this.formContent.next(data);
  }

  getFormContent(): Observable<any> {
    return this.formContent.asObservable();
  }

}
