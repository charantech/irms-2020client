import { OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, NavigationEnd, ActivationEnd } from '@angular/router';
import { SectionService } from '../section.service';
import { SectionDataService } from '../section-data.service';
import { BaseModel } from '../base.model';
import { Subscription } from 'rxjs/Subscription';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { IResponseModel, IListPager } from 'app/core/types/types';
import { filter } from 'rxjs/operators';
import { BASE_PAGE_SIZE_OPTIONS, Section } from 'app/constants/constants';



export class SectionListComponent<T extends BaseModel> implements OnInit, OnDestroy {

  @ViewChildren(FusePerfectScrollbarDirective) scrollBars: QueryList<FusePerfectScrollbarDirective>; //.first - list, .last - details

  public data: Observable<IResponseModel>;
  public childComponent;
  public pageSizeOptions = BASE_PAGE_SIZE_OPTIONS;
  public isSubroute = false;
  public totalCount = 0;
  public maxSelectable = 0;
  public checked = false;
  public indeterminate = false;
  public isData = false;
  



  subscriptions: Subscription[] = [];
  private hasId = false;
  private notRefreshList = false;
  allSelectables = [];
  allData: IResponseModel;
  isParent: any;

  constructor(protected sectionName: Section,
    public sectionService: SectionService<T>,
    protected dataService: SectionDataService<T>,
    protected router: Router) {
    this.subscriptions.push(this.router.events.pipe(filter((e: any) => e instanceof NavigationEnd || e instanceof ActivationEnd)).subscribe((event) => {
      if (event instanceof ActivationEnd && event.snapshot.params.id) {
        this.hasId = true;
      }
      if (event instanceof NavigationEnd) {
        if (event.url.toLowerCase().replace(/\/$/, '') === `/${this.sectionName}`) {
          this.sectionService.onCurrentChanged.next(null);
          this.sectionService.selectedItemId = null;
          this.sectionService.isVisibleListBox = true;
        }
        let leaf = this.router.routerState.snapshot.root;
        while (leaf && leaf.children && leaf.children.length > 0/* && Object.keys(leaf.params).length === 0*/) {
          leaf = leaf.children[0];
        }
        this.notRefreshList = SectionListComponent.isPrototypeOf(leaf.parent.routeConfig.component) || this.hasId; // If we are one step below list
        this.isSubroute = SectionListComponent.isPrototypeOf(leaf.parent.routeConfig.component); // If we are one step below list        this.sectionService.init();
        // this.isParent = 
      }

    }));
  }

  ngOnInit() {
    this.subscriptions.push(this.sectionService.onCurrentChanged.subscribe(model => {
      this.sectionService.selectedItemId = model ? model.id : null;
    }));

    this.data = this.sectionService.data;

    this.subscriptions.push(this.sectionService.resetScroll.subscribe(v => {
      if (this.scrollBars && this.scrollBars.first) {
        this.scrollBars.first.scrollToTop();
      }
    }));

    this.subscriptions.push(this.sectionService.resetSubScroll.subscribe(v => {
      if (this.scrollBars && this.scrollBars.first) {
        this.scrollBars.last.scrollToTop();
      }
    }));
    if (!this.notRefreshList) {
      this.hasId = false;
      this.sectionService.refreshList();
    }
  }

  public changePagination(paginationFilter: IListPager) {
    if (this.sectionService.pageIndex !== paginationFilter.pageIndex || this.sectionService.pageSize !== paginationFilter.pageSize) {
      this.scrollBars.first.scrollToTop();
    }
    this.sectionService.pageIndex = paginationFilter.pageIndex;
    this.sectionService.pageSize = paginationFilter.pageSize;
    this.sectionService.refreshList();
    this.sectionService.handleRowCheckChange([]);
  }

  public selectItem(item) {
    this.sectionService.isCreated = false;
    this.router.navigate([`/${this.sectionName}/${item.id}`]);
  }


  public toggleSelectAll(): any {
    this.setMaxSelectable();
    if(!this.checked){
      this.allData.items.forEach(element => {
        this.allSelectables.push(element.id);
        this.checked = true;
        this.indeterminate = false;
      });
    } else {
      this.allSelectables = [];
    }
    return this.allSelectables;
  }

  setMaxSelectable(){
    if (this.data != null) {
      this.isData = true;
      this.data.subscribe(x => {
        this.allData = x;
        this.maxSelectable = x.items.length;
      });
    }
  }

  ngOnDestroy() {
    // this.sectionService.data.;
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    // this.sectionService.data.next(null);
    this.sectionService.resetFilter();
  }

}
