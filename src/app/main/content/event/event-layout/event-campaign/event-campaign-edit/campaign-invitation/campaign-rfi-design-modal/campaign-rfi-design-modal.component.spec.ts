import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignRfiDesignModalComponent } from './campaign-rfi-design-modal.component';

describe('CampaignRfiDesignModalComponent', () => {
  let component: CampaignRfiDesignModalComponent;
  let fixture: ComponentFixture<CampaignRfiDesignModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignRfiDesignModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignRfiDesignModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
