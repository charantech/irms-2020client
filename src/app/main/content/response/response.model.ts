import { BaseModel } from 'app/main/content/components/shared/section/base.model';

export class ResponseModel extends BaseModel {
    data: string
}