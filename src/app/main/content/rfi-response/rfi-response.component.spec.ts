import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfiResponseComponent } from './rfi-response.component';

describe('RfiResponseComponent', () => {
  let component: RfiResponseComponent;
  let fixture: ComponentFixture<RfiResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfiResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfiResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
