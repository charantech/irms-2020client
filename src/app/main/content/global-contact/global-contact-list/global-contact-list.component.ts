import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from 'app/core/animations';
import { SectionListComponent } from '../../components/shared/section/section-list/section-list.component';
import { GlobalContactListItemComponent } from './global-contact-list-item/global-contact-list-item.component';
import { GlobalContactService } from '../global-contact.service';
import { GlobalContactDataService } from '../global-contact-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Section } from 'app/constants/constants';
import { GlobalContactModel } from '../global-contact.model';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { GlobalContactCreateComponent } from '../global-contact-list-create/global-contact-list-create.component';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { appInjector } from 'app/main/app-injector';

@Component({
  selector: 'irms-global-contact-list',
  templateUrl: './global-contact-list.component.html',
  styleUrls: ['./global-contact-list.component.scss'],
  animations: fuseAnimations
})
export class GlobalContactListComponent extends SectionListComponent<GlobalContactModel> implements OnInit, OnDestroy {
  public childComponent: Object = GlobalContactListItemComponent;
  public selectable = false;
  listHeaders = ['Name', 'Count', 'Imported From', 'Imported By', 'Date', 'Actions'];
  public count = 0;
  constructor(public sectionService: GlobalContactService,
              protected dataService: GlobalContactDataService,
              router: Router, public route: ActivatedRoute, private dialog: MatDialog) {
    super(Section.GlobalContact, sectionService, dataService, router);
    this.dataService.getGlobalContactsCount().subscribe(result => this.count = result);
  }

  ngOnInit(): void {
    this.sectionService.resetFilter();
    super.ngOnInit();
  }

  // Handle Search 
  searchQueryChange(event): void{
    this.sectionService.resetQueryString(event);
  }

  /// Handle Create New Contact List 
  createList(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '500px';
    dialogConfig.maxWidth = '740px';
    const dialogRef = this.dialog.open(GlobalContactCreateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        this.dataService.createList(data).subscribe(result => {
          const toasterService = appInjector().get(ToasterService);
          if (result) {
            this.sectionService.doRefreshList();
            toasterService.pop('success', null, 'Successfully created');
          } else {
            toasterService.pop('error', null, 'List name must be unique.');
          }
        });
      });
  }

  public goToAllContacts(): void {
    this.router.navigateByUrl(`${Section.GlobalContact}/all-contacts`);
  }

  ngOnDestroy(): void {
    this.sectionService.resetFilter();
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    super.ngOnDestroy();
  }
}
