import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'preferredMediaListFilter',
  pure: false
})
export class PreferredMediaListFilterPipe implements PipeTransform {

  transform(items: any[], filter: any): any {
    if (!items || (!filter.queryString && filter.selectedFilters.length === 0)) {
      return items;
    }
    // filter items array, items which match and return true will be
    // kept, false will be filtered out

    // CASE: only query string provided
    else if (items && filter.queryString && filter.selectedFilters.length === 0) {
      return items.filter(item => item.controls.fullName.value.toLowerCase().includes(filter.queryString.toLowerCase()));
    }

    // CASE: only selected media provided
    else if (items && !filter.queryString && filter.selectedFilters.length > 0) {
      return items.filter(item => {
        const filterEmail =  filter.selectedFilters.indexOf('email') > -1 ? true : false;
        const filterSms =  filter.selectedFilters.indexOf('sms') > -1 ? true : false;
        const filterWhatsapp =  filter.selectedFilters.indexOf('whatsapp') > -1 ? true : false;
        if ((filterEmail && item.controls.emailDisabled.value) || 
              (filterSms && item.controls.smsDisabled.value) || 
              (filterWhatsapp && item.controls.whatsAppDisabled.value)){
                return true;
          } else { return false; }
      });
    }

    // CASE: selected media and filter provided
    else if (items && filter.queryString && filter.selectedFilters.length > 0) {
      return items.filter(item => {
        if (item.controls.fullName.value.toLowerCase().includes(filter.queryString.toLowerCase())){
          const filterEmail =  filter.selectedFilters.indexOf('email') > -1 ? true : false;
          const filterSms =  filter.selectedFilters.indexOf('sms') > -1 ? true : false;
          const filterWhatsapp =  filter.selectedFilters.indexOf('whatsapp') > -1 ? true : false;
          if ((filterEmail && item.controls.emailDisabled.value) || 
              (filterSms && item.controls.smsDisabled.value) || 
              (filterWhatsapp && item.controls.whatsAppDisabled.value)){
                return true;
          } else { return false; }
        } else {
          return false;
        }
      });
    }
  }
}
