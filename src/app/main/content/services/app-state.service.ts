import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { IResponseSimpleEntity } from '../../../../types';
import { BASE_URL } from '../../../constants/constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppStateService {
  public headerTitle: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private urlAccount = `${BASE_URL}api/Account`;
  private urlDictionary = `${BASE_URL}api/dictionary`;
  private urlSystem = `${BASE_URL}api/system`;

  constructor(private http: HttpClient) { }

  setTitle(value: string) {
    this.headerTitle.next(value);
  }

  getTenantLogo(): Observable<any> {
    return this.http.get<any>(`${BASE_URL}api/tenant/logo`);
  }

  getVersion(){
    return this.http.get( `${this.urlSystem}/version`, {responseType: 'text'});
  }

  // getLang(): Observable<any> {
  //   return this.http.get(`${this.urlDictionary}`);
  // }

  checkPhone(phone) {
    return this.http.post(`${this.urlAccount}/CheckPhoneUniqueness`, phone);
  }

  checkEmail(email) {
    return this.http.post(`${this.urlAccount}/CheckEmailUniqueness`, email);
  }

  checkNationalId(nationalId) {
    return this.http.post(`${this.urlAccount}/CheckNationalIdUniqueness`, nationalId);
  }

  getTaskTemplatesPage(filterParam): Observable<IResponseSimpleEntity[]> {
    return this.http.post<IResponseSimpleEntity[]>(`${this.urlDictionary}/taskTemplatesPage`, filterParam);
  }

  getCountries(): Observable<any[]> {
    return this.http.get<any[]>(`${this.urlDictionary}/country`);
  }

  GetNationalities(): Observable<any[]> {
    return this.http.get<any[]>(`${this.urlDictionary}/nationality`);
  }

  GetDocumentTypes(): Observable<any[]> {
    return this.http.get<any[]>(`${this.urlDictionary}/document-type`);
  }

  getEventTypes(): Observable<any[]> {
    return this.http.get<any[]>(`${this.urlDictionary}/event-type`);
  }

  // getTimeZones(): Observable<any[]> {
  //   return this.http.get<any[]>(`${this.urlDictionary}/time-zone`);
  // }
}
