import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { GlobalGuestModel } from '../global-guest.model';
import { GlobalGuestService } from '../global-guest.service';
import { GlobalGuestDataService } from '../global-guest-data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Section, sideDialogConfig } from 'app/constants/constants';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { of } from 'rxjs';
import { GlobalGuestListGuestsItemComponent } from './global-guest-list-guests-item/global-guest-list-guests-item.component';
import { GlobalGuestListGuestsDataService } from './global-guest-list-guests-data.service';
import { GlobalGuestListGuestsService } from './global-guest-list-guests.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GlobalContactDataService } from '../../global-contact/global-contact-data.service';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';
import { DialogAnimationService } from '../../services/dialog-animation.service';

@Component({
  selector: 'irms-global-guest-list-guests',
  templateUrl: './global-guest-list-guests.component.html',
  styleUrls: ['./global-guest-list-guests.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestListGuestsComponent extends SectionListComponent<GlobalGuestModel> implements OnInit {
  public listItemComponent: Object = GlobalGuestListGuestsItemComponent;
  public listHeaders = ['Guest Name', 'Mobile Number', 'Email', 'Status'];
  selectedGuests = [];
  public guestsListGuest;
  listName: any = "List Name";
  guestListPage;
  listId: any;

  constructor(
    private dialog: MatDialog,
    public sectionService: GlobalGuestListGuestsService,
    public dataService: GlobalGuestListGuestsDataService,
    router: Router,
    public route: ActivatedRoute,
    private exportService: GlobalContactDataService,
    public animDialog: DialogAnimationService
  ) {
    super(Section.EventGuests, sectionService, dataService, router);
  }

  ngOnInit() {
    this.sectionService.subscriptions = [];
    this.route.params.subscribe((params: Params) => {
      this.listId = params.lid;

      this.guestListPage = {
        listId: params.lid,
        pageSize: 5,
        pageNo: 1
      };

      this.dataService.getListName({
        id: params.lid
      })
        .subscribe(result => {
          this.listName = result;
        });

      this.sectionService.setFilter({ listId: params.lid });
      super.ngOnInit();
    });
    //this.reloadGuests();
  }

  reloadGuests() {
    this.dataService.getList(this.guestListPage)
      .subscribe(response => {
        this.data = of(response);
        this.sectionService.data.next(response);
        this.totalCount = response.totalCount;
      });
  }

  exportGuests() {
    const dialogConfig:any = sideDialogConfig;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    // dialogConfig.height = '55vh';
    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const columns = result.columns;
          const customFieldIds = result.customFieldIds;

          this.exportService
            .exportContacts({
              contactListId: this.listId,
              filename: this.listName.listName,
              columns: columns,
              customFieldIds: customFieldIds
            }).subscribe(resp => {
              this.saveExportedContacts(resp);
            });
        }
      });
  }

  private saveExportedContacts(resp: any): any {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.listName.listName}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  public selectionChange(event): void {
    event = event;
  }

  search(event): void {
    this.sectionService.resetQueryString(event);
  }

}
