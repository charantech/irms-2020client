import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestEventAdmissionComponent } from './event-guest-event-admission.component';

describe('EventGuestEventAdmissionComponent', () => {
  let component: EventGuestEventAdmissionComponent;
  let fixture: ComponentFixture<EventGuestEventAdmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestEventAdmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestEventAdmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
