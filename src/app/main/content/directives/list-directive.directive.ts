import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[irmsListDirective]'
})
export class ListDirectiveDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }
}
