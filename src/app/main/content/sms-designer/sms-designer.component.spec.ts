import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDesignerComponent } from './sms-designer.component';

describe('SmsDesignerComponent', () => {
  let component: SmsDesignerComponent;
  let fixture: ComponentFixture<SmsDesignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDesignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDesignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
