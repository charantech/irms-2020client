import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventSettingsComponent } from './event-settings.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { RouterModule } from '@angular/router';
import { MainModule } from 'app/main/main.module';
import { EventFeaturesComponent } from './event-features/event-features.component';
import { EventInfoComponent } from './event-info/event-info.component';
import { EventService } from '../../event.service';
import { EventDataService } from '../../event-data.service';
import { AgmCoreModule } from '@agm/core';

const routes = [
  {
      path: '',
      component: EventSettingsComponent,
      canLoad: [AuthGuard],
      data: { expectedRoles: [RoleType.TenantAdmin] }
  }
];

@NgModule({
  declarations: [EventSettingsComponent, EventFeaturesComponent, EventInfoComponent],
  imports: [
    MainModule,
    CommonModule,
    AgmCoreModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    EventService,
    EventDataService
  ],
  exports: [RouterModule]
})
export class EventSettingsModule { }
