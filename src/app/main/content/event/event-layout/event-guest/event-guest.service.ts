import { Injectable } from '@angular/core';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { EventGuestModel } from './event-guest.model';
import { EventGuestDataService } from './event-guest-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventGuestService extends SectionService<EventGuestModel> {
  private selectedGuestName = new BehaviorSubject<any>('');
  private guestInfo = new Subject<any>();
  invitationId: any;
  campaignId: any;
  rfiFormId: any;
  listId: any;
  stepConfig: any;
  eventId: any;
  pageConfig: any;
  constructor(
    dataService: EventGuestDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }

  public get unassignedList() {
    return {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'Unassigned'
    };
  }

  getGuestInfo(): Observable<any> {
    return this.guestInfo.asObservable();
  }

  setGuestInfo(data: any): void {
    this.guestInfo.next(data);
  }

  // Campaign name observables
  getSelectedGuestName(): Observable<any> {
    return this.selectedGuestName.asObservable();
  }

  setSelectedGuestName(name: string): void {
    this.selectedGuestName.next(name);
  }

  // Proceed background images for emails here in future
  proceedEmailTemplate(info: any): any {
    return {
      id: info.id,
      image: info.emailImage,
      sender: info.emailSender,
      subject: info.emailSubject
    };
  }

  // Proceed background images for sms here in future
  proceedSmsTemplate(info: any): any {
    return {
      id: info.id,
      image: info.smsImage,
      sender: info.smsSender,
    };
  }

  proceedWhatsappTemplate(info: any): any {
    return {
      id: info.id,
      image: info.whatsappImage,
      sender: info.whatsappSender,
    };
  }

  // Proceed background images for emails here in future
  proceedRfiTemplate(info: any): any {
    if (info.formCreated) {
      return {
        id: info.id,
        image: info.formBackgroundPath,
      };
    } else {
      return null;
    }
  }
}
