import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDesignerSendTestSmsComponent } from './sms-designer-send-test-sms.component';

describe('SmsDesignerSendTestSmsComponent', () => {
  let component: SmsDesignerSendTestSmsComponent;
  let fixture: ComponentFixture<SmsDesignerSendTestSmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDesignerSendTestSmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDesignerSendTestSmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
