import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { EventGuestDataService } from '../../event-guest-data.service';
import { EventGuestService } from '../../event-guest.service';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { GenericLoaderService } from 'app/main/content/components/shared/generic-loader/generic-loader.service';
import { appInjector } from 'app/main/app-injector';
import { EventGuestListItemRenameModalComponent } from './event-guest-list-item-rename-modal/event-guest-list-item-rename-modal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { EventGuestListExportModalComponent } from '../event-guest-list-export-modal/event-guest-list-export-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { sideDialogConfig } from 'app/constants/constants';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';

@Component({
  selector: 'irms-event-guest-list-item',
  templateUrl: './event-guest-list-item.component.html',
  styleUrls: ['./event-guest-list-item.component.scss']
})
export class EventGuestListItemComponent implements OnInit {
  @Input() data;
  @Output() updateEvent: EventEmitter<any> = new EventEmitter();

  constructor(
    private dialog: MatDialog,
    private animDialog: DialogAnimationService,
    private sectionService: EventGuestService,
    private dataService: EventGuestDataService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  eventId: any;

  ngOnInit() {
    this.eventId = this.router.url.substring(8).substring(0, 36);
    console.log(this.data,"aaa")
  }

  @PopUp('Are you sure want to delete event guests list?')
  public delete(): void{
    const toasterService = appInjector().get(ToasterService);


    this.dataService.deleteGuestList({
      id: this.data.id
    })
    .subscribe(result => {
      if (result) {
        toasterService.pop('success', null, 'Successfully deleted.');
      } else {
        toasterService.pop('error', 'This list can not be deleted!', 'This list can’t be deleted because it is in use with a campaign.\n' 
                                  + 'Please disassociate this list from this association before attempting to delete.');
      }
      this.sectionService.doRefreshList();
    });
  }
  
  public navigateToGuestList() {
    this.router.navigate([`../${this.data.id}`], { relativeTo: this.route});
  }

  public rename(): void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '20vw';  
    dialogConfig.data = { 
      id: this.data.id,
      name: this.data.name
    };
 
    const dialogRef = this.dialog.open(EventGuestListItemRenameModalComponent, dialogConfig);
    
    dialogRef.afterClosed()
      .subscribe(result => {
        if (result && result.name && result.name != this.data.name) {
          this.dataService.renameList({
            id: this.data.id,
            name: result.name
          })
          .subscribe(_ => {
            this.sectionService.doRefreshList();
          });
        }
      });
  }

  public export(): void {
    const dialogConfig:any = sideDialogConfig;
    //dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    //dialogConfig.height = '55vh';
    dialogConfig.data = {
      eventId:this.eventId,
      filename:this.data.name,
      contactListId:this.data.id
    }
    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
    .subscribe(result => {
      if (result) {
        const columns = result.columns;
        const customFieldIds = result.customFieldIds

        this.dataService
          .exportContacts({
            contactListId: this.data.id,
            filename: this.data.name.listName,
            columns: columns,
            customFieldIds: customFieldIds,
            eventId: this.eventId
          }).subscribe(resp => {
            this.saveExportedContacts(resp);
          });
      }
    })
  }
  
  private saveExportedContacts(resp: any) {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.data.name}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
}
