import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { AccountService } from './account.service';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { VerifyOtpComponent } from './veirfy-otp/veiry-otp.component';
import { LayoutModule } from 'app/layout/layout.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular5-toaster/dist/angular5-toaster';
import { MaterialModule } from 'app/material.module';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { VerifyMobileComponent } from './verify-mobile/verify-mobile.component';
import { CreatePasswordComponent } from './create-password/create-password.component';


const routes = [
    { path: 'login', component: LoginComponent },
    { path: 'verify-otp', component: VerifyOtpComponent },
    { path: 'recover-password', component: RecoverPasswordComponent },
    { path: 'change-password', component: ChangePasswordComponent },
    { path: 'verify-mobile', component: VerifyMobileComponent },
    { path: 'create-password', component: CreatePasswordComponent }
];

@NgModule({
    declarations: [
        LoginComponent,
        VerifyOtpComponent,
        RecoverPasswordComponent,
        ChangePasswordComponent,
        VerifyMobileComponent,
        CreatePasswordComponent
    ],
    imports: [
        ToasterModule,
        ReactiveFormsModule,
        LayoutModule,
        MaterialModule,
        RouterModule.forChild(routes),
        RecaptchaModule.forRoot(),
        RecaptchaFormsModule,        
        FuseSharedModule
    ],
    providers: [
        AccountService, AppStateService
    ],
    exports: [ RouterModule ]
})
export class AccountModule {}
