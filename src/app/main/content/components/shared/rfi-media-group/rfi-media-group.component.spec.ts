import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfiMediaGroupComponent } from './rfi-media-group.component';

describe('RfiMediaGroupComponent', () => {
  let component: RfiMediaGroupComponent;
  let fixture: ComponentFixture<RfiMediaGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfiMediaGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfiMediaGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
