import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDesignerFormComponent } from './sms-designer-form.component';

describe('SmsDesignerFormComponent', () => {
  let component: SmsDesignerFormComponent;
  let fixture: ComponentFixture<SmsDesignerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDesignerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDesignerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
