import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { EventCampaignModel } from '../../event-campaign.model';
import { IResponseModel } from 'types';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationDataService implements SectionDataService<EventCampaignModel> {

  private url = `${BASE_URL}api/campaignInvitation`;
  constructor(private http: HttpClient) { }
  
  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}`);
  }

  @Toast('Successfully updated')
  createInvitation(model) {
    return this.http.post(`${this.url}`, model);
  }

  getList(filterParam): Observable<IResponseModel> {
    return of();
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }
  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
}
