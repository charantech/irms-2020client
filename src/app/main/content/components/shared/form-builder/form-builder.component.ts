import { Component, OnInit, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { FormService } from '../form/form.service';
import { FormBuilder, Validators } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { SectionViewEditComponent } from '../section/section-view-edit/section-view-edit.component';
import { MatDialogConfig, MatDialog, MatDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { Section } from 'app/constants/constants';
import { FormBuilderService } from './form-builder.service';
import { FormBuilderDataService } from './form-builder-data.service';
import { BaseModel } from '../section/base.model';
import { FormDesignerService } from 'app/main/content/form-designer/form-designer.service';
import { Subscription } from 'rxjs';
import { CampaignRfiDesignModalComponent } from 'app/main/content/event/event-layout/event-campaign/event-campaign-edit/campaign-invitation/campaign-rfi-design-modal/campaign-rfi-design-modal.component';

@Component({
  selector: 'irms-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
  animations: fuseAnimations
})
export class FormBuilderComponent extends SectionViewEditComponent<BaseModel> implements OnInit, AfterViewInit, OnDestroy, CanExit {
  selectedTab = 0;
  public form = this.fb.group(
    {
      formContent: this.fb.group({
        welcome: this.fb.group({
          content: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
      <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">
      Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
          contentArabic: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
      <h5 style="text-align: left;"><span style="color: #34495e;"><em>
      <span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
          button: ['Proceed'],
          buttonArabic: ['تقدم']
        }),
        questions: this.fb.array([]),
        submit: this.fb.group({
          submitMessage: `Everything seems good,\nYou can submit now!`,
          submitMessageArabic: `جميع البيانات صحيحة،\n قم بتقديم الطلب الآن!`,
          submitButton: 'Submit',
          submitButtonArabic: 'تقدم'
        }),
        thanks: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`],
          contentArabic: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`]
        })
      }),
      formTheme: this.fb.group({
        buttons: this.fb.group({
          background: '#576268',
          text: '#fff',
        }),
        questions: this.fb.group({
          color: '#576268'
        }),
        answers: this.fb.group({
          color: '#576268'
        }),
        background: this.fb.group({
          color: '#fff',
          image: '',
          style: '',
        }),
      }),
      formSettings: this.fb.group({
        title: ['Form'],
        defaultLanguage: 'en',
        languageOption: false
      })
    });
  @Input() invitationId;
  formId: string;
  subscriptions: Subscription[] = [];

  constructor(private fb: FormBuilder,
    protected preview: FormService,
    public sectionService: FormBuilderService,
    protected dataService: FormBuilderDataService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected designerService: FormDesignerService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<CampaignRfiDesignModalComponent>
  ) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
    this.subscriptions.push(this.designerService.saveObs().subscribe(() => this.saveRfiForm()));
  }

  ngOnInit(): void {
    this.subscriptions.push(this.sectionService.getFormContent().subscribe(val => { this.form.patchValue(val); }));
    this.dataService.get(this.invitationId).subscribe(m => {
      this.model = m;
      if (m) {
        this.model['rfiFormQuestions'] = m.rfiFormQuestions.sort((obj1, obj2) => {
          if (obj1.sortOrder > obj2.sortOrder) {
            return 1;
          }

          if (obj1.sortOrder < obj2.sortOrder) {
            return -1;
          }

          return 0;
        });
        this.formId = m.id;
        this.isDataLoaded = true;
        this.patchModelInForms();
      } else {
        this.sectionService.setIsFormLoaded(true);
      }
    });
    this.preview.setForm(this.form.value);
    this.form.valueChanges.subscribe(data => {
      this.preview.setForm(data);
    });
  }



  ngAfterViewInit(): void {
    this.preview.setForm(this.form.value);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.form.reset();
  }

  refreshFormPreview(): void {
    this.preview.refreshFormTrigger();
  }

  patchModelInForms(): void {

    this.form.controls['formContent']['controls'].welcome.patchValue(JSON.parse(this.model['welcomeHtml']));
    this.form.controls['formContent']['controls'].submit.patchValue(JSON.parse(this.model['submitHtml']));
    this.form.controls['formContent']['controls'].thanks.patchValue(JSON.parse(this.model['thanksHtml']));
    this.form.controls['formContent']['controls'].submit.patchValue(JSON.parse(this.model['submitHtml']));
    this.form.controls['formTheme']['controls'].buttons.patchValue(JSON.parse(this.model['formTheme']).buttons);
    this.form.controls['formTheme']['controls'].questions.patchValue(JSON.parse(this.model['formTheme']).questions);
    this.form.controls['formTheme']['controls'].answers.patchValue(JSON.parse(this.model['formTheme']).answers);
    this.form.controls['formTheme']['controls'].background.controls.color.setValue(JSON.parse(this.model['formTheme']).background.color);
    this.form.controls['formTheme']['controls'].background.controls.style.setValue(JSON.parse(this.model['formTheme']).background.style);
    this.form.controls.formSettings.patchValue(JSON.parse(this.model['formSettings']));

    // patch image
    if (this.model['themeBackgroundImagePath']) {
      this.form.controls['formTheme']['controls'].background.controls.image.setValue(this.model['themeBackgroundImagePath']);
    }

    // patch questions
    this.patchQuestions(this.model['rfiFormQuestions']);
  }

  patchQuestions(questions): void {
    const mappedField = [];
    questions.forEach(q => {
      const parsedQ = JSON.parse(q.question);
      this.addQuestion(q, parsedQ);
      if (parsedQ.mapping) {
        mappedField.push({
          type: parsedQ.type,
          field: parsedQ.mappingField
        });
      }
    });
    this.sectionService.setSelectedFields(mappedField);
    this.sectionService.setIsFormLoaded(true);
  }

  addQuestion(ques, question): void {
    question.id = ques.id;

    // add choices if question type is select/search
    if (question.type === 'select' || question.type === 'selectSearch') {
      this.addChoices(question, question.choices);
    }

    this.form.controls['formContent']['controls'].questions.insert(ques.sortOrder - 1, this.fb.group(question));
  }

  addChoices(ques, choices): void {
    ques.choices = this.fb.array([]);
    choices.forEach(ch => {
      ques.choices.insert(ch.value, this.fb.group({
        label: ch.label,
        labelArabic: ch.labelArabic,
        value: ch.value,
        selected: ch.selected
      }));
    });
  }

  canDeactivate(): any {
    if (this.form.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

  close() {
    this.dialogRef.close();
  }

  saveRfiForm(): void {
    const formValue = this.form.value;
    if (this.form.valid) {

      const allQuestions = formValue.formContent.questions;
      const questions = [];
      allQuestions.forEach((q, i) => {
        // fallback for validations in question object
        if (this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('minLength')) {
          q.minLength = this.form.controls['formContent']['controls'].questions.controls[i].controls.minLength.value;
          q.maxLength = this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('maxLength') ? this.form.controls['formContent']['controls'].questions.controls[i].controls.maxLength.value : 50;
        } else if (this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('minDate')) {
          q.minDate = this.form.controls['formContent']['controls'].questions.controls[i].controls.minDate.value;
          q.maxDate = this.form.controls['formContent']['controls'].questions.controls[i].controls.maxDate.value;
        }
        if (this.form.controls['formContent']['controls'].questions.controls[i].controls.hasOwnProperty('pattern')) {
          q.pattern = this.form.controls['formContent']['controls'].questions.controls[i].controls.pattern.value;
        }

        questions.push({
          id: q.id,
          question: JSON.stringify(q),
          mapped: q.mapping,
          mappedField: q.mappingField,
          sortorder: (i + 1)
        });
      });

      let rfi = {
        campaignInvitationId: this.invitationId,
        welcomeHtml: JSON.stringify(this.form.controls['formContent']['controls'].welcome.value),
        submitHtml: JSON.stringify(this.form.controls['formContent']['controls'].submit.value),
        thanksHtml: JSON.stringify(this.form.controls['formContent']['controls'].thanks.value),
        formTheme: JSON.stringify({
          buttons: this.form.controls['formTheme']['controls'].buttons.value,
          questions: this.form.controls['formTheme']['controls'].questions.value,
          answers: this.form.controls['formTheme']['controls'].answers.value,
          background: {
            color: this.form.controls['formTheme']['controls'].background.controls.color.value,
            style: this.form.controls['formTheme']['controls'].background.controls.style.value
          }
        }),

        formSettings: JSON.stringify(this.form.controls['formSettings'].value),
        themeBackgroundImage: this.form.controls['formTheme']['controls'].background.controls.image.value,
        questionJson: JSON.stringify(questions)
      };

      rfi = this.sectionService.trimValues(rfi);
      if (this.formId) {
        rfi['id'] = this.formId;
      }

      const fd = new FormData();
      for (const key in rfi) {
        fd.append(key, rfi[key]);
      }

      this.dataService.upsert(fd).subscribe(id => {
        this.formId = id.toString();
      });
    }
  }
}
