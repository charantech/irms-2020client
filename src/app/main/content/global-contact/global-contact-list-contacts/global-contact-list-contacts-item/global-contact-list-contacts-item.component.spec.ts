import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactListContactsItemComponent } from './global-contact-list-contacts-item.component';

describe('GlobalContactListContactsItemComponent', () => {
  let component: GlobalContactListContactsItemComponent;
  let fixture: ComponentFixture<GlobalContactListContactsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListContactsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListContactsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
