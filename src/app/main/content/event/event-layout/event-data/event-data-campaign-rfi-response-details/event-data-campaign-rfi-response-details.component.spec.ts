import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiResponseDetailsComponent } from './event-data-campaign-rfi-response-details.component';

describe('EventDataCampaignRfiResponseDetailsComponent', () => {
  let component: EventDataCampaignRfiResponseDetailsComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiResponseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiResponseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiResponseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
