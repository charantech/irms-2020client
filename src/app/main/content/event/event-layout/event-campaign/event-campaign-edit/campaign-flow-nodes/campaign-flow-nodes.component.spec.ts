import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignFlowNodesComponent } from './campaign-flow-nodes.component';

describe('CampaignFlowNodesComponent', () => {
  let component: CampaignFlowNodesComponent;
  let fixture: ComponentFixture<CampaignFlowNodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignFlowNodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignFlowNodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
