import { Component, OnInit } from '@angular/core';
import { EventGuestListGuestsItemComponent } from './event-guest-list-guests-item/event-guest-list-guests-item.component';
import { EventGuestService } from '../event-guest.service';
import { EventGuestDataService } from '../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Section, sideDialogConfig } from 'app/constants/constants';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { EventGuestModel } from '../event-guest.model';
import { fuseAnimations } from 'app/core/animations';
import { of, Subscription} from 'rxjs';
import { IActionButtons, IGuestPage } from 'types';
import { EventGuestListGuestsItemLockedComponent } from './event-guest-list-guests-item-locked/event-guest-list-guests-item-locked.component';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { MatDialog } from '@angular/material';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { appInjector } from 'app/main/app-injector';
import { EventGuestListExportModalComponent } from '../event-guest-list/event-guest-list-export-modal/event-guest-list-export-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';

@Component({
  selector: 'irms-event-guest-list-guests',
  templateUrl: './event-guest-list-guests.component.html',
  styleUrls: ['./event-guest-list-guests.component.scss'],
  animations: fuseAnimations,
})
export class EventGuestListGuestsComponent extends SectionListComponent<EventGuestModel> implements OnInit {
  public guests;
  public listName:any  = 'Back';
  public actionButtons: IActionButtons = {
    leftSide: [
      {
        title: 'Delete Selected',
        icon: 'delete',
        handler: this.removeGuests.bind(this),
      },
      {
        title: 'Export Selected',
        icon: 'launch',
        handler: this.exportGuests.bind(this),
      }
    ],
  };
  selectedGuests = [];
  isLocked;
  listId: number;

  eventId: any;
  getListSubscription: Subscription;

  public listItemComponent: Object = EventGuestListGuestsItemComponent;
  public lockedListItemComponent: Object = EventGuestListGuestsItemLockedComponent;
  listHeadersEditable = ['Guest Name', 'Mobile Number', 'Email', 'Date Added', 'Date Updated'];
  listHeadersLocked =  ['Guest Name', 'Mobile Number', 'Email', 'Status'];
  allCount: number;
  isLoading: boolean;
  isLive: any;
  constructor(public sectionService: EventGuestService,
              protected dataService: EventGuestDataService,
              private dialog: MatDialog,
              private animDialog: DialogAnimationService,
              router: Router, public route: ActivatedRoute) {
    super(Section.EventGuests, sectionService, dataService, router);
  }

  guestPage: IGuestPage;

  ngOnInit(): void {

    this.sectionService.subscriptions = [];
    this.route.params.subscribe((params: Params) => {
      this.eventId = this.router.url.substring(8).substring(0, 36);
      this.listId = params.glid;
      this.isLoading = true;
      this.guestPage = {
        listId: params.glid,
        eventId: this.eventId,
        pageSize: 5,
        pageNo: 1,
        searchText: null
      };

      this.dataService.getListName({
        id: params.glid
      })
      .subscribe(result => {
        console.log(typeof result)
        this.listName = result.listName;
        this.isLive = result.isLive;
      });

      this.sectionService.setFilter({ });
      super.ngOnInit();
    });
    this.reloadGuests(true);
  }
  /// Handle List Selection Change
  public selectionChange(event): void{
    if (this.maxSelectable === 0){
      this.setMaxSelectable();
    }
    this.selectedGuests = event;
    if (this.selectedGuests.length !== this.maxSelectable && this.selectedGuests.length > 0){
      this.indeterminate = true;
      this.checked = false;
    } else if (this.selectedGuests.length === this.maxSelectable){
      this.indeterminate = false;
      this.checked = true;
    } else {
      this.checked = false;
      this.indeterminate = false;
    }
  }

  /// handle select all
  public selectAll(event): void{
    const data = this.toggleSelectAll();
    if (event) {
      this.selectedGuests = this.guests;
      this.selectionChange(data);
    } else {
      this.selectionChange([]);
    }
  }

  selectAllGuests(event): void {
  }

  public search(text): void {
    if (text !== null && text.length === 0) {
      this.guestPage.searchText = null;
    } else {
      this.guestPage.searchText = text;
    }
    this.guestPage.pageNo = 1;
    this.sectionService.pageIndex = 0;
    this.sectionService.pageSize = this.guestPage.pageSize;
    this.reloadGuests();
  }

  public changePagination(event): void {
    this.guestPage.pageNo = event.pageIndex + 1;
    this.guestPage.pageSize = event.pageSize;
    this.reloadGuests();
  }


  private reloadGuests(firstInit = false): void {
    // this.isLocked = true;
    if ( this.getListSubscription ) {
      this.getListSubscription.unsubscribe();
    }
    this.getListSubscription = this.dataService.getlocalGuestContactByListId(this.guestPage)
      .subscribe(response => {
        if (firstInit) {
          this.allCount = response.totalCount;
        }
        this.isLocked = response.isReadonly;
        this.data = of(response);
        this.sectionService.data.next(response);
        this.totalCount = response.totalCount;
        this.isLoading = false;
        
      });
  }

  /// Remove Guests 
  @PopUp('Are you sure you want to delete guests?')
  removeGuests(): void {
    this.dataService.removeUsers({
      contactIds: this.selectedGuests,
      contactListId: this.route.snapshot.params.glid
    })
    .subscribe(_ => {
      this.selectedGuests = [];
      this.reloadGuests(true);
    });          
  }

  @PopUp('Are you sure you want to delete contact list?')
  deleteList(): void {
    const toasterService = appInjector().get(ToasterService);
    
    this.dataService.deleteGuestList({
      id: this.route.snapshot.params.glid
    })
    .subscribe(resp => {
      
      if (resp) {
        toasterService.pop('success', null, 'Successfully deleted.');
      } else {
        toasterService.pop('error', 'This list can not be deleted!', 'This list can’t be deleted because it is in use with a campaign.\n' 
                                  + 'Please disassociate this list from this association before attempting to delete.');
      }
      this.router.navigate(['../'], {relativeTo: this.route});
    });
  }

  // Export all guests
  public exportAllGuests(): void {
    const dialogConfig: any = sideDialogConfig;
    // dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    dialogConfig.data = {
      eventId: this.eventId,
      contactListId: this.listId,
      filename: this.listName,
      
    };

    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
    .subscribe(result => {
      if (result) {
        const columns = result.columns;
        const customFieldIds = result.customFieldIds;

        this.dataService
          .exportContacts({
            contactListId: this.listId,
            filename: this.listName,
            columns: columns,
            customFieldIds: customFieldIds,
            eventId: this.eventId
          }).subscribe(resp => {
            this.saveExportedContacts(resp);
          });
      }
    });
  }

  /// Export Guests 
  exportGuests(): void {
    const dialogConfig: any = sideDialogConfig;
    // dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    // dialogConfig.height = '55vh';
    dialogConfig.data = {
      eventId: this.eventId,
      contactListId: this.listId,
      filename: this.listName,
      contactIds: this.selectedGuests,
    };
                                          //EventGuestListExportModalComponent
    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);

    // dialogRef.afterClosed()
    //   .subscribe(result => {
    //     if (result) {
    //       const columns = result.columns;

    //       this.dataService
    //         .exportContacts({
    //           contactListId: this.listId,
    //           filename: this.listName,
    //           contactIds: this.selectedGuests,
    //           columns: columns,
    //           eventId: this.eventId
    //         }).subscribe(resp => {
    //           this.saveExportedContacts(resp);
    //         });
    //     }
    //   });
  }

  private saveExportedContacts(resp: any): void {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.listName}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  goToImportGuests(): void{
    this.router.navigateByUrl(`/global-contacts/upload-contact-list`, {
      state: {
        data: {
          eventId: this.eventId,
          eventGuestId: this.listId 
        }
      }
    });

  }

  goToCreateGuests(): void{
    this.router.navigateByUrl(`/global-contacts/add-contact`, {
      state: {
        data: {
          eventId: this.eventId,
          eventGuestId: this.listId 
        }
      }
    });
  }
}
