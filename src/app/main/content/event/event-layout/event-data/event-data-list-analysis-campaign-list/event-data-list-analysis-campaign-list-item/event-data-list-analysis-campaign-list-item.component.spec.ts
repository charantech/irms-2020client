import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataListAnalysisCampaignListItemComponent } from './event-data-list-analysis-campaign-list-item.component';

describe('EventDataListAnalysisCampaignListItemComponent', () => {
  let component: EventDataListAnalysisCampaignListItemComponent;
  let fixture: ComponentFixture<EventDataListAnalysisCampaignListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataListAnalysisCampaignListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataListAnalysisCampaignListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
