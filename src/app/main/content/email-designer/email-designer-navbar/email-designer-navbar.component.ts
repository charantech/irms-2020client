import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EmailDesignerNavigation, EmailDesignerNavigationWithoutRsvp } from './email-designer-navigation';
import {Location} from '@angular/common';
import { EmailDesignerService } from '../email-designer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailDesignerDataService } from '../email-designer-data.service';

@Component({
  selector: 'irms-email-designer-navbar',
  templateUrl: './email-designer-navbar.component.html',
  styleUrls: ['./email-designer-navbar.component.scss']
})
export class EmailDesignerNavbarComponent implements OnInit {
  navigation;
  
  @Output() sendTestEmails: EventEmitter<any> = new EventEmitter();
  @Output() saveTemplate: EventEmitter<any> = new EventEmitter();
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: EmailDesignerDataService,
    protected location: Location,
    protected designerService: EmailDesignerService
  ) { 
    this.designerService.invitationId.next(route.snapshot.params.tempId);
    this.dataService.invitationInfo(route.snapshot.params.tempId)
    .subscribe(x => {
      if (x.invitationType === 0 || x.invitationType === 1){ // rsvp or pending
      this.navigation = EmailDesignerNavigation;
      } else { 
      this.navigation = EmailDesignerNavigationWithoutRsvp;
      }
    });
    this.designerService.flushTemplate();
  }

  ngOnInit(): void {
  }

  onClickSend(): void{
    this.designerService.triggerSendTest(true);
  }

  onClickSave(): void{
    this.designerService.triggerSave();
  }

  // Go back
  back(): void{
    this.location.back();
  }

}
