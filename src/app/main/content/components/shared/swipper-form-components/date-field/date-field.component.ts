import { Component, OnInit, Input, Output, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'irms-date-field',
  templateUrl: './date-field.component.html',
  styleUrls: ['./date-field.component.scss']
})
export class DateFieldComponent implements OnInit, AfterViewInit {
  @Input() theme: any;
  @Input() isArabic: boolean;
  @Input() reviewMode = false;
  @Input() question: any;
  @Input() public inputControl: FormControl;
  @Output() scrollNext = new EventEmitter<any>();
  @Output() fieldUpdate = new EventEmitter<string>();

  public dateString: any;
  mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  minDate: any;
  maxDate: any;
  constructor(public element: ElementRef) { }

  ngOnInit(): void{
  }
  
  setAsCurrent(): void{
    // this.questionSwipperService.setCurrent(this.question);
  }
  ngAfterViewInit(): void{
    (this.question.minDate != null) ? this.minDate = new Date(this.question.minDate) : this.minDate = null ;
    (this.question.maxDate != null) ? this.maxDate = new Date(this.question.maxDate) : this.maxDate = null ;
    if (this.question.hasOwnProperty('response')
      && this.question.response !== undefined 
      && this.question.response !== null 
      && this.question.response !== '' ) {
        const dateObj = new Date(this.question.response);
        const options = {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit'
        };
        this.inputControl.setValue(dateObj.toLocaleDateString('en-GB', options));
    }
  }

  // On Enter key press
  onPressEnter(event): void{
    if (event.keyCode === 13 || event.keyCode === 9) {
      event.preventDefault();
      this.gotoNext();
    }
  }
  onBlur(): void{
    this.fieldUpdate.next();
  }
  //  Go to next
  gotoNext(): void{
    if (this.reviewMode) {
      return;
    }
    this.fieldUpdate.next();
    this.scrollNext.emit();
  }
  replaceRange(dateString, start, end, substitute): string{
    return dateString.substring(0, start) + substitute + dateString.substring(end);
  }
  keyup(): void{
    this.dateString = this.inputControl.value.toString();   
    this.prettyDate();
    if (this.validateDate() === false) {
      this.inputControl.setErrors({ dateError: true });
    } else { 
      const parts = this.dateString.split('/');
      const day = parts[0];
      const month = parts[1];
      const year = parts[2];      
      const date = new Date(`${month}/${day}/${year}`);
      if (this.minDate && this.maxDate) {
        if (date.getTime() < this.minDate.getTime() || date.getTime() > this.maxDate.getTime()) {
          this.inputControl.setErrors({ dateError: false, between: true });
          return;
        }
      } else if ((this.minDate && !this.maxDate) && (date.getTime() < this.minDate.getTime())) {
        this.inputControl.setErrors({ dateError: false, less: true });
        return;
      } else if ((!this.minDate && this.maxDate) && (date.getTime() > this.maxDate.getTime())) {
        this.inputControl.setErrors({ dateError: false, greater: true });
        return;
      } else {

      }
    }
  }
  prettyDate(): void{
    let start = this.element.nativeElement.querySelector('input').selectionStart;
    let end = this.element.nativeElement.querySelector('input').selectionEnd;
    let replacedate = this.dateString;
    const parts = this.dateString.split('/');
    const day = parts[0];
    const month = parts[1];
    const year = parts[2];
    // Day check
    if (parseInt(day, 10) <= 31 && (isNaN(parseInt(day[0], 10)) || isNaN(parseInt(day[1], 10)))) {
      if (parseInt(day[0], 10) > 3 && isNaN(parseInt(day[1], 10))) {
        replacedate = this.replaceRange(this.dateString, 0, 2, '0' + day[0]);
        start = 3;
        end = 3;
      } else if (parseInt(day[1], 10) <= 9 && parseInt(day[1], 10) > 0 && isNaN(parseInt(day[0], 10))) {
        replacedate = this.replaceRange(this.dateString, 0, 2, '0' + day[1]);
        start = 3;
        end = 3;
      }
    } else if (parseInt(day, 10) > 31) {
      replacedate = this.replaceRange(this.dateString, 0, 2, '30');
      start = 3;
      end = 3;
    }
    // Month Check
    if (parseInt(month, 10) <= 12 && (isNaN(parseInt(month[0], 10)) || isNaN(parseInt(month[1], 10)))) {
      if (parseInt(month[0], 10) > 1 && isNaN(parseInt(month[1], 10))) {
        replacedate = this.replaceRange(this.dateString, 3, 5, '0' + month[0]);
        start = 6;
        end = 6;
      } else if (parseInt(month[1], 10) <= 2 && isNaN(parseInt(month[0], 10))) {
        replacedate = this.replaceRange(this.dateString, 3, 5, '0' + month[1]);
      } else if (parseInt(month[0], 10) === 0 && parseInt(month[1], 10) === 0) {
        replacedate = this.replaceRange(this.dateString, 3, 5, '01');
        start = 6;
        end = 6;
      }
      
    } else if (parseInt(month, 10) > 12) {
      replacedate = this.replaceRange(this.dateString, 3, 5, '12');
      start = 6;
      end = 6;
    }
    // Year Check
    // console.log('year len', Math.floor(Math.log10(parseInt(year, 10)) + 1));
    // if ((Math.floor(Math.log10(parseInt(year, 10)) + 1)) === 4) {
    //   if (parseInt(year, 10) === 0 || parseInt(year, 10) < this.question.minDate) {
    //     replacedate = this.replaceRange(this.dateString, 6, 10, this.question.minDate);
    //   } 
    //   else if (parseInt(year, 10) > this.question.maxDate) {
    //     replacedate = this.replaceRange(this.dateString,  6, 10, this.question.maxDate);
    //   }
    // }
    this.inputControl.setValue(replacedate);
    this.dateString = replacedate;
    this.element.nativeElement.querySelector('input').focus();
    const that = this;
    window.setTimeout(() => {
      that.element.nativeElement.querySelector('input').setSelectionRange(start, end);
    }, 0);
  }

  validateDate(): boolean{
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(this.dateString)) {
      return false;
    }
    // Parse the date parts to integers
    const parts = this.dateString.split('/');
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10);
    const year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month === 0 || month > 12 || day === 0 || day > 31) {
      return false;
    }

    const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
      monthLength[1] = 29;
    }

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
  }

}
