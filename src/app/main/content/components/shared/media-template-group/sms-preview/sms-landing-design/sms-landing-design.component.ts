import { Component, Inject, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmailDesignerDataService } from 'app/main/content/email-designer/email-designer-data.service';
import { SmsDesignerDataService } from 'app/main/content/sms-designer/sms-designer-data.service';

@Component({
  selector: 'irms-sms-landing-design',
  templateUrl: './sms-landing-design.component.html',
  styleUrls: ['./sms-landing-design.component.scss']
})
export class SmsLandingDesignComponent implements OnInit {
  public rsvpForm = this.fb.group(
    {
      formContent: this.fb.group({
        welcome: this.fb.group({
          content: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
      <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
          button: ['Proceed', [Validators.required]]
        }),
        rsvp: this.fb.group({
          content: [`<p><span style="font-family: tahoma, arial, helvetica, sans-serif;">Do you accept the Invitation to the&nbsp;</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p><span style="color: #34495e; font-family: 'arial black', sans-serif;">Select your Response:&nbsp;</span></p>`, [Validators.required]],
          acceptButton: [`Yes I'll be there`, [Validators.required]],
          rejectButton: [` Nope I won't be there`, [Validators.required]]
        }),
        accept: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`, [Validators.required]]
        }),
        reject: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`, [Validators.required]]
        })
      }),
      formTheme: this.fb.group({
        buttons: this.fb.group({
          background: '#576268',
          text: '#fff',
        }),
        background: this.fb.group({
          color: '#fff',
          image: '',
          style: '',
        }),
      }),
      formSettings: this.fb.group({
        title: ['']
      })
    });
  tempInfo: {};
  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<SmsLandingDesignComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dataService: SmsDesignerDataService) { }

  ngOnInit(): void {
    this.tempInfo = {
      id: this.data.id,
      campaignInvitationId: this.data.campaignInvitationId,
      senderName: this.data.senderName,
      body: this.data.body,
      type: this.data.type,
      copyTemplate: this.data.copyTemplate
    };
    if (this.data.welcomeHtml) {
      this.rsvpForm.get('formContent.rsvp.content').setValue(this.data.rsvpHtml ? this.data.rsvpHtml : this.data.rsvphtml);
      this.rsvpForm.get('formContent.rsvp.acceptButton').setValue(this.data.acceptButtonText);
      this.rsvpForm.get('formContent.rsvp.rejectButton').setValue(this.data.rejectButtonText);
      this.rsvpForm.get('formContent.accept.content').setValue(this.data.acceptHtml);
      this.rsvpForm.get('formContent.reject.content').setValue(this.data.rejectHtml);
      this.rsvpForm.get('formContent.welcome.content').setValue(this.data.welcomeHtml);
      this.rsvpForm.get('formContent.welcome.button').setValue(this.data.proceedButtonText);
      if (this.data.themeJson != null) {
        this.rsvpForm.get('formTheme').patchValue(this.data.themeJson);
        this.rsvpForm.get('formTheme.background.image').setValue(this.data.backgroundImagePath)

      }
    }
  }
}