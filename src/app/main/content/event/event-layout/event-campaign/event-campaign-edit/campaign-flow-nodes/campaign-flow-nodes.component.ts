import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { EventCampaignEditService } from '../event-campaign-edit.service';
import { CampaignConfigurationsService } from '../campaign-configurations/campaign-configurations.service';
import { CampaignConfigurationsDataService } from '../campaign-configurations/campaign-configurations-data.service';
import { timePeriodUnits } from 'app/constants/constants';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CampaignFlowNodesService } from './campaign-flow-nodes.service';
@Component({
  selector: 'irms-campaign-flow-nodes',
  templateUrl: './campaign-flow-nodes.component.html',
  styleUrls: ['./campaign-flow-nodes.component.scss']
})
export class CampaignFlowNodesComponent implements OnInit, OnDestroy {
  timePeriodUnits = timePeriodUnits;
  flowNodes = [];
  @Input() parentText = 'Invitation Sent';
  @Input() parentIcon = 'near_me';
  @Input() nodeLabelPrefix = 'Reminder';
  @Input() emptyText = 'Add reminders for your pending guests';
  @Input() readonly: boolean = false;
  @Output() copyEvent: EventEmitter<any> = new EventEmitter();
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter();
  @Output() addEvent: EventEmitter<null> = new EventEmitter();
  @Output() updateEvent: EventEmitter<null> = new EventEmitter();
  updater: Subscription;
  @Input() 
  set nodes(nodes) {
    this.flowNodes = nodes;
    this.setCurrentNodeIndex(this.router.url);
  }


  constructor(
    public service: EventCampaignEditService,
    public sectionService: CampaignConfigurationsService,
    protected dataService: CampaignConfigurationsDataService,
    protected location: Location, private router: Router,
    protected nodeService: CampaignFlowNodesService) {
      location.onUrlChange( url => {
        this.setCurrentNodeIndex(url);
      });
     }

  ngOnInit(): void {
    this.updater = this.sectionService.updateNodes.subscribe(() => {
      this.updateEvent.emit();
    });
  }
  copyNode(id): void {
    this.copyEvent.emit(id);
  }
  deleteNode(id): void {
    this.deleteEvent.emit(id);
  }
  addNode(): void {
    this.addEvent.emit();
  }

  setCurrentNodeIndex(url): void{
    if (!this.flowNodes || !this.flowNodes[0]) {
      return;
    }
    const nodeId = url.substr(url.lastIndexOf('/') + 1);
    if (this.flowNodes[0].id === nodeId) {
      this.nodeService.setFirstNode(true);
    } else {
      this.nodeService.setFirstNode(false);
    }
  }

  ngOnDestroy(): void {
    this.updater.unsubscribe();
  }
}
