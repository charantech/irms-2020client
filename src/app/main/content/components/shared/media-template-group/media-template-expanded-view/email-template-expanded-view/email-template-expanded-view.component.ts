import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'irms-email-template-expanded-view',
  templateUrl: './email-template-expanded-view.component.html',
  styleUrls: ['./email-template-expanded-view.component.scss']
})
export class EmailTemplateExpandedViewComponent implements OnInit {

  @Input() invitationId: string;
  @Input() isDialog;
  @Input() step;
  @Input() isReview = false;
  
  constructor() { }

  ngOnInit(): void {
  }

}
