import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-preferred-media-list-item',
  templateUrl: './preferred-media-list-item.component.html',
  styleUrls: ['./preferred-media-list-item.component.scss']
})
export class PreferredMediaListItemComponent implements OnInit {
  @Input() selectable: boolean;
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
